import React, { useState, useRef, useEffect } from 'react'
import { useHistory } from 'react-router-dom';
import { Container, Form } from 'react-bootstrap';
import NavigationBar from '../../parts/NavigationBar/NavigationBar';

import { faUsers, faEdit, faUndo } from '@fortawesome/free-solid-svg-icons';

import classes from './MainDigitalGobuddyPage.module.css'
import Logo from '../../components/Logo/Logo';
import AddQuizView from '../../components/AddQuizView/AddQuizView';

import DigitalGobuddyButton from '../../components/DigitalGobuddyButton/DigitalGobuddyButton';
import { GOBUDDY_LS_TOKEN } from '../../constants/localstoragenames';
import { getQuizByCode } from '../../shared/backend/backend-service';
import LoaderLogo from '../../components/UI/LoaderLogo/LoaderLogo';

// Constants
const codeInit = {"0": "", "1": "", "2": "", "3": "", "4": "", "5": "", "6": "", "7": ""};
const ID_BASE = 'gobuddy_text_input_';
const JOIN_GAME_OPTION = 'join-game';
const CREATE_GAME_OPTION = 'create-game';
const DEFAULT_OPTION = '';

type codeType = {
    [key: string]: string
}

const MainDigitalGobuddyPage = () => {
    const history = useHistory();
    
    const [userOption, setUserOption] = useState<string>(DEFAULT_OPTION);
    const [code, setCode] = useState<codeType>(codeInit);
    const inputNum = useRef(0)
    const inputRef = useRef<HTMLInputElement>(null);
    const [errorMessage, setErrorMessage] = useState<string>('');
    const [loading, setLoading] = useState<boolean>(false);

    // Event listener handle left and right click
    useEffect(() => {
        const handleKeypress = (event: any) => {
            // left arrow
            const currentId = inputNum.current;
            if(event.keyCode === 37){
                changeFocus(currentId-1)
            }
            // right arrow
            if(event.keyCode === 39){
                changeFocus(currentId+1)
            }
        }
        window.addEventListener('keydown', handleKeypress);
    
        return () => {
          window.removeEventListener('keydown', handleKeypress);
        };
    }, []);

    const changeFocus = (focusId: number) => {
        if(focusId >= 0 && focusId < Object.keys(codeInit).length){
            const htmlElement = document.getElementById(ID_BASE+focusId.toString())
            htmlElement?.focus();
            inputNum.current = focusId;
            const htmlInputElement = htmlElement as HTMLInputElement
            if(htmlInputElement.value !== ''){
                htmlInputElement.select();
            }
        }
    }

    const onChangeGameCode = (event: any) => {
        console.log(inputRef)
        const name = event.target.name;
        const newCode: codeType = {...code, [name]: event.target.value};
        setCode(newCode);
        // console.log(newCode, codeIsValid(newCode));

        if(event.target.value){
            const nextId = parseInt(name) + 1
            changeFocus(nextId);
        }

        //Join game automatically when gamecode is valid
        if(codeIsValid(newCode)){
            joinGame(newCode);
        }
    }

    const codeIsValid = (object: codeType) => { 
        const keys = Object.keys(object);
        const filteredKeys = keys.filter(key => object[key] !== '');
        return filteredKeys.length === 8
    }

    const joinGame = (newCode: codeType) => {
        setLoading(true);
        //Create gamecode string
        const gameCode = Object.keys(newCode).map(key => newCode[key]).join('');
        const token = localStorage.getItem(GOBUDDY_LS_TOKEN);
        getQuizByCode(token, gameCode, 'play-quiz')
        .then(quiz => {
            setLoading(false);
            if(quiz._id){
                history.push('quiz/' + gameCode)
            }else{
                throw new Error('Could not find challenge with given id')
            }
        })
        .catch(error => {
            setLoading(false);
            setErrorMessage(error.message);
        })
    }

    const onClickButton = (option: string) =>{
        setErrorMessage('') //Reset errormessage
        if(userOption !== DEFAULT_OPTION){
            setUserOption(DEFAULT_OPTION); 
            setCode(codeInit); // reset code
        }else{
            setUserOption(option);
        }
    }

    let view = <Container fluid>
        <DigitalGobuddyButton icon={faUsers} onClick={(e: any) => onClickButton(JOIN_GAME_OPTION)}>Join Gobuddy</DigitalGobuddyButton>
        <DigitalGobuddyButton icon={faEdit} onClick={(e: any) => onClickButton(CREATE_GAME_OPTION)}>Create Gobuddy</DigitalGobuddyButton>
    </Container >
    switch(userOption){
        case JOIN_GAME_OPTION:
            view =  <Container fluid>
                <DigitalGobuddyButton icon={faUndo} onClick={(e: any) => onClickButton(JOIN_GAME_OPTION)}>Join Gobuddy</DigitalGobuddyButton>
                {/* TBD: Put this whole form inside a component.... aint nobody got time for that.*/}
                <Form className={classes.Form} >
                        <Form.Group className={classes.FormGroup}>
                            {
                                Object.keys(codeInit).map(key => {
                                    return  <Form.Control
                                                key={key}
                                                id={ID_BASE+key}
                                                type="text"
                                                className={[classes.Input, errorMessage ? classes.Error : ''].join(' ')}
                                                onChange={onChangeGameCode}
                                                onClick={(e: any )=> changeFocus(parseInt(key))}
                                                name={key}
                                                ref={inputRef}
                                                maxLength={1}
                                                autoComplete="off"
                                            />
                                })
                            }
                        </Form.Group>
                    </Form>
                </Container >
            break;
        case CREATE_GAME_OPTION:
            view = <Container fluid>
                <DigitalGobuddyButton icon={faUndo} onClick={(e: any) => onClickButton(CREATE_GAME_OPTION)}>Create Gobuddy</DigitalGobuddyButton>
                <AddQuizView />
            </Container >;
            break;
        default:
            view = <Container fluid>
            <DigitalGobuddyButton icon={faUsers} onClick={(e: any) => onClickButton(JOIN_GAME_OPTION)}>Join Gobuddy</DigitalGobuddyButton>
            <DigitalGobuddyButton icon={faEdit} onClick={(e: any) => onClickButton(CREATE_GAME_OPTION)}>Create Gobuddy</DigitalGobuddyButton>
        </Container >;
    }


    return (
        <Container fluid className={classes.MainDigitalGobuddyPage}>
            <NavigationBar />
            <Container fluid className={classes.FormContainer}>
                {loading ? <LoaderLogo></LoaderLogo> : <Logo></Logo>}
                <h1>GoBuddy</h1>
                <p>Bringing people together</p>
                {view}
                <p className={classes.ErrorMessage}>{errorMessage.trim()==='Not Found' ? 'Could not find quiz' :  errorMessage}</p>
            </Container>
        </Container>
    )
}

export default MainDigitalGobuddyPage
