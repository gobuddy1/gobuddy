import React from 'react'
import { Container } from 'react-bootstrap'

import classes from './CreateChallengePage.module.css'

import NavigationBar from '../../parts/NavigationBar/NavigationBar';

const CreateChallengePage = () => {
    return (
        <Container fluid className={classes.CreateChallengePage}>
            <NavigationBar />
            <div className={classes.CreateChallengePageHeader}>Create Challenge</div>
        </Container>
    )
}

export default CreateChallengePage
