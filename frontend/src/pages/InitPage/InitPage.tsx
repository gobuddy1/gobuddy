import React, { useEffect, useState, useRef } from 'react'
import { Container, Image, Navbar } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInstagram, faLinkedin, faFacebook } from '@fortawesome/free-brands-svg-icons'
import { faRocket, faGlobeEurope, faUsers } from '@fortawesome/free-solid-svg-icons'
import openSocket from 'socket.io-client'


import classes from './InitPage.module.css'
import NavigationBar from '../../parts/NavigationBar/NavigationBar';
import gobuddyWoman from '../../shared/gobuddy_woman.jpg'
import ButtonUI from '../../components/UI/ButtonUI/ButtonUI';
import { screen } from '@testing-library/react';


const InitPage = () => {

    const [scrollY, setScrollY] = useState(0);
    const HOST: string = process.env.REACT_APP_HOST + '';

    useEffect(() => {
        openSocket(HOST); //Establish connection between client and server through websocket.
    })

    useEffect(() => {
        window.addEventListener("scroll", scrollHandler, true);
    
        return () => {
          window.removeEventListener("scroll", scrollHandler, true);
        };
    }, []);

    const scrollHandler = (event: any) => {
        setScrollY(event.target.scrollTop);
    }
  
    return (
        <Container fluid className={classes.InitPage}>
            <NavigationBar className={scrollY === 0 ? classes.NavbarTop : classes.NavbarScroll} />
            <div className={classes.Overlay}></div>
            <Image className={classes.Image} src={gobuddyWoman}></Image>
            <Container fluid className={classes.TitleContainer}>
                <h1 className={classes.MainHeader}>Gobuddy</h1>
                <p className={classes.MainText}>Play games and have fun!</p>
                <ButtonUI className={classes.Button}>About Us</ButtonUI>
            </Container>
            <Container fluid className={classes.DescriptionContainer}>
                <Container fluid className={classes.Features}>
                    <Container className={classes.FeatureHeader}>
                        <h1 className={classes.DescriptionHeader}>How does it work?</h1>
                        <p className={classes.DescriptionBody}>We believe in the principles of having fun, being innovative and always challenge the status quo. That is why we are creating GoBuddy.</p>
                    </Container>
                    <Container className={classes.FeatureBox}>
                        <FontAwesomeIcon className={classes.Icon} icon={faRocket}/>
                        <h2 className={classes.FeatureTitle}>Play Games</h2>
                        <p className={classes.FeatureDescription}>At the metro station, waiting for a friend or taking a walk in your city - it does not matter! The Marketplays has a variety of games wherever you are, all you need is your phone.</p>
                    </Container>
                    <Container className={classes.FeatureBox}>
                        <FontAwesomeIcon className={classes.Icon} icon={faGlobeEurope}/>
                        <h2 className={classes.FeatureTitle}>Socialize</h2>
                        <p className={classes.FeatureDescription}>Invite your friends, share achievements and be a part of the community</p>
                    </Container>
                    <Container className={classes.FeatureBox}>
                        <FontAwesomeIcon className={classes.Icon} icon={faUsers}/>
                        <h2 className={classes.FeatureTitle}>Team Up</h2>
                        <p className={classes.FeatureDescription}>Be a part of the fun Marketplays team and create new challenges and games in your local city - on your own conditions</p>
                    </Container>
                </Container>
                <div className={classes.Separator}></div>
                <Container fluid className={classes.TeamArticle} >
                    <Container className={classes.FeatureHeader}>
                        <h1 className={classes.DescriptionHeader}>Meet our team</h1>
                    </Container>
                    <Container className={classes.FeatureBox}>
                        <FontAwesomeIcon className={classes.Icon} icon={faUsers}/>
                        <h2 className={classes.FeatureTitle}>Erik Högberg</h2>
                        <p className={classes.FeatureDescription}>By working in the surface between business and IT, Erik is driven by challenging the status quo and stimulate innovative and modern value propositions, creating new revenue streams as well as increased customer experience.</p>
                    </Container>
                    <Container className={classes.FeatureBox}>
                        <FontAwesomeIcon className={classes.Icon} icon={faUsers}/>
                        <h2 className={classes.FeatureTitle}>Eric Barclay</h2>
                        <p className={classes.FeatureDescription}>Eric is an executive leader in sales and marketing with advanced experience in strategy and digital business. Focusing on network orchestration, his strengths are within scalability, growth and long term execution.</p>
                    </Container>
                    <Container className={classes.FeatureBox}>
                        <FontAwesomeIcon className={classes.Icon} icon={faUsers}/>
                        <h2 className={classes.FeatureTitle}>Robin Moreno Rinding</h2>
                        <p className={classes.FeatureDescription}>Having experience from IT consulting, Robin bring deep knowledge in full stack development, data analytics and test automation. As the technical lead he focus on developing the platform while maintaining the technological vision of GoBuddy.</p>
                    </Container>
                </Container>
                <footer className={classes.Footer}>
                    <a href={'https://www.instagram.com/'}><FontAwesomeIcon className={classes.FaIcon} icon={faInstagram}/></a>
                    <a href={'https://www.linkedin.com/feed/'}><FontAwesomeIcon className={classes.FaIcon} icon={faLinkedin}/></a>
                    <a href={'https://www.facebook.com/'}><FontAwesomeIcon className={classes.FaIcon} icon={faFacebook}/></a>
                    <p className={classes.Text}>Copyright © Gobuddy. All rights reserved.</p>
                </footer>
            </Container>
        </Container>
    )
}

export default InitPage
