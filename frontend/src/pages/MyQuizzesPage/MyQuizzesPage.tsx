import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom';

import { Container } from 'react-bootstrap'
import NavigationBar from '../../parts/NavigationBar/NavigationBar'

import classes from './MyQuizzesPage.module.css'
import { IQuiz } from '../../shared/interfaces/quiz.interface';
import { GOBUDDY_LS_TOKEN } from '../../constants/localstoragenames';
import { deleteQuiz, getQuizzes } from '../../shared/backend/backend-service';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
import Modal from '../../components/UI/Modal/Modal';
import ButtonUI from '../../components/UI/ButtonUI/ButtonUI';

const MyQuizzesPage = () => {
    const history = useHistory();

    const [quizzes, setQuizzes] = useState<IQuiz[]>([]);
    const [showModal, setShowModal] = useState<boolean>(false);
    const [quizId, setQuizId] = useState<string>('');

    useEffect(() => {
        const token = localStorage.getItem(GOBUDDY_LS_TOKEN);
        getQuizzes(token)
        .then(quizzes => {
            if(quizzes?.quizzes.length > 0){
                setQuizzes(quizzes.quizzes);
            }else{
                throw new Error('Could not find quizzes with given id');
            }
        })
        .catch(error => {
            console.log(error)
        })
    }, [showModal])

    const editQuiz = (quizCode: string) => {
        history.push(`/quiz/add-questions/${quizCode}`);
    }

    const onOpenModal = (quizId: string) => {
        setShowModal(true);
        setQuizId(quizId);
    }
    const onCloseModal = () => {
        setShowModal(false);
        setQuizId('');
    }

    const onDeleteQuiz = () => {
        const token = localStorage.getItem(GOBUDDY_LS_TOKEN);
        const data = {
            quizId: quizId
        }
        deleteQuiz(token, data)
        .then(response => {
            if(response.quizId){
                setQuizId(''); // reset quizid
                setShowModal(false);
            }else{
                throw new Error('Could not delete quiz!')
            }
        })
        .catch(error => {
            // setErrorMessage(error.message);
            console.log(error.message);
        })
    }

    return (
        <Container fluid className={classes.MyQuizzesPage}>
            <NavigationBar />
            <Modal show={showModal} onClose={onCloseModal}>
                <Container fluid className={classes.Modal}>
                    <p className={classes.ModalText}>Do you really want to delete this quiz?</p>
                    <Container fluid className={classes.ModalButtonContainer}>
                        <ButtonUI className={classes.ModalButton} clicked={onCloseModal}>Cancel</ButtonUI>
                        <ButtonUI className={classes.ModalButton} clicked={onDeleteQuiz}>Yes</ButtonUI>
                    </Container>
                </Container>
            </ Modal>
            <Container fluid className={classes.FormContainer}>
                <h1 className={classes.Header}>My Gobuddy Digital Quizzes</h1>
                {quizzes.map(quiz => {
                    return <Container fluid className={classes.QuizTag} key={quiz._id} >
                        <p className={classes.QuizName}>
                            {quiz.name}
                        </p>
                        <Container fluid className={classes.IconContainer}>
                            <FontAwesomeIcon className={classes.Icon} icon={faEdit} onClick={(e: any) => editQuiz(quiz.code)}></FontAwesomeIcon>
                            <FontAwesomeIcon className={classes.Icon} icon={faTrash} onClick={(e: any) => onOpenModal(quiz._id)}></FontAwesomeIcon>
                            <p className={classes.Icon}>
                                {quiz.code}
                            </p>
                        </Container>
                    </Container>
                })}
            </Container>
        </Container>
    )
}

export default MyQuizzesPage
