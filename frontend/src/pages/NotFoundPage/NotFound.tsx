import React from 'react'
import { Container } from 'react-bootstrap';

import classes from './NotFound.module.css'

const NotFound = () => {
    return (
        <Container fluid className={classes.NotFound}>
            <div className={classes.NotFoundText}>
                404 Not Found
            </div>
        </Container>
    )
}

export default NotFound
