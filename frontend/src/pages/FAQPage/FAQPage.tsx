import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React, { useState, useEffect } from 'react'
import Container from 'react-bootstrap/esm/Container'

import classes from './FAQPage.module.css'
import { faQuestion, faExclamation, faUndo } from '@fortawesome/free-solid-svg-icons';
import { FAQContent, IFAQ } from '../../shared/content/FAQContent';
import { ListGroup } from 'react-bootstrap';


const FAQPage = () => {

    const [ answersShown, setAnswersShown ] = useState<number[]>([]);
    const [ selectedPage, setSelectedPage ] = useState<string>('');
    const [ content, setContent ] = useState<IFAQ[]>([]);

    useEffect(() => {
        let newContent: IFAQ[] = [];
        if(selectedPage){
            newContent = FAQContent.pages[selectedPage.toLowerCase()].faqs
        }else{
            newContent = [];
        }
        setContent(newContent)
    }, [selectedPage])

    const onTogglePage = (e: any) => {
        if(!selectedPage){
            setSelectedPage(e.target.id);
        }else{
            setSelectedPage('');
            setAnswersShown([]);
        }
    }

    const toggleShowAnswer = (e: any, index: number) => {
        let newAnswersShown = [...answersShown];
        if(answersShown.indexOf(index) > -1){
            newAnswersShown = newAnswersShown.filter((item) => item !== index);
        }else{
            newAnswersShown.push(index);
        }
        setAnswersShown(newAnswersShown);
    }

    let view = <Container fluid className={classes.QAContainer}>
        <ListGroup>
            {Object.keys(FAQContent.pages).map((key) => {
                    const name = FAQContent.pages[key].name;
                    return <ListGroup.Item onClick={onTogglePage} key={name} id={name} className={classes.ListItem}>{name}</ListGroup.Item>
            })}
        </ListGroup>
    </Container>
    if(selectedPage){
        view = <Container fluid className={classes.QAContainer}>
                <Container fluid className={classes.QAGroup}>
                    <ListGroup>
                        {content.map((faq, index) => {
                            return <Container fluid key={index}>
                                <ListGroup.Item onClick={(e: any) => toggleShowAnswer(e, index)} className={classes.QuestionListItem}>
                                    <FontAwesomeIcon icon={faQuestion} />
                                    <p className={classes.Paragraph}>{faq.question}</p>
                                </ListGroup.Item>
                                {(answersShown.indexOf(index) > -1) ? <ListGroup.Item className={classes.AnswerListItem}>
                                    <FontAwesomeIcon icon={faExclamation} />
                                    <p className={classes.Paragraph}>{faq.answer}</p>
                                </ListGroup.Item> : null}
                            </Container>
                        })}
                    </ListGroup>
                </Container>
        </Container>
    }

    return (

        <Container fluid className={classes.FAQContainer}>
            <Container fluid className={classes.Header}>
                {selectedPage ? 
                    <FontAwesomeIcon icon={faUndo} onClick={onTogglePage} className={classes.ReturnIcon} /> : 
                    <h1 className={classes.HeaderClass}>FAQ</h1>
                }
            </Container>
            {view}
        </Container>
    )
}

export default FAQPage
