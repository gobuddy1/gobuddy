import React, { useEffect, useState } from 'react'
import { useParams, useHistory } from 'react-router-dom';
import { Container, Image } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';

import classes from './AddQuizQuestionPage.module.css';

import NavigationBar from '../../parts/NavigationBar/NavigationBar';
import { GOBUDDY_LS_TOKEN } from '../../constants/localstoragenames';
import { getQuizByCode } from '../../shared/backend/backend-service';
import { IQuiz } from '../../shared/interfaces/quiz.interface';
import { Question, IQuestion } from '../../shared/interfaces/question.interface';

interface IParams{
    quizCode: string
}

const AddQuizQuestionPage = () => {

    const params = useParams<IParams>();
    const history = useHistory();
    const gameCode = params.quizCode;

    const [quiz, setQuiz] = useState<IQuiz>();

    // Execute upon changing gameCode
    useEffect(() => {
        const token = localStorage.getItem(GOBUDDY_LS_TOKEN);
        getQuizByCode(token, gameCode, 'edit-quiz')
        .then(quiz => {
            if(quiz._id){
                setQuiz(quiz);
            }else{
                throw new Error('Could not find challenge with given id');
            }
        })
        .catch(error => {
            console.log(error)
        })
    }, [gameCode])

    const OnEditQuestion = (question: IQuestion) => {
        history.push('edit-question/' + quiz?._id + '/' + question._id);
    }

    let view = <Container fluid>
                <h1 className={classes.Header}>{quiz?.name}</h1>
                {quiz?.questions.map(question => {
                    return <Container fluid className={classes.QuestionTag} key={question._id} onClick={(e: any) => OnEditQuestion(question)}>
                        <Image src={`${process.env.REACT_APP_HOST}${question.image}`} className={classes.Image}></Image>
                        <p className={classes.QuestionName}>
                            {question.question}
                        </p>
                    </Container>
                })}
                <Container fluid className={[classes.AddQuestionTag, classes.QuestionTag].join(' ')} onClick={(e: any) => OnEditQuestion(new Question())}>
                    <FontAwesomeIcon icon={faPlusCircle} />
                    <p className={classes.QuestionName}>Add Question</p>
                </Container>
    </Container>
    
    return (
        <Container fluid className={classes.AddQuizQuestionPage}>
            <NavigationBar />
            <Container fluid className={classes.FormContainer}>
                {view}
            </Container>
        </Container>
    )
}

export default AddQuizQuestionPage
