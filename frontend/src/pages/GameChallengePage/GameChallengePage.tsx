import React, { useState } from 'react'
import { useHistory } from 'react-router-dom';
import { Container, Form } from 'react-bootstrap';
import { useParams } from 'react-router-dom'

import { GOBUDDY_LS_TOKEN } from '../../constants/localstoragenames';
import NavigationBar from '../../parts/NavigationBar/NavigationBar';
import classes from './GameChallengePage.module.css'
import ButtonUI from '../../components/UI/ButtonUI/ButtonUI';
import { getChallengeByCode } from '../../shared/backend/backend-service';

const GameChallengePage = () => {
    const history = useHistory();
    const [gameCode, setGameCode] = useState('');
    const [error, setError] = useState('')

    const onChangeGameCode = (event: any) => {
        setGameCode(event.target.value)
    }

    const onJoinGame = () => {
        const token = localStorage.getItem(GOBUDDY_LS_TOKEN);
        getChallengeByCode(token, gameCode)
        .then(challenge => {
            if(challenge._id){
                console.log('challengio', challenge)
                history.push('game/' + gameCode)
            }else{
                throw new Error('Could not find challenge with given id')
            }
        })
        .catch(error => {
            setError(error.message);
        })
    }

    return (
        <Container fluid className={classes.GameChallengePage}>
            <NavigationBar />
            <Container className={classes.GameChallengePageContainer}>
                <p className={classes.ErrorMsg}>{error}</p>
                <Form>
                    <Form.Group controlId="input">
                        <Form.Control 
                            type="text"
                            className={classes.Input}
                            onChange={onChangeGameCode}
                        />
                    </Form.Group>
                </Form>
                <ButtonUI test-dataid="test" clicked={onJoinGame} disabled={!gameCode}>Join</ButtonUI>
            </Container>
        </Container>
    )
}

export default GameChallengePage
