import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import ReactMapGL from 'react-map-gl';
import { Container } from 'react-bootstrap';

import { ILocation } from '../../shared/interfaces/location.interface'

import { GOBUDDY_LS_TOKEN } from '../../constants/localstoragenames';
import { getLocations, getChallengeByCode } from '../../shared/backend/backend-service';
import NavigationBar from '../../parts/NavigationBar/NavigationBar';
import classes from './SingleGamePage.module.css'
import MapMarker from '../../components/MapMarker/MapMarker';
import { USER_LOCATION, GAMES } from '../../constants/locationtypes'
import LoaderModal from '../../components/LoaderModal/LoaderModal';
import { MAP_STYLE_DARK } from '../../shared/styles/map-style'
import { isNearGame } from '../../util/mapfunctions/mapfunctions';
import LocationInformation from '../../components/LocationInformation/LocationInformation';

interface IParams{
    gameId: string
}

const SingleGamePage = () => {
    const options = {
        enableHighAccuracy: true,
        maximumAge: 30000,
        timeout: 27000
      };
    const params = useParams<IParams>();
    const [locations, setLocations] = useState<ILocation[]>([]);
    const [selectedLocation, setSelectedLocation] = useState<ILocation>();
    const [challengeFound, setChallengeFound] = useState<boolean>();
    const [viewport, setViewport] = useState({
        width: '100vw',
        height: '100vh',
        latitude: 0,
        longitude: 0,
        zoom: 15
    })
    const [loading, setLoading] = useState<boolean>(true);
    const [loadingMessage, setLoadingMessage] = useState<string>('Loading map...')
    const [currentPosition, setCurrentPosition] = useState<ILocation>({
        longitude: 0, 
        latitude: 0,
        name: 'user-location',
        description: 'user-location',
        range: 10,
        _id: 'user-location',
        questions: []
    })

    //Retreive locations for given challenge
    useEffect(() => {
        updateLocations();
    }, [])

    useEffect(() => {
        // Updates user position
        // can use watchPosition
        navigator.geolocation.getCurrentPosition((position) => {
            const newPosition = {...currentPosition}
            newPosition.latitude = position.coords.latitude
            newPosition.longitude = position.coords.longitude
            setCurrentPosition(newPosition)
        },
        () => {
            console.log('No position available');
            // setLoading(true)
            // setLoadingMessage('No position available')
        }, options)
    });

    const updateLocations = () => {
        const token = localStorage.getItem(GOBUDDY_LS_TOKEN);
        getChallengeByCode(token, params.gameId)
        .then(challenge => {
            return getLocations(token, challenge._id)
        })
        .then(locations => {
            setLocations([...locations.locations]);
            const initLocation = locations.locations[0] ? locations.locations[0] : currentPosition; //Change this logic...
            let newViewport = {...viewport, latitude: initLocation.latitude, longitude: initLocation.longitude}
            setViewport(newViewport);
            setChallengeFound(true);
            setLoading(false);
        })
        .catch(error => {
            console.log(error);
            setChallengeFound(false)
        })
    }

    const onChangingViewPort = (viewport: any) => {
        const width: string = '100vw';
        const height: string = '100vh';
        const latitude: number = viewport.latitude;
        const longitude: number = viewport.longitude;
        const zoom: number = viewport.zoom;
        const newViewport = {width: width, height: height, latitude: latitude, longitude: longitude, zoom: zoom}
        setViewport(newViewport);
    }

    const selectLocation = (event: any, location?: ILocation) => {
        setSelectedLocation(location);
    }

    const onCloseModal = () => {
        setSelectedLocation(undefined)
    }


    return (
        <Container fluid className={classes.SingleGamePage}>
            <NavigationBar className={classes.Navigationbar}/>
            {loading ? <LoaderModal message={loadingMessage}/> : null}
            <LocationInformation userLoc={currentPosition} selLoc={selectedLocation} show={selectedLocation !== undefined} onClose={onCloseModal}/>
            <ReactMapGL
                {...viewport}
                mapboxApiAccessToken={process.env.REACT_APP_MAPBOX_ACCESS_TOKEN}
                mapStyle={MAP_STYLE_DARK}
                onViewportChange={onChangingViewPort}
                >
                    <MapMarker 
                        key={'user-key'}
                        long={currentPosition.longitude}
                        lat={currentPosition.latitude}
                        location={currentPosition}
                        type={USER_LOCATION}
                        unlocked={false}
                        offsetTop={0}
                        offsetLeft={0}
                    ></MapMarker>
                    {locations.map(location => {
                    return <MapMarker
                        key={location._id}
                        id={'question-location-marker'}
                        location={location}
                        long={location.longitude}
                        lat={location.latitude}
                        type={GAMES}
                        unlocked={isNearGame(currentPosition, location)}
                        offsetLeft={-22}
                        offsetTop={-22}
                        onClick={event => selectLocation(event, location)}
                        selectedLocation={selectedLocation}
                    ></MapMarker>
                })}
            </ReactMapGL>
        </Container>
    )
}

export default SingleGamePage;