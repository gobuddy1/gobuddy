import React, {useState, useEffect} from 'react';
import ReactMapGL from 'react-map-gl';

import MapMarker from '../../components/MapMarker/MapMarker'
import { USER_LOCATION, GAMES } from '../../constants/locationtypes'
import { isNearGame } from '../../util/mapfunctions/mapfunctions';
import NavigationBar from '../../parts/NavigationBar/NavigationBar';

const MAP_STYLE = "mapbox://styles/romori/cki3k629c4la719lrdw3568q9";

type geoMarker = {
    id: string,
    longitude: number,
    latitude: number
}

const MapPage = () => {
    const [viewport, setViewport] = useState({
        width: '100vw',
        height: '100vh',
        latitude: 0,
        longitude: 0,
        zoom: 15
    })

    const [currentPosition, setCurrentPosition] = useState({
        latitude: 0,
        longitude: 0
    })

    const [gameMarkers, setGameMarkers] = useState<geoMarker[]>([])

    //Uppdatera med timer? --Robin
    useEffect(() => {
        // Updates user position
        navigator.geolocation.getCurrentPosition((position) => {
            const newPosition = {...currentPosition}
            newPosition.latitude = position.coords.latitude
            newPosition.longitude = position.coords.longitude
            setCurrentPosition(newPosition)
        })
    });

    useEffect(() => {
        // Initiates user location
        console.log('Initiate viewport with user location')
        navigator.geolocation.getCurrentPosition((position) => {
            let newViewport = {...viewport, latitude: position.coords.latitude, longitude: position.coords.longitude}
            setViewport(newViewport)
        })
    }, []);

    const onClickMap = (event: any) => {
        const lngLat = event.lngLat;
        setGameMarkers([...gameMarkers, {id: Math.random().toString(), longitude: lngLat[0], latitude: lngLat[1]}]);
    }

    return (
        <div>
            <NavigationBar/>
            <div style={{width: '100vw', height: '100vh'}}>
                <ReactMapGL
                    {...viewport}
                    mapboxApiAccessToken={process.env.REACT_APP_MAPBOX_ACCESS_TOKEN}
                    mapStyle={MAP_STYLE}
                    onViewportChange={viewport => {
                        const width: string = '100vw';
                        const height: string = '100vh';
                        const latitude: number = viewport.latitude;
                        const longitude: number = viewport.longitude;
                        const zoom: number = viewport.zoom;
                        const newViewport = {width: width, height: height, latitude: latitude, longitude: longitude, zoom: zoom}
                        setViewport(newViewport);
                    }}
                    onClick={(e) => onClickMap(e)}
                    getCursor={(e) => "crosshair"}>
                    {/* <MapMarker 
                        key={'user-key'}
                        long={currentPosition.longitude}
                        lat={currentPosition.latitude}
                        type={USER_LOCATION}
                        unlocked={false}
                        offsetTop={0}
                        offsetLeft={0}
                    ></MapMarker>
                    {gameMarkers.map(position => {
                        const gameIsNearlayer: boolean = isNearGame({
                            long: currentPosition.longitude, 
                            lat: currentPosition.latitude
                        },
                        {
                            long: position.longitude,
                            lat: position.latitude
                        })
                        return <MapMarker
                            key={position.id}
                            long={position.longitude}
                            lat={position.latitude}
                            type={GAMES}
                            unlocked={gameIsNearlayer}
                            offsetTop={0}
                            offsetLeft={0}
                        ></MapMarker>
                    })} */}
                </ReactMapGL>
            </div>
        </div>
    );
}

export default MapPage;
