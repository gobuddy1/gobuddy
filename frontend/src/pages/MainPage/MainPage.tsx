import React, { useEffect, useState } from 'react'
import { Container, Row, Col } from 'react-bootstrap';

import { getPublicChallenges } from '../../shared/backend/backend-service';
import { GOBUDDY_LS_TOKEN } from '../../constants/localstoragenames';
import NavigationBar from '../../parts/NavigationBar/NavigationBar';
import classes from './MainPage.module.css';
import Challenge from '../../components/Challenge/Challenge';
import { IChallenge } from '../../shared/interfaces/challenge.interface';

function Startpage() {
    const [publicChallenges, setPublicChallenges] = useState<IChallenge[]>([]);

    useEffect(() => {
        const token = localStorage.getItem(GOBUDDY_LS_TOKEN);
        getPublicChallenges(token)
            .then(challenges => {
                setPublicChallenges(challenges.challenges);
                console.log(challenges)
            })
            .catch(error => {
                console.log(error);
            })

    }, [])

    return (
        <Container fluid className={classes.MainPage}>
        <NavigationBar />
            <div className={classes.MainPageHeader}>GoBuddy Challenges</div>
            <Container fluid className={classes.Challenges}>
                <Row>
                        {publicChallenges.map(challenge => {
                            return <Col md={3} key={challenge._id}><Challenge 
                                challenge={challenge}
                            /></Col>
                        })}
                </Row>
            </Container>
        </Container>
    )
}

export default Startpage
