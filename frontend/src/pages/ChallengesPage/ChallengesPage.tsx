import React, { useState, useEffect } from 'react'
import { Container, Row, Col } from 'react-bootstrap';
import { v4 as uuidv4 } from 'uuid';

import classes from './ChallengesPage.module.css'
import { getChallenges } from '../../shared/backend/backend-service';
import NavigationBar from '../../parts/NavigationBar/NavigationBar';
import { GOBUDDY_LS_TOKEN } from '../../constants/localstoragenames';
import Challenge from '../../components/Challenge/Challenge';
import { IChallenge } from '../../shared/interfaces/challenge.interface';
import MapPreview from '../../components/MapPreview/MapPreview';
import AddChallengeButton from '../../components/UI/AddChallengeButton/AddChallengeButton';
import AddChallengeView from '../../components/AddChallengeView/AddChallengeView';
import SelectedChallenge from '../../components/SelectedChallenge/SelectedChallenge';

const ChallengesPage = () => {
    
    const [challenges, setChallenges] = useState<IChallenge[]>([]);
    const [showAddChallenge, setShowAddChallenge] = useState<boolean>(false);
    const [selectedChallenge, setSelectedChallenge] = useState<IChallenge>({name: '', description: '', locations: [], createdby: '', _id: '', public: false})

    useEffect(() => {
        const token = localStorage.getItem(GOBUDDY_LS_TOKEN);
        getChallenges(token)
            .then(challenges => {
                setChallenges(challenges.challenges);
                console.log(challenges)
            })
            .catch(error => {
                console.log(error);
            })

    }, [])

    const onShowAddChallenge = () => {
        setShowAddChallenge(true);
    }

    const onCloseAddChallenge = () => {
        setShowAddChallenge(false);
    }

    const onSelectChallenge = (event: any, challenge: IChallenge) => {
        console.log(challenge)
        setSelectedChallenge(challenge)
    }

    return (
        <Container fluid className={classes.ChallengePage}>
            <NavigationBar />
            <Container fluid className={classes.Container}>
                <Container fluid className={classes.Challenges}>
                        <Row>
                            {selectedChallenge.name !== '' ? <SelectedChallenge key={uuidv4()} challenge={selectedChallenge} /> : null}
                            {challenges.filter(challenge => selectedChallenge._id !== challenge._id).map(challenge => {
                                return <Col md={4} key={challenge._id} className={classes.Column}>
                                        <Challenge challenge={challenge} onClick={e => onSelectChallenge(e, challenge)}/>
                                    </Col>
                            })}
                            <Col  md={4} className={classes.Column} >
                                <AddChallengeButton onClick={onShowAddChallenge}/>
                            </Col>
                        </Row>
                </Container>
                <Container fluid id={'searchbar-container'} className={classes.SearchBar}>
                    <MapPreview 
                        challenge={selectedChallenge}
                        longitude={selectedChallenge.locations[0] ? selectedChallenge.locations[0].longitude : 0}
                        latitude={selectedChallenge.locations[0] ? selectedChallenge.locations[0].latitude : 0} />
                </Container>
            </Container>
            <AddChallengeView onClick={onCloseAddChallenge} show={showAddChallenge}/>
        </Container>
    )
}

export default ChallengesPage
