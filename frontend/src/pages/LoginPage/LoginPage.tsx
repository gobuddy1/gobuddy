import React from 'react'
import { Row, Container } from 'react-bootstrap'

import Login from '../../components/Auth/Login/Login';
// import Logo from '../../components/Logo/Logo';
import classes from './LoginPage.module.css';

function LoginPage() {
    return (
        <Container fluid className={classes.LoginPage}>
            <Row className="justify-content-center">
                <Login/>
            </Row>
        </Container>
    )
}

export default LoginPage
