import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import ReactMapGL from 'react-map-gl';
import { Container } from 'react-bootstrap';

import { ILocation } from '../../shared/interfaces/location.interface'

import { GOBUDDY_LS_TOKEN } from '../../constants/localstoragenames';
import { getLocations, postLocation } from '../../shared/backend/backend-service';
import NavigationBar from '../../parts/NavigationBar/NavigationBar';
import classes from './SingleChallengePage.module.css'
import MapMarker from '../../components/MapMarker/MapMarker';
import { USER_LOCATION, GAMES } from '../../constants/locationtypes'
import ChallengeSideBar from '../../components/ChallengeSideBar/ChallengeSideBar';
import LoaderModal from '../../components/LoaderModal/LoaderModal';
import { MAP_STYLE_DARK, MAP_STYLE_LIGHT } from '../../shared/styles/map-style'
import { LOCATION_DESCRIPTION, LOCATION_NAME } from '../../shared/formid/add-location-form';

// const MAP_STYLE_DARK = process.env.REACT_APP_MAPBOX_STYLE_DARK;
// const MAP_STYLE_LIGHT = process.env.REACT_APP_MAPBOX_STYLE_LIGHT;

interface IParams{
    gameId: string
}

const SingleChallengePage = (props: any) => {
    const params = useParams<IParams>();
    const [locations, setLocations] = useState<ILocation[]>([]);
    const [currentRange, setRange] = useState<number>(0);
    const [selectedLocation, setSelectedLocation] = useState<ILocation>();
    const [challengeFound, setChallengeFound] = useState<boolean>();
    const [cursor, setCursor] = useState<string>('');
    const [viewport, setViewport] = useState({
        width: '100vw',
        height: '100vh',
        latitude: 0,
        longitude: 0,
        zoom: 15
    })
    const [loading, setLoading] = useState<boolean>(true);
    const [loadingMessage, setLoadingMessage] = useState<string>('Loading map...')
    const [mapStyle, setMapStyle] = useState(MAP_STYLE_DARK);
    const [locationName, setLocationName] = useState('');
    const [locationDescription, setLocationDescription] = useState('');

    //Retreive locations for given challenge
    useEffect(() => {
        updateLocations();
    }, [])

    useEffect(() => {
        // Initiates user location
        console.log('Initiate viewport with user location')
        navigator.geolocation.getCurrentPosition(
            (position) => {
            let newViewport = {...viewport, latitude: position.coords.latitude, longitude: position.coords.longitude}
            setViewport(newViewport);
            console.log('position', position);
            },
            (error) => {
                console.warn('Could not find user position!', error.message)
            })
            console.log('Done updating position')

    }, []);

    const updateLocations = () => {
        const token = localStorage.getItem(GOBUDDY_LS_TOKEN);
        getLocations(token, params.gameId)
        .then(locations => {
            setLocations([...locations.locations]);
            console.log('Updating locations', locations)
            setChallengeFound(true);
            setLoading(false);
        })
        .catch(error => {
            console.log(error);
            setChallengeFound(false)
        })
    }

    const createLocation = (lngLat: Array<number>) => {
        setLoadingMessage('Creating location...')
        setLoading(true);
        const token = localStorage.getItem(GOBUDDY_LS_TOKEN);
        const data = {
            name: locationName,
            description: locationDescription,
            latitude: lngLat[1],
            longitude: lngLat[0],
            range: currentRange,
            gameId: params.gameId
        }
        postLocation(token, data)
        .then(response => {
            //added the following logic in order to re-render page upon creating new location.
            const locationId = response.locationId;
            let newLocations = [...locations];
            const newLocation = {_id: locationId, name: data.name, description: data.description, latitude: data.latitude, longitude: data.longitude, range: data.range, questions: []}
            newLocations.push(newLocation)
            setLocations([...newLocations]);
            setRange(0); // Reset range
            setCursor('') //Reset cursor
            setLoadingMessage('')
            setLoading(false);
            setSelectedLocation(newLocation)
        })
        .catch(error => {
            console.log(error);
            setCursor('') //Reset cursor
        })
    }

    const onChangingViewPort = (viewport: any) => {
        const width: string = '100vw';
        const height: string = '100vh';
        const latitude: number = viewport.latitude;
        const longitude: number = viewport.longitude;
        const zoom: number = viewport.zoom;
        const newViewport = {width: width, height: height, latitude: latitude, longitude: longitude, zoom: zoom}
        setViewport(newViewport);
    }

    const onClickMap = (event: any) => {
        if(currentRange !== 0 && locationName !== '' && locationDescription !== ''){
            const lngLat = event.lngLat;
            createLocation(lngLat);
        }
    }

    const selectLocation = (event: any, location?: ILocation) => {
        setSelectedLocation(location);
    }

    const unSelectLocation = () => {
        setSelectedLocation(undefined);
    }

    const selectLocationRange = (event: any, range: number) => {
        if(currentRange === range || range === 0){
            setRange(0);
            setCursor('')
        }else{
            setRange(range);
            setCursor('crosshair');
        }
    }

    //calls every time input changes
    const addLocationInputHandler = (event: any) => {
        const value = event.target.value;
        const id = event.target.id;
        if(id === LOCATION_NAME){
            setLocationName(value);
        }
        else if(id === LOCATION_DESCRIPTION){
            setLocationDescription(value);
        }
    }

    
    const toggleMapStyle = () => {
        console.log('mapstyle')
        if(mapStyle === MAP_STYLE_DARK){
            setMapStyle(MAP_STYLE_LIGHT);
        }else{
            setMapStyle(MAP_STYLE_DARK);
        }
    }

    const deleteLocation = () => {
        console.log('Deleting location placeholder')
    }

    const editLocation = () => {
        console.log('Editing location placeholder')
    }

    return (
        <Container fluid className={classes.SingleChallengePage}>
            
            <NavigationBar className={classes.Navigationbar}/>
            {/* <ChallengeToolbar
            addLocationControl={toggleAddLocationControl}
            active={activeButton}
            locationMenuOpen={true}/> */}
            {loading ? <LoaderModal message={loadingMessage}/> : null}
            <ChallengeSideBar 
                location={selectedLocation}
                unSelectLocation={unSelectLocation}
                selectLocationRange={selectLocationRange}
                addLocationInputHandler={addLocationInputHandler}
                editLocation={editLocation}
                deleteLocation={deleteLocation}
                range={currentRange}
                toggleMapStyle={toggleMapStyle}
                mapStyle={mapStyle}/>
            <ReactMapGL
                {...viewport}
                mapboxApiAccessToken={process.env.REACT_APP_MAPBOX_ACCESS_TOKEN}
                mapStyle={mapStyle}
                getCursor={(e) => cursor}
                onViewportChange={onChangingViewPort}
                onClick={(e) => onClickMap(e)}>
                    {locations.map(location => {
                    return <MapMarker
                        key={location._id}
                        id={'question-location-marker'}
                        location={location}
                        long={location.longitude}
                        lat={location.latitude}
                        type={GAMES}
                        unlocked={true}
                        offsetLeft={-22}
                        offsetTop={-22}
                        onClick={event => selectLocation(event, location)}
                        selectedLocation={selectedLocation}
                    ></MapMarker>
                })}
            </ReactMapGL>
        </Container>
    )
}

export default SingleChallengePage
