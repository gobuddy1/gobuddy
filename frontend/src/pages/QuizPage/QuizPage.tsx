import React, { useContext, useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';
import { Container } from 'react-bootstrap';

import NavigationBar from '../../parts/NavigationBar/NavigationBar';
import Logo from '../../components/Logo/Logo';

import classes from './QuizPage.module.css'
import { GOBUDDY_LS_TOKEN, GOBUDDY_LS_UUID, GOBUDDY_LS_USER } from '../../constants/localstoragenames';
import { getQuizByCode } from '../../shared/backend/backend-service';
import { IQuiz } from '../../shared/interfaces/quiz.interface';
import ButtonUI from '../../components/UI/ButtonUI/ButtonUI';
import { MainContext } from '../../context/MainContext';
import { SocketContext } from '../../context/socketContext';
import { UsersContext } from '../../context/UsersContext/usersContext';
import { QuestionContext } from '../../context/QuestionContext/QuestionContext';
import QuizView from '../../components/QuizView/QuizView';
import { IAnswer, IQuestion } from '../../shared/interfaces/question.interface';
import RoomCodeDisplay from '../../components/UI/RoomCodeDisplay/RoomCodeDisplay';
import { TimeCountContext } from '../../context/TimeCountContext';
import HighScoreBoard from '../../components/HighScoreBoard/HighScoreBoard';

interface IParams{
    quizCode: string
}

const QuizPage = () => {
    // Contexts
    const socket = useContext(SocketContext);
    const mainContext = useContext(MainContext);
    const { putUsers, users } = useContext(UsersContext)
    const { putQuestion, question, putLockedAnswer, putShowCorrectAnswer, showDescription, putShowDescription } = useContext(QuestionContext)
    const { putCounter, counter } = useContext(TimeCountContext)

    const params = useParams<IParams>();
    const gameCode = params.quizCode;
    const [ quiz, setQuiz ] = useState<IQuiz>();
    const [uuid, setUuid] = useState(localStorage.getItem(GOBUDDY_LS_UUID));
    const [questionCount, setQuestionCount] = useState<number>(-1);
    const [showScore, setShowScore] = useState<boolean>(false);

    // Execute once
    useEffect(() => {
        const token = localStorage.getItem(GOBUDDY_LS_TOKEN);
        const localStorageUser = localStorage.getItem(GOBUDDY_LS_USER);
        if(localStorageUser && token){
            console.log(JSON.parse(localStorageUser));
            mainContext.putName(JSON.parse(localStorageUser).firstname + ' ' + JSON.parse(localStorageUser).lastname)
            mainContext.putRoom(gameCode)
            mainContext.putToken(token)
            mainContext.putAvatarImage(JSON.parse(localStorageUser).avatarimage)
        }else{
            throw new Error('A token could not be fetched')
        }
    }, [])

    useEffect(() => {
        socket.on("users", (users: any) => {
            putUsers(users);
        })
    })

    // This is executed on changing question
    useEffect(() => {
        socket.on("question", (newQuestion: IQuestion) => {
            if(newQuestion !== question){
                putQuestion(newQuestion);
                putShowCorrectAnswer(false);
                putLockedAnswer({_id: undefined, answer: '', correct: false});
                putShowDescription(true)
                // if(counter === 0){
                //     putCounter(10);
                // }
                setQuestionCount(questionCount+1)
            }
        })
    })

    useEffect(() => {
        socket.on("showDescription", (showDescription: any) => {
            putShowDescription(showDescription.showDesc);
            if(counter === 0){
                putCounter(10);
            }
        })
    })

    useEffect(() => {
        if(quiz){
            putQuestion(quiz.questions[questionCount])
        }
    }, [questionCount])

    // Execute upon changing name and room 
    useEffect(() => {
        const token = localStorage.getItem(GOBUDDY_LS_TOKEN);
        getQuizByCode(token, gameCode, 'play-quiz')
        .then(quiz => {
            if(quiz._id){
                setQuiz(quiz);
                // handleCountUser();
                const name = mainContext.name
                const room = mainContext.room
                const token = mainContext.token
                const avatarImage = mainContext.avatarImage
                socket.emit('login', { name, room, token, avatarImage }, (error: any) => {
                    if(error){
                        console.log(error);
                    }
                })
            }else{
                throw new Error('Could not find challenge with given id')
            }
        })
        .catch(error => {
            console.log(error)
        })
    }, [mainContext.name, mainContext.room])

    const changeQuestion = () => {
        if(quiz){
            if(questionCount < quiz.questions.length - 1){
                socket.emit('changeQuestion', quiz.questions[questionCount+1])
            }
        }
    }

    const showQuestion = () => {
        // putShowDescription(false);
        const showDesc = false;
        socket.emit('setShowDescription', { showDesc }, (error: any) => {
            if(error){
                console.log(error);
            }
        })
    }

    let button = null;
    if(quiz?.createdby.toString() === uuid?.toString()){
        if(!question._id){
            button = <ButtonUI className={classes.Button} clicked={changeQuestion}>Start Gobuddy</ButtonUI>
        }else if(question && showDescription){
            button = <ButtonUI className={classes.Button} clicked={showQuestion}>Continue</ButtonUI>
        }
        else if(question && counter === 0 && quiz?.questions.length !== questionCount+1){
            button = <ButtonUI className={classes.Button} clicked={changeQuestion}>Next Question</ButtonUI>
        }

    }
    if(question && counter === 0 && quiz?.questions.length === questionCount+1 && !showDescription){
        button = <ButtonUI className={classes.Button} clicked={() => setShowScore(true)}>Show Score</ButtonUI>
    }

    let content = null;
    if(showScore){
        content = <HighScoreBoard users={users} token={mainContext.token}/>
    }else{
        content = <Container fluid className={classes.Container}>
                    {question?._id ? <p>Question {questionCount + 1} of {quiz?.questions.length}</p> : null}
                    <h1>{quiz?.name}</h1>
                    <QuizView quiz={quiz} />
                    {button}
                </Container>
    }

    return (
        <Container fluid className={showScore ? classes.QuizPageAnimated : classes.QuizPage}>
            <NavigationBar className={classes.Navigationbar}/>
            <RoomCodeDisplay />
            <Container fluid className={classes.QuizContainer}>
                {content}
            </Container>
        </Container>
    )
}

export default QuizPage
