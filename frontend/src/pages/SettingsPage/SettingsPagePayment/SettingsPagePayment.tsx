import React from 'react'
import { Container } from 'react-bootstrap';
import classes from './SettingsPagePayment.module.css'

import SettingsPage from '../SettingsPage'

const SettingsPagePayment = () => {
    return (
        <SettingsPage settingItem={'Payment'}>
            <div>
                Payment
            </div>
        </SettingsPage>
    )
}

export default SettingsPagePayment
