import React from 'react'
import { Container } from 'react-bootstrap';
import classes from './SettingsPageAccount.module.css'

import SettingsPage from '../SettingsPage'

const SettingsPageAccount = () => {
    return (
        <SettingsPage settingItem={'Account'}>
            <div>
                Account
            </div>
        </SettingsPage>
    )
}

export default SettingsPageAccount
