import React, { useState } from 'react'

import { Container } from 'react-bootstrap'

import classes from './SettingsPage.module.css'

import NavigationBar from '../../parts/NavigationBar/NavigationBar';
import SettingsSelector from '../../components/SettingsSelector/SettingsSelector';

const MENU = ['Profile', 'Photo', 'Account', 'Payment']

type SettingsPageProps = {
    children: JSX.Element,
    settingItem: string
}

const SettingsPage = (props: SettingsPageProps) => {

    return (
        <Container fluid className={classes.SettingsPage}>
            <NavigationBar />
            <Container className={classes.SettingsContainer}>
                <Container className={classes.SettingsHeader}>
                    <div>{props.settingItem}</div>
                </Container>
                <Container className={classes.SettingsBody}>
                    {MENU.map(menuItem => {
                        return <SettingsSelector>{menuItem}</SettingsSelector>
                    })}
                </Container>
                <Container className={classes.SettingsSidebar}>
                    {props.children}
                </Container>
            </Container>
            
        </Container>
    )
}

export default SettingsPage
