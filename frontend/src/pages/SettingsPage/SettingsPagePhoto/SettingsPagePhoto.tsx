import React from 'react'
import { Container } from 'react-bootstrap';
import classes from './SettingsPagePhoto.module.css'

import SettingsPage from '../SettingsPage'

const SettingsPagePhoto = () => {
    return (
        <SettingsPage settingItem={'Photo'}>
            <div>
                Photo
            </div>
        </SettingsPage>
    )
}

export default SettingsPagePhoto
