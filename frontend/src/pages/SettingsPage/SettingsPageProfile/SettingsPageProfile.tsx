import React from 'react'
import { Container } from 'react-bootstrap';
import classes from './SettingsPageProfile.module.css'

import SettingsPage from '../SettingsPage'

const SettingsPageProfile = () => {
    return (
        <SettingsPage settingItem={'Profile'}>
            <div>
                Profile
            </div>
        </SettingsPage>
    )
}

export default SettingsPageProfile
