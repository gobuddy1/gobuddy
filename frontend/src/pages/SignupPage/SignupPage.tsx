import React from 'react'
import { Container, Row } from 'react-bootstrap'

import SignupForm from '../../components/Auth/SignupForm/SignupForm'
import classes from './SignupPage.module.css';

const SignupPage = (props: any) => {
    const params = new URLSearchParams(props.location.search);
    const status = params.get('status');
    const exceptionMessage = params.get('errormessage')

    return (
        <Container fluid className={classes.SignupPage}>
            <Row className="justify-content-center">
                <SignupForm/>
            </Row>
        </Container>
    )
}

export default SignupPage
