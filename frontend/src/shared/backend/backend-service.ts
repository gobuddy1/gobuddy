// This file is shit.........

const MAIN_URL = process.env.REACT_APP_HOST

  export const postUser = (event: any, data: any) => {
    event.preventDefault();
    return fetch(MAIN_URL + 'auth/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            email: data.email,
            password: data.password
        })
    })
      .then(res => {
        if (res.status === 422) {
          throw new Error('Validation failed.');
        }
        if (res.status !== 200 && res.status !== 201) {
          console.log('Error!');
          throw new Error('Failed logging in. Make sure you are using correct email and password');
        }
        return res.json();
      })
  };

  const getMain = (url: string, token: string | null) => {
    return fetch(url, {
      headers: {
        Authorization: 'Bearer ' + token
      }
    })
    .then(res => {
      if (res.status === 422) {
        throw new Error('Validation failed.');
      }
      if (res.status !== 200 && res.status !== 201) {
        throw new Error(res.statusText);
      }
      return res.json();
    })
  }

  export const postMain = (url: string, body: string, token: string | null) => {
    return fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + token
        },
        body: body
    })
      .then(res => {
        if (res.status === 422) {
          throw new Error('Validation failed.');
        }
        if (res.status !== 200 && res.status !== 201) {
          console.log('Error!');
          throw new Error('Failed request.');
        }
        return res.json();
      })
  };

  export const deleteMain = (url: string, body: string, token: string | null) => {
    return fetch(url, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + token
        },
        body: body
    })
      .then(res => {
        if (res.status === 422) {
          throw new Error('Validation failed.');
        }
        if (res.status !== 200 && res.status !== 201) {
          console.log('Error!');
          throw new Error('Failed request.');
        }
        return res.json();
      })
  };

  export const fetchMainWithImage = (url: string, data: any, token: string | null, method: string, headers: Headers | string[][] | Record<string, string> | undefined) => {
    // const body = JSON.stringify({
    //   name: data.name,
    //   description: data.description,
    //   public: data.public,
    // })

    return fetch(url, {
        method: method,
        headers: headers,
        body: data
    })
      .then(res => {
        if (res.status === 422) {
          throw new Error('Validation failed.');
        }
        if (res.status !== 200 && res.status !== 201) {
          console.log('Error!');
          throw new Error('Failed request.');
        }
        return res.json();
      })
  };

  export const getWhoami = (token: string | null) => {
    return getMain(MAIN_URL + 'auth/whoami', token)
  }

  export const getUsername = (token: string | null, userId: string) => {
    return getMain(MAIN_URL + 'auth/username/' + userId, token)
  }

  export const getChallenges = (token: string | null) => {
    return getMain(MAIN_URL + 'challenge/get-challenges', token)
  }

  export const getPublicChallenges = (token: string | null) => {
    return getMain(MAIN_URL + 'challenge/get-public-challenges', token)
  }

  export const getLocations = (token: string | null, gameId: string) => {
    return getMain(MAIN_URL + 'challenge/get-locations/' + gameId, token)
  }

  export const getChallengeByCode = (token: string | null, codeId: string) => {
    return getMain(MAIN_URL + 'challenge/get-challenge/' + codeId, token)
  }

  export const getQuizByCode = (token: string | null, codeId: string, requestType: string) => {
    return getMain(MAIN_URL + `quiz/get-quiz/${requestType}/${codeId}`, token)
  }

  export const getQuizById= (token: string | null, quizId: string, requestType: string) => {
    return getMain(MAIN_URL + `quiz/get-quiz-byId/${requestType}/${quizId}`, token)
  }

  export const getQuizzes = (token: string | null) => {
    return getMain(MAIN_URL + 'quiz/get-quizzes', token)
  }

  export const getQuestion = (token: string | null, questionId: string) => {
    return getMain(MAIN_URL + 'quiz/get-question/' + questionId, token)
  }

  export const postLocation = (token: string | null, data: any) => {
      const body = JSON.stringify({
        name: data.name,
        description: data.description,
        longitude: data.longitude,
        latitude: data.latitude,
        range: data.range,
        challengeId: data.gameId
    })
    return postMain(MAIN_URL + 'challenge/create-location', body, token)
  }
  
  export const postChallenge = (token: string | null, data: any) => {
    const formData = new FormData();
    formData.append('name', data.name)
    formData.append('description', data.description)
    formData.append('public', data.public)
    formData.append('image', data.image)
    return fetchMainWithImage(MAIN_URL + 'challenge/create-challenge', formData, token, 'POST', {Authorization: 'Bearer ' + token})
  }

  export const putUser = (token: string | null, data: any) => {
    const formData = new FormData();
    formData.append('email', data.email.value)
    formData.append('lastname', data.lastname.value)
    formData.append('firstname', data.firstname.value)
    formData.append('password', data.password.value)
    formData.append('telnumber', data.telnumber.value)
    formData.append('image', data.image)

    return fetchMainWithImage(MAIN_URL + 'auth/signup', formData, token, 'PUT', undefined)
  }

  export const postQuiz = (token: string | null, data: any) => {
    const body = JSON.stringify({
        name: data.name,
        description: data.description
    })
    return postMain(MAIN_URL + 'quiz/create-quiz', body, token)
  }

  export const deleteQuiz = (token: string | null, data: any) => {
    const body = JSON.stringify({
        quizId: data.quizId
    })
    return deleteMain(MAIN_URL + 'quiz/delete-quiz', body, token)
  }
  


  export const postQuestion = (token: string | null, data: any) => {
    const newAnswers = [ ...data.answers ].map(answer => {
      return {
        correct: answer.correct,
        answer: answer.answer
      }
    })
    const formData = new FormData();
    formData.append('question', data.question)
    // Stringify data (is parsed in backend)
    formData.append('answers', JSON.stringify(newAnswers))
    formData.append('description', data.description)
    formData.append('category', data.category)
    formData.append('type', data.type)
    formData.append('image', data.image)
    formData.append('timelimit', data.timelimit)
    formData.append('pricecategory', data.pricecategory)
    formData.append('quizId', data.quizId)

    return fetchMainWithImage(MAIN_URL + 'quiz/create-question', formData, token, 'POST', {Authorization: 'Bearer ' + token})
  }

  export const putQuestion = (token: string | null, data: any) => {
    const newAnswers = [ ...data.answers ].map(answer => {
      return {
        correct: answer.correct,
        answer: answer.answer
      }
    })
    const formData = new FormData();
    formData.append('question', data.question)
    formData.append('answers', JSON.stringify(newAnswers))
    formData.append('description', data.description)
    formData.append('category', data.category)
    formData.append('type', data.type)
    formData.append('image', data.image)
    formData.append('timelimit', data.timelimit)
    formData.append('pricecategory', data.pricecategory)
    formData.append('questionId', data.questionId)

    return fetchMainWithImage(MAIN_URL + 'quiz/update-question', formData, token, 'PUT', {Authorization: 'Bearer ' + token})
  }