export const FAQContent: IFAQContent = {
    pages: {
        general: {
            name: 'General',
            faqs: [{
                question: 'Detta är en testfråga',
                answer: 'Detta är ett testsvar'
            },
            {
                question: 'Detta är en testfråga',
                answer: 'Detta är ett testsvar'
            }]
        },
        quiz: {
            name: 'Quiz',
            faqs: [{
                question: 'I\'m not able to create a question, what should I do?',
                answer: 'In order to create a question. Make sure that the following rules are applied'
            }]
        }
    }
}

export type IFAQContent = {
    pages: {
        [index: string]: {
            name: string,
            faqs: IFAQ[]
        }
    }
}

export type IFAQ = {
    question: string,
    answer: string
}

export type IFAQs = {
    faqs: IFAQ[]
}

// <ListGroup className={classes.ListGroup}>
// <ListGroup.Item>Question is between 1 and 60 characters long</ListGroup.Item>
// <ListGroup.Item>Total number of answers is exactly 4 (TBD)</ListGroup.Item>
// <ListGroup.Item>All answer are between 1 and 25 characters long</ListGroup.Item>
// <ListGroup.Item>One (and only one) answer has "true" marked to it</ListGroup.Item>
// </ListGroup>