export interface IUser{
    firstname: string,
    lastname: string,
    imageUrl?: string
}