import { ILocation } from './location.interface';

export interface IChallenge{
    name: string,
    description: string,
    locations: Array<ILocation>,
    createdby: string,
    _id: string,
    public: boolean,
    imageUrl?: string,
    mapChallenge?: boolean
}