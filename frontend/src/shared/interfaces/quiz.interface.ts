import { IQuestion } from './question.interface'

export interface IQuiz{
    name: string,
    description: string,
    questions: Array<IQuestion>,
    createdby: string,
    _id: string,
    imageUrl?: string,
    code: string
}