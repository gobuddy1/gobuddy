export interface IAnswer{
    _id?: string,
    answer: string,
    correct: boolean
}

export class Question{
    _id?: string
    question?: string
    answers?: Array<IAnswer>
    description?: string
    difficulty?: string
    category?: string
    type?: string
    image?: string
    timelimit?: number
    pricecategory?: string
}

export interface IQuestion extends Question{}