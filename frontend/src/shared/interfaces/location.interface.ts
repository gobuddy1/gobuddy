import { IQuestion } from './question.interface'

export interface ILocation{
    name: string,
    description: string,
    longitude: number,
    latitude: number,
    range: number,
    _id: string,
    questions: Array<IQuestion>
}