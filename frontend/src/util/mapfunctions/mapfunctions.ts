import {distance} from '@turf/turf'
import { ILocation } from '../../shared/interfaces/location.interface';


const MIN_DISTANCE: number = 15;

// Returns true if player is near game - NEED TO UPDATE LOGIC HERE
export const isNearGame = (fromCoords: ILocation | undefined, toCoords: ILocation | undefined) => {
    return (fromCoords && toCoords) ? distance([fromCoords.longitude, fromCoords.latitude], [toCoords.longitude, toCoords.latitude], {units: 'meters'}) < toCoords.range : false;
};

export const distToGame = (fromCoords: ILocation | undefined, toCoords: ILocation | undefined) => {
    return (fromCoords && toCoords) ? distance([fromCoords.longitude, fromCoords.latitude], [toCoords.longitude, toCoords.latitude], {units: 'meters'}) : null;
};