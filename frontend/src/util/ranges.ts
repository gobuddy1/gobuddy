type ranges = {
    name: string,
    range: number,
    title: string 
}

export const RANGE_25: ranges = {name: 'Range 25m', range: 25, title: '25m'};
export const RANGE_50: ranges = {name: 'Range 50m', range: 50, title: '50m'};
export const RANGE_100: ranges = {name: 'Range 100m', range: 100, title: '100m'};