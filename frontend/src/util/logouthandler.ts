import { GOBUDDY_LS_TOKEN, GOBUDDY_LS_UUID, GOBUDDY_LS_TOKEN_EXPIRY_DATE, GOBUDDY_LS_USER} from '../constants/localstoragenames'

export const logoutHandler = (history: any) => {
    localStorage.removeItem(GOBUDDY_LS_TOKEN);
    localStorage.removeItem(GOBUDDY_LS_TOKEN_EXPIRY_DATE);
    localStorage.removeItem(GOBUDDY_LS_UUID);
    localStorage.removeItem(GOBUDDY_LS_USER);

    history.push('/')

    console.log('Logged out!')
};

export const isAuthenticated = () => {
    let token = localStorage.getItem(GOBUDDY_LS_TOKEN);
    const date = localStorage.getItem(GOBUDDY_LS_TOKEN_EXPIRY_DATE);
    if(date){
        const tokenExpiration = new Date(date);
        if(token){
            const dateNow = new Date();
            if(tokenExpiration.getTime() < dateNow.getTime()){
                return false;
            }
        }
    }
    return true;
}