import { useEffect, useRef, useState } from "react";
import socketIOClient from "socket.io-client";
import { Socket } from "socket.io-client";

const NEW_CHAT_MESSAGE_EVENT = "newChatMessage"; // Name of the event
const COUNT_USER_EVENT = "countUser";
const SOCKET_SERVER_URL = "http://localhost:8080";

const useChat = (quizId: string) => {
    const [messages, setMessages] = useState<any[]>([]);
    const [users, setUsers] = useState<Object>({});
    const socketRef = useRef<typeof Socket>();

    useEffect(() => {
        
        // Create websocket connection
        socketRef.current = socketIOClient(SOCKET_SERVER_URL, {
            query: { quizId }
        })
        console.log('socketRef', socketRef)
        if(socketRef){
            // Listen for incoming messages
            socketRef.current.on(NEW_CHAT_MESSAGE_EVENT, (message: any) => {
                const incomingMessage = {
                    ...message,
                    ownedByCurrentUser: message.senderId === socketRef.current?.id,
                };
                setMessages((messages) => [...messages, incomingMessage])
            })

            // Listen for new users
            socketRef.current.on(COUNT_USER_EVENT, (users: any) => {
                console.log('useChat', users)
                setUsers(users)
            })

            // Destroy socket reference when connection is closed
            return () => {
                socketRef.current?.disconnect();
            }

        }
    }, [quizId]);

    // Send message to server that forwards to all user in same room
    const sendMessage = (messageBody: any) => {
        socketRef.current?.emit(NEW_CHAT_MESSAGE_EVENT, {
            body: messageBody,
            senderId: socketRef.current.id,
        });
    }

    //Count users. Create object if to be used later with other attributes
    const countUser = (userId: any) => {
        socketRef.current?.emit(COUNT_USER_EVENT, {
            userId: userId
        });
    }

    return { messages, sendMessage, users, countUser }
}

export default useChat;