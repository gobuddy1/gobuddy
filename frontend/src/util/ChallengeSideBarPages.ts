export const ADD_CHALLENGE = 'Add Challenge';
export const ADD_LOCATION = 'Add Location';
export const ADD_QUESTION = 'Add Question';
export const VIEW_QUESTIONS = 'View Questions';
export const TOGGLE_MAP_STYLE = 'toggle-map-style';
export const MAIN_VIEW = 'Challenge';
