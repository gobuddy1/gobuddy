import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import { faChartPie, faSlidersH, faHandPointer, faListOl, faHammer } from '@fortawesome/free-solid-svg-icons';

type questionType = {
    name: string,
    icon: IconDefinition
}

export const OPTION: questionType = {name: 'The Option', icon: faHandPointer};
export const GUESSTIMATE: questionType = {name: 'The Guesstimate', icon: faChartPie};
export const LIST: questionType = {name: 'The List', icon: faListOl};
export const MATCH: questionType = {name: 'The Match', icon: faHammer};
export const PARAMETERS: questionType = {name: 'The Parameters', icon: faSlidersH};