import React from 'react'
import { Container } from 'react-bootstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import classes from './DigitalGobuddyButton.module.css'
import { IconDefinition } from '@fortawesome/fontawesome-common-types';

type DigitalGobuddyButtonProps = {
    children?: string,
    icon: IconDefinition,
    onClick?: ((event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void) | undefined
}

const DigitalGobuddyButton = (props: DigitalGobuddyButtonProps) => {
    return (
        <Container fluid className={classes.DigitalGobuddyButton} onClick={props.onClick}>
            <Container fluid className={classes.Text}>
                {props.children}
            </Container>
            <Container className={classes.IconContainer}>
                <FontAwesomeIcon icon={props.icon} className={classes.Icon} />
            </Container>
        </Container>
    )
}

export default DigitalGobuddyButton
