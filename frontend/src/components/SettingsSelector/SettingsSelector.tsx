import React, { Children } from 'react'
import { Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import classes from './SettingsSelector.module.css'

type SettingsSelectorProps = {
    children: string
}

const SettingsSelector = (props: SettingsSelectorProps) => {
    return (
        <Link className={classes.Link} to={'/user-settings/' + props.children.toLowerCase()}>
            <Container className={classes.SettingsSelectorContainer}>
                    {props.children}
            </Container>
        </Link>
    )
}

export default SettingsSelector
