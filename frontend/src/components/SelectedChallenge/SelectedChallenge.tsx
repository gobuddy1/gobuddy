import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom';
import { Container, Image, Row, Col } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMapMarkerAlt, faQuestion, faEdit } from '@fortawesome/free-solid-svg-icons';


import classes from './SelectedChallenge.module.css'
import { IChallenge } from '../../shared/interfaces/challenge.interface';
import defaultImage from '../../shared/stockholm.jpg'

type SelectedChallengeProps = {
    challenge: IChallenge,
}

const SelectedChallenge = (props: SelectedChallengeProps) => {
    const history = useHistory();

    const [noQuestions, setNoQuestions] = useState<number>(0);

    //Count number of questions
    useEffect(() => {
        setNoQuestions(props.challenge.locations.map(location => location.questions.length).reduce((a, b) => a + b, 0))
    }, [props.challenge.locations])

    const onEditChallenge = () => {
        history.push('/challenges/' + props.challenge._id)
    }

    return (
        <Container fluid className={classes.SelectedChallenge}>
            <Image src={props.challenge.imageUrl ? process.env.REACT_APP_HOST + props.challenge.imageUrl : defaultImage} className={classes.Image}/>
            <Container className={classes.TextContainer}>
                <h1 className={classes.Title}>{props.challenge.name}</h1>
                <p className={classes.Description}>{props.challenge.description}</p>
                <Container className={classes.SummaryContainer}>
                    <Row>
                        <Col>
                            <FontAwesomeIcon icon={faMapMarkerAlt} />
                            <p>{props.challenge.locations.length} locations</p>
                        </Col>
                        <Col>
                            <FontAwesomeIcon icon={faQuestion} />
                            <p>{noQuestions} questions</p>
                        </Col>
                        <Col onClick={onEditChallenge} className={classes.Edit}>
                            <FontAwesomeIcon icon={faEdit} />
                            <p>Edit</p>
                        </Col>
                    </Row>
                </Container>
            </Container>
        </Container>
    )
}

export default SelectedChallenge
