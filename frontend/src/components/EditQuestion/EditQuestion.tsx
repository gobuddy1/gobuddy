import React, { useEffect, useState } from 'react'
import { useParams, useHistory } from 'react-router-dom';
import { Container, Form } from 'react-bootstrap'

import classes from './EditQuestion.module.css'
import { IQuestion, Question, IAnswer } from '../../shared/interfaces/question.interface';
import ButtonUI from '../UI/ButtonUI/ButtonUI';
import { GOBUDDY_LS_TOKEN } from '../../constants/localstoragenames';
import { getQuestion, getQuizById, postQuestion, putQuestion } from '../../shared/backend/backend-service';
import NavigationBar from '../../parts/NavigationBar/NavigationBar';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash, faCheck, faTimes, faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { generateBase64FromImage } from '../../util/image';
import ImageSelector from '../ImageSelector/ImageSelector';
import Modal from '../UI/Modal/Modal';
import FAQPage from '../../pages/FAQPage/FAQPage';
import { IQuiz } from '../../shared/interfaces/quiz.interface';

const TEXTAREA_TYPE = 'textarea';
const TEXT_TYPE = 'text';
const DESCRIPTION_ID = 'description';
const QUESTION_ID = 'question';

interface IParams{
    quizId: string,
    questionId: string
}

const EditQuestion = () => {
    const params = useParams<IParams>();
    const history = useHistory();

    const [question, setQuestion] = useState<IQuestion>(new Question());
    const [showAddAnswerButton, setShowAddAnswerButton] = useState<boolean>(true);
    const [imagePreview, setImagePreview] = useState<any>(null);
    const [image, setImage] = useState<any>(null);
    const [questionValid, setQuestionValid] = useState<boolean>(false);
    const [showHelp, setShowHelp] = useState<boolean>(false);
    const [quiz, setQuiz] = useState<IQuiz>();
    
    useEffect(() => {
        if(params.questionId !== 'undefined'){
            const token = localStorage.getItem(GOBUDDY_LS_TOKEN);
            getQuestion(token, params.questionId)
            .then(response => {
                setQuestion(response);
                setImagePreview(process.env.REACT_APP_HOST + response.image);
            })
            .catch(error => {
                // setErrorMessage(error.message);
                console.log(error.message);
            })
        }else{
            //Init question
            const answerPlaceholder: IAnswer[] = [];
            const questionPlaceholder: string = '';
            const imagePlaceholder: string = '';
            const descriptionPlaceholder: string = '';
            
            const categoryPlaceholder: string = 'placeholder-category'; 
            const difficultyPlaceholder: string = 'placeholder-difficulty'; 
            const typePlaceholder: string = 'placeholder-type';
            const timelimitPlaceholder: number = 10000;
            const pricecategoryPlaceholder: string = 'placeholder-pricecategory';
            let newQuestion = { ...question };
            newQuestion.answers = answerPlaceholder;
            newQuestion.question = questionPlaceholder;
            newQuestion.image = imagePlaceholder;
            newQuestion.description = descriptionPlaceholder;
            newQuestion.category = categoryPlaceholder;
            newQuestion.difficulty = difficultyPlaceholder;
            newQuestion.type = typePlaceholder;
            newQuestion.timelimit = timelimitPlaceholder;
            newQuestion.pricecategory = pricecategoryPlaceholder;
            setQuestion(newQuestion)
        }
    }, [])

    // Get quiz
    useEffect(() => {
        const token = localStorage.getItem(GOBUDDY_LS_TOKEN);
        getQuizById(token, params.quizId, 'edit-quiz')
        .then(quiz => {
            if(quiz._id){
                setQuiz(quiz);
            }else{
                throw new Error('Could not find challenge with given id');
            }
        })
        .catch(error => {
            console.log(error)
        })
    }, [])

    useEffect(() => {
        if(question.answers){
            setShowAddAnswerButton(question.answers.length < 4)
        }
        //Validation logic for question
        const questionValidated = validateQuestion();
        setQuestionValid(questionValidated)
    }, [question])

    //Validate question before POST
    const validateQuestion = () => {
        let validated = true;
        // Answers must exist and be of length 4
        validated = validated && (question.answers !== undefined) && (question?.answers?.length === 4);

        //Each answer must be between 1 and 25 characters long and one answer (and only one) needs to be correct
        if(question.answers){ 
            validated = validated && question.answers.map(answer => {
                return answer.answer.length >= 1 && answer.answer.length <= 25
            }).reduce((acc, current) => acc && current);
            validated = validated && (question.answers.filter(answer => answer.correct).length === 1);
        }

        //Question must exist and must be between 1 and 60 characters long
        validated = validated && (question.question !== undefined) && 
                    ((question?.question?.length >= 1) && (question?.question?.length <= 60))

        //Description must exist and must be between 1 and 60 characters long
        validated = validated && (question.description !== undefined) && 
                    ((question?.description?.length >= 1) && (question?.description?.length <= 60))

        // Image must exist - update
        // validated = validated && (question.image !== '' || image !== null)

        // Difficulty
        // timelimit
        // type
        // price category
        return validated;
    }

    const onCreateQuestion = (e: any) => {
        e.preventDefault();
        if(questionValid){
            const token = localStorage.getItem(GOBUDDY_LS_TOKEN);
            const data = {
                question: question.question, 
                answers: question.answers, 
                description: question.description, 
                category: question.category, 
                type: question.type,
                image: '',
                difficulty: question.difficulty,
                timelimit: question.timelimit,
                pricecategory: question.pricecategory,
                quizId: '',
                questionId: ''
            };
            let updateQuestion;
            if(params.questionId === 'undefined'){
                data.quizId = params.quizId;
                updateQuestion = postQuestion;
                data.image = image;
            }else{
                data.questionId = params.questionId;
                updateQuestion = putQuestion;
                // If new image is uploaded then update data.image - otherwise data.image will be an empty string.
                if(image){
                    data.image = image;
                }
            }
            updateQuestion(token, data)  
            .then(resData => {
                history.push('/quiz/add-questions/' + quiz?.code)
            })
            .catch(error => {
                console.log(error);
            });
        }
            
    }


    const onChangeInput = (e: any) => {
        const value = e.target.value;
        const type = e.target.type;
        const id = e.target.id;

        // Create copy of question
        let newQuestion = { ...question };

        switch(type){
            case TEXTAREA_TYPE:
                // Updating question
                if(id === QUESTION_ID){
                    newQuestion.question = value;
                    setQuestion(newQuestion);
                }else{
                    newQuestion.description = value;
                    setQuestion(newQuestion);
                }
                break;
            case TEXT_TYPE:
                // Updating question
                if(question.answers){
                    const answerIndex = question.answers.map(answer => answer._id).indexOf(e.target.id);
                    let newAnswers: IAnswer[] = [...question.answers];
                    newAnswers[answerIndex] = {...newAnswers[answerIndex], answer: value};
                    newQuestion.answers = newAnswers;
                    setQuestion(newQuestion);
                }
                break;
        }
    }

    const onToggleAnswerStatus = (e: any, id: string | undefined) => {
        if(question.answers && id){
            const answerIndex = question.answers.map(answer => answer._id).indexOf(id);
            const otherIndices = question.answers.reduce((a: number[], answer, i) => {
                if (answer._id !== id){
                    a.push(i);
                }
                return a;
            }, []);

            let newAnswers: IAnswer[] = [...question.answers];
            // Logic to toggle other indices (make sure only one answer is correct)
            if(!newAnswers[answerIndex].correct){
                otherIndices.map(index => {
                    newAnswers[index] = {...newAnswers[index], correct: false};
                })
            }
            newAnswers[answerIndex] = {...newAnswers[answerIndex], correct: !newAnswers[answerIndex].correct};
            let newQuestion = { ...question, answers: newAnswers }
            setQuestion(newQuestion);
        }
    }

    const onAddAnswer = () => {
        let newQuestion = { ...question };
        let newAnswers: IAnswer[] = [];

        if(question.answers){
            newAnswers = [...question.answers];
        }
        newAnswers.push({_id: (Math.random()*10000).toString(), correct: false, answer: ''})
        newQuestion.answers = newAnswers;
        setQuestion(newQuestion);
    }

    const onDeleteAnswer = (e: any, id: string | undefined) => {
        let newQuestion = { ...question };        
        if(question.answers){
            let newAnswers: IAnswer[] = [...question.answers].filter(answer => answer._id !== id)
            newQuestion.answers = newAnswers;
            setQuestion(newQuestion);
        }
    }

    const onImageUpload = (event: any) => {
        const files = event.target.files;
        setImage(files[0])
        if(files[0]) {
            generateBase64FromImage(files[0])
              .then(b64 => {
                setImagePreview(b64);
              })
              .catch(e => {
                setImagePreview('');
              });
          }
    }

    const toggleShowHelp = () => {
        const currentShowHelp = showHelp;
        setShowHelp(!currentShowHelp)
    }

    return (
        <Container fluid className={classes.AddQuizQuestionPage}>
        <NavigationBar />
        <Modal onClose={toggleShowHelp} show={showHelp}>
            <FAQPage></FAQPage>
        </Modal>
        <Container fluid className={classes.FormContainer}>
            <Container fluid className={classes.EditQuestionContainer}>
                <Form className={classes.Form} onSubmit={onCreateQuestion}>
                    <ImageSelector imagePreview={imagePreview} onChange={onImageUpload}/>
                    <Form.Group className={classes.Description}>
                        <Form.Label className={classes.InputLabel}>Description</Form.Label>
                        <Form.Control
                            as="textarea"
                            className={classes.Input}
                            onChange={onChangeInput}
                            autoComplete="off"
                            defaultValue={question.description}
                            id={DESCRIPTION_ID}
                        />
                    </Form.Group>
                    <Form.Group className={classes.Question}>
                        <Form.Label className={classes.InputLabel}>Question</Form.Label>
                        <Form.Control
                            as="textarea"
                            className={classes.Input}
                            onChange={onChangeInput}
                            autoComplete="off"
                            defaultValue={question.question}
                            id={QUESTION_ID}
                        />
                    </Form.Group>
                    
                    <Form.Group className={classes.Options}>
                        <Form.Label className={classes.InputLabel}>Answers</Form.Label>
                        {question.answers?.map(answer => {
                            return <Container key={answer._id} fluid className={classes.FormControlContainer}>

                                <Container id={answer._id} fluid onClick={(e: any) => onToggleAnswerStatus(e, answer._id)}
                                    className={[classes.TrueFalseButton, answer.correct ? classes.True : classes.False].join(' ')}>
                                        <FontAwesomeIcon onClick={(e: any) => onToggleAnswerStatus(e, answer._id)} icon={answer.correct ? faCheck : faTimes}></FontAwesomeIcon>
                                </Container>

                                <Form.Control
                                    key={answer._id}
                                    id={answer._id}
                                    type="text"
                                    className={classes.Input}
                                    onChange={onChangeInput}
                                    autoComplete="off"
                                    defaultValue={answer.answer}
                                />
                                    <FontAwesomeIcon 
                                        className={classes.Icon} 
                                        icon={faTrash} 
                                        onClick={(e: any) => onDeleteAnswer(e, answer._id)}></FontAwesomeIcon>
                            </Container>
                        })}
                        {showAddAnswerButton ? <Container fluid
                            className={classes.AddAnswerButton} 
                            onClick={onAddAnswer}>
                            <p>Add Answer</p>
                        </Container> : null}
                    </Form.Group>
                    <ButtonUI disabled={!questionValid}>Create</ButtonUI>
                </Form>
            </Container>
            <Container fluid className={classes.HelpContainer} onClick={toggleShowHelp}>
                <FontAwesomeIcon icon={faInfoCircle}></FontAwesomeIcon>
            </Container>
        </Container>
        </Container>
    )
}

export default EditQuestion
