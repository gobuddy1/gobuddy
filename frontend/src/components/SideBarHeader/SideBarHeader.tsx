import React from 'react'
import { Container } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes, faArrowLeft } from '@fortawesome/free-solid-svg-icons';

import classes from './SideBarHeader.module.css'

interface SidebarHeaderProps{
    returnHandler: (event: any, view: string) => void,
    isInMain: boolean,
    menuOpen: boolean,
    title: string,
    previousView: string
}

const SideBarHeader = (props: SidebarHeaderProps) => {

    return (
        <Container fluid className={classes.SideBarHeader}>
            {props.menuOpen ? <div className={classes.Title}>{props.title}</div> : null}
            {props.isInMain ? 
                <FontAwesomeIcon 
                    className={classes.ReturnButton} 
                    icon={faArrowLeft} 
                    onClick={e => props.returnHandler(e, props.previousView)}
                /> : <div className={classes.Placeholder}></div>}
        </Container>
    )
}

export default SideBarHeader
