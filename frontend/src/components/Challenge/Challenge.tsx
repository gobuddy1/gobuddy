import React, { useEffect, useState } from 'react'

import { Container, Image } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faShareAlt, faTrash, faThumbtack, faUser } from '@fortawesome/free-solid-svg-icons';

import { IChallenge } from '../../shared/interfaces/challenge.interface'
import { IUser } from '../../shared/interfaces/user.interface'
import classes from './Challenge.module.css';
import { GOBUDDY_LS_TOKEN } from '../../constants/localstoragenames';
import { getUsername } from '../../shared/backend/backend-service';

import defaultImage from '../../shared/stockholm.jpg'

type ChallengeProps = {
    challenge: IChallenge,
    onClick?: ((event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void) | undefined
}

const Challenge = (props: ChallengeProps) => {
    const history = useHistory();
    const [createdByUser, setCreatedByUser] = useState<IUser>({firstname: '', lastname: ''})

    useEffect(() => {
        const token = localStorage.getItem(GOBUDDY_LS_TOKEN);
        getUsername(token, props.challenge.createdby)
            .then(user => {
                setCreatedByUser({firstname: user.firstname, lastname: user.lastname});
            })
            .catch(error => {
                console.log(error);
            })

    }, [])

    const onChallengeEdit = () => {
        history.push('/challenges/' + props.challenge._id)
    }
    const onShareChallenge = () => {
        console.log('Sharing challenge...')
    }
    const onDeleteChallenge = () => {
        console.log('Deleting challenge...')
    }

    return (
        <Container className={classes.Challenge} onClick={props.onClick}>
            {/* <div className={classes.Title}>
                {props.challenge.name}
            </div>
            <div className={classes.Description}>
                {props.challenge.description}
            </div>
            <Container className={classes.ChallengeFooter}>
                <Container fluid className={classes.Summary}>
                    <div className={classes.SummaryItem}>
                        <FontAwesomeIcon className={classes.SummaryIcon} icon={faThumbtack}/> {props.challenge.locations.length}
                    </div>
                    <div className={classes.SummaryItem}>
                        <FontAwesomeIcon className={classes.SummaryIcon} icon={faUser}/> {createdByUser.firstname} {createdByUser.lastname}
                    </div>
                </Container>
                <Container fluid className={classes.ChallengeActions}>
                    <FontAwesomeIcon className={classes.Icon} icon={faEdit} onClick={onChallengeEdit}/>
                    <FontAwesomeIcon className={classes.Icon} icon={faShareAlt} onClick={onShareChallenge}/>
                    <FontAwesomeIcon className={classes.Icon} icon={faTrash} onClick={onDeleteChallenge}/>
                </Container>
            </Container> */}
            <Image  src={props.challenge.imageUrl ? process.env.REACT_APP_HOST + props.challenge.imageUrl : defaultImage} 
                    className={classes.Image}/>
            <p className={classes.Title}>{props.challenge.name}</p>
        </Container>
    )
}

export default Challenge
