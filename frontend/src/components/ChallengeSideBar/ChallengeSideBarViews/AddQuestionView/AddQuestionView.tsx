import React, {useState, useEffect} from 'react'
import { Container, Button, Form } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { v4 as uuidv4 } from 'uuid';

import { ILocation } from '../../../../shared/interfaces/location.interface';
import { IQuestion, Question, IAnswer } from '../../../../shared/interfaces/question.interface';

import classes from './AddQuestionView.module.css'
import QuestionTypeButton from '../../../UI/QuestionTypeButton/QuestionTypeButton';
import { OPTION, GUESSTIMATE, LIST, MATCH, PARAMETERS } from '../../../../util/question-types';
import { MAIN_VIEW } from '../../../../util/ChallengeSideBarPages';
import ButtonUI from '../../../UI/ButtonUI/ButtonUI';
import { QUESTION_DESCRIPTION, QUESTION_FORM, QUESTION_ANSWER } from '../../../../shared/formid/add-question-form';

const QUESTION_TYPES = [OPTION, GUESSTIMATE, LIST, MATCH, PARAMETERS];

type AddQuestionViewProps = {
    selectedLocation: ILocation | undefined,
    setView: (event: any, view: string) => void
}

const AddQuestionView = (props: AddQuestionViewProps) => {
    const [selectedType, setSelectedType] = useState<string>('');
    const [typeLocked, setTypeLocked] = useState<boolean>(false);
    const [headerText, setHeaderText] = useState<string>('');
    const [question, setQuestion] = useState<IQuestion>(new Question());
    const [answers, setAnswers] = useState<IAnswer[]>([{answer: '', correct: false, _id: uuidv4()}, {answer: '', correct: false, _id: uuidv4()}]);

    useEffect(() => {
        // Reset view
        if(props.selectedLocation === undefined){
            props.setView(null, MAIN_VIEW);
        }
    }, [props.selectedLocation])

    useEffect(() => {
        if(selectedType){
            setHeaderText(selectedType);
        }else{
            setHeaderText('Select Question Type')
        }
    }, [selectedType])

    const selectTypeHandler = (event: any) => {
        setSelectedType(event.target.id);
    }

    const lockType = () => {
        setTypeLocked(true);
    }

    //calls every time input changes
    const addQuestionInputHandler = (event: any) => {
        const value = event.target.value;
        let id = event.target.id;
        let index = -1;
        if(id.includes(QUESTION_ANSWER)){
            index = id.split('_')[1];
            id = id.split('_')[0];
        }
        switch(id){
            case QUESTION_FORM:
                setQuestion({question: value});
                break;
            case QUESTION_ANSWER:
                const newAnswers = answers.slice();
                newAnswers[index] = value;
                setAnswers(newAnswers);
        }
    }

    const addAnswerHandler = () => {
        console.log(answers)
        const newAnswers = answers.slice();
        const defaultAnswer = {answer: '', correct: false, answerId: uuidv4()}
        newAnswers.push(defaultAnswer); //pushes empty answer to array of answer
        setAnswers(newAnswers)
    }

    //Fix this
    const removeAnswerHandler = (event: any, answerId: string | undefined) => {
        console.log('Removing item with id: ', answerId)
        const newAnswers = answers.filter(answer => answer._id !== answerId);
        setAnswers(newAnswers);
    }

    const onSelectingCorrectAnswer = (event: any) => {
        console.log(event.target.id)
    }


    //Content selector
    let content = <Container></Container>

    if (selectedType === OPTION.name && typeLocked){
        content = <Container fluid className={classes.BodyContainer}>
            <Form>
                <Form.Group controlId={QUESTION_FORM}>
                    <Form.Control className={classes.FormControl} as="textarea" rows={3} placeholder="Question" onChange={addQuestionInputHandler}/>
                </Form.Group>
                {answers.map((answer, index) => {
                    return  <Form.Group className={classes.FormGroup} key={index} controlId={QUESTION_ANSWER + '_' + index}>
                                <Form.Check className={classes.Radio} name="anynum" type="radio" onClick={onSelectingCorrectAnswer}></Form.Check>
                                <Form.Control className={classes.FormControl} type="text" placeholder={"Answer " + (index + 1)} onChange={addQuestionInputHandler}/>
                                <FontAwesomeIcon className={classes.FormIcon} icon={faTimes} onClick={e => removeAnswerHandler(e, answer._id)}/>
                            </Form.Group>
                })}
                <Button onClick={addAnswerHandler}>Add Answer</Button>
            </Form>
        </Container>
    }
    else if (selectedType === GUESSTIMATE.name && typeLocked){
        content = <h1 className={classes.Header}>Content not available yet!</h1>
    }
    else if (selectedType === LIST.name && typeLocked){
        content = <h1 className={classes.Header}>Content not available yet!</h1>
    }
    else if (selectedType === MATCH.name && typeLocked){
        content = <h1 className={classes.Header}>Content not available yet!</h1>
    }
    else if (selectedType === PARAMETERS.name && typeLocked){
        content = <h1 className={classes.Header}>Content not available yet!</h1>
    }else{
        content = <Container fluid className={classes.QuestionTypeContainer}>
                    {QUESTION_TYPES.map(type => {
                        return <QuestionTypeButton key={type.name} active={selectedType===type.name} id={type.name} icon={type.icon} onClick={(e) => selectTypeHandler(e)}>{type.name}</QuestionTypeButton>
                    })}
                    <ButtonUI clicked={lockType}>Select Type</ButtonUI>
                </Container>
    }

    return (
        <Container fluid className={classes.Container}>
            <h1 className={classes.Header}>{headerText}</h1>
            <Container fluid className={classes.BodyContainer}>
                {content}
            </Container>
        </Container>
    )
}

export default AddQuestionView
