import React, { useEffect } from 'react'

import { Container, Form } from 'react-bootstrap';

import { RANGE_25, RANGE_50, RANGE_100 } from '../../../../util/ranges';
import classes from './AddLocationView.module.css'
import AddLocationButton from '../../../AddLocationButton/AddLocationButton';
import { LOCATION_DESCRIPTION, LOCATION_NAME } from '../../../../shared/formid/add-location-form';

const RANGES = [RANGE_25, RANGE_50, RANGE_100]

type AddLocationProps = {
    range: number,
    selectLocationRange: (event: any, range: number) => void,
    addLocationInputHandler: (event: any) => void
}

const AddLocation = (props: AddLocationProps) => {


    return (
        <Container fluid>
            <Form>
                <Form.Group controlId={LOCATION_NAME}>
                    <Form.Control className={classes.FormControl} type="text" placeholder="Name" onChange={props.addLocationInputHandler}/>
                </Form.Group>
                <Form.Group controlId={LOCATION_DESCRIPTION}>
                    <Form.Control className={classes.FormControl} as="textarea" rows={3} placeholder="Description" onChange={props.addLocationInputHandler}/>
                </Form.Group>
            
                <Container className={[classes.SideBarContainer, classes.LocationRangeSelector].join(' ')}>
                    {RANGES.map(range => {
                        return <AddLocationButton
                        key={range.title}
                        isSelected={props.range===range.range} 
                        selectLocationRange={props.selectLocationRange}
                        range={range.range}
                        title={range.title}/>
                    })}

                </Container>
            </Form>
        </Container>
    )
}

export default AddLocation
