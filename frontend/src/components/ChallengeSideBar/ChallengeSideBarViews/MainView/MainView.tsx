import React from 'react'
import { Container } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusSquare, faEye, faChevronLeft, faChevronRight, faThumbtack, faToggleOn, faToggleOff } from '@fortawesome/free-solid-svg-icons';

import { ILocation } from '../../../../shared/interfaces/location.interface';

import classes from './MainView.module.css'
import ChallengeSidebarButton from '../../../UI/ChallengeSidebarButton/ChallengeSidebarButton';
import MarkerSummary from '../../../MarkerSummary/MarkerSummary';
import { ADD_LOCATION, ADD_QUESTION, VIEW_QUESTIONS } from '../../../../util/ChallengeSideBarPages';
import { MAP_STYLE_DARK, MAP_STYLE_LIGHT } from '../../../../shared/styles/map-style'

type MainViewProps = {
    location: ILocation | undefined,
    menuOpen: boolean,
    setView: (event: any, view: string) => void,
    unSelectLocation: () => void,
    toggleMapStyle: () => void,
    mapStyle: string | undefined,
    deleteLocation: () => void,
    editLocation: () => void
}

const MainView = (props: MainViewProps) => {
    return (
        <Container className={classes.SideBarContainer}>
                {props.location ? <MarkerSummary editLocation={props.editLocation} deleteLocation={props.deleteLocation} location={props.location} menuOpen={props.menuOpen} unSelectMarkerSummary={props.unSelectLocation} /> : null}
                <ChallengeSidebarButton icon={faThumbtack} onClick={e => props.setView(e, ADD_LOCATION)} isAddLocation>Add Location</ChallengeSidebarButton>
                {/* <ChallengeSidebarButton icon={props.mapStyle === MAP_STYLE_DARK ? faToggleOff : faToggleOn} onClick={e => props.toggleMapStyle()}>Map Style</ChallengeSidebarButton> */}
                {props.location ? <ChallengeSidebarButton icon={faPlusSquare} onClick={e => props.setView(e, ADD_QUESTION)}>Add Question</ChallengeSidebarButton> : null}
                {props.location ? <ChallengeSidebarButton icon={faEye} onClick={e => props.setView(e, VIEW_QUESTIONS)}>View Questions</ChallengeSidebarButton> : null}
        </Container>
    )
}

export default MainView
