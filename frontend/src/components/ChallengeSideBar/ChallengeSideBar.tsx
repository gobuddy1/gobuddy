import React, { useState } from 'react'
import { Container } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons';

import { IQuestion } from '../../shared/interfaces/question.interface';
import { ILocation } from '../../shared/interfaces/location.interface';
import { MAIN_VIEW, ADD_LOCATION, ADD_QUESTION, VIEW_QUESTIONS } from '../../util/ChallengeSideBarPages';

import classes from './ChallengeSideBar.module.css'
import ChallengeSidebarButton from '../UI/ChallengeSidebarButton/ChallengeSidebarButton'
import SideBarHeader from '../SideBarHeader/SideBarHeader';
import MarkerSummary from '../MarkerSummary/MarkerSummary';
import MainView from './ChallengeSideBarViews/MainView/MainView';
import AddLocationView from './ChallengeSideBarViews/AddLocationView/AddLocationView'
import AddQuestionView from './ChallengeSideBarViews/AddQuestionView/AddQuestionView';


type ChallengeSideBarProps = {
    location: ILocation | undefined,
    unSelectLocation: () => void,
    selectLocationRange: (event: any, range: number) => void,
    range: number,
    toggleMapStyle: () => void,
    mapStyle: string | undefined,
    addLocationInputHandler: (event: any,) => void,
    deleteLocation: () => void,
    editLocation: () => void
}

const ChallengeSideBar = (props: ChallengeSideBarProps) => {
    const locationId = props.location ? props.location._id : ''
    const questions: IQuestion[] = props.location ? props.location.questions : []

    const [currentView, setCurrentView] = useState<string>(MAIN_VIEW);
    const [previousView, setPreviousView] = useState<string>('');
    const [menuOpen, setMenuOpen] = useState<boolean>(false);

    // Open or close menu
    const toggleSideBar = () => {
        setMenuOpen(!menuOpen);
    }

    // Set current and previous view
    const setView = (event: any, view: string) => {
        setPreviousView(currentView);
        setCurrentView(view);
        if(view === MAIN_VIEW){
            props.selectLocationRange(event, 0)
        }
    }

    //Content selector
    let  sideMenuContent = null;
    switch(currentView){
        case MAIN_VIEW:
            sideMenuContent = <MainView deleteLocation={props.deleteLocation} editLocation={props.editLocation} mapStyle={props.mapStyle} toggleMapStyle={props.toggleMapStyle} setView={setView} location={props.location} unSelectLocation={props.unSelectLocation} menuOpen={menuOpen} />
            break;
        case ADD_LOCATION:
            sideMenuContent = <AddLocationView range={props.range} selectLocationRange={props.selectLocationRange} addLocationInputHandler={props.addLocationInputHandler}/>
            break;
        case ADD_QUESTION:
            sideMenuContent = <AddQuestionView selectedLocation={props.location} setView={setView} />
            break;
        case VIEW_QUESTIONS:
            sideMenuContent = <div onClick={e => setView(e, MAIN_VIEW)}>View Questions</div>
            break;
    }

    const menuStateStyle = menuOpen ? classes.MenuOpen : classes.MenuClose;
    const sideBarButton = menuOpen ? classes.SideBarButtonOpen : classes.SideBarButtonClose;
    
    return (
        <Container fluid className={classes.SideBarContainer}>
            <Container fluid className={[classes.ChallengeSideBar, menuStateStyle].join(' ')}>
                <SideBarHeader
                    returnHandler={setView}
                    title={currentView}
                    isInMain={currentView!==MAIN_VIEW}
                    previousView={previousView}
                    menuOpen={menuOpen}
                />
                {menuOpen ? sideMenuContent : null}
            </Container>
            <Container className={[classes.SideBarButton, sideBarButton].join(' ')} onClick={toggleSideBar}><FontAwesomeIcon icon={menuOpen ? faChevronLeft : faChevronRight} /></Container>
            {(props.location && !menuOpen) ? <MarkerSummary deleteLocation={props.deleteLocation} editLocation={props.deleteLocation} location={props.location} menuOpen={menuOpen} unSelectMarkerSummary={props.unSelectLocation}/> : null}
        </Container>
    )
}

export default ChallengeSideBar