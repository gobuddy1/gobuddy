import React from 'react'
import { Container } from 'react-bootstrap';

import { IQuestion } from '../../shared/interfaces/question.interface'
import classes from './ChallengeMenuQuestion.module.css'

interface ChallengeMenuQuestionProps {
    question: IQuestion,
}

const ChallengeMenuQuestion = (props: ChallengeMenuQuestionProps) => {
    return (
        <Container className={classes.ChallengeMenuQuestion}>
            {props.question ? props.question.question : ''}
        </Container>
    )
}

export default ChallengeMenuQuestion
