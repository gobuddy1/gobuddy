import React from 'react'
import { Container } from 'react-bootstrap';


import classes from './AddLocationButton.module.css'

type AddLocationButtonProps = {
    title: string,
    range: number,
    selectLocationRange: (event: any, range: number) => void,
    isSelected: boolean
}

const AddLocationButton = (props: AddLocationButtonProps) => {
    const AddLocationButtonActiveClass = props.isSelected ? classes.AddLocationButtonActive : classes.AddLocationButtonInactive;
    const TitleActiveClass = props.isSelected ? classes.TitleActive : classes.TitleInactive;

    //size selector
    let SizeClass = ''
    switch(props.range){
        case 25: SizeClass = classes.Range25; break;
        case 50: SizeClass = classes.Range50; break;
        case 100: SizeClass = classes.Range100; break;
    }


    return (
        <Container className={classes.Container} >
            <div className={[classes.AddLocationButton, AddLocationButtonActiveClass, SizeClass].join(' ')} onClick={e => props.selectLocationRange(e, props.range)}></div>
            <div className={[classes.Title, TitleActiveClass].join(' ')} onClick={e => props.selectLocationRange(e, props.range)}>{props.title}</div>
        </Container>
    )
}

export default AddLocationButton
