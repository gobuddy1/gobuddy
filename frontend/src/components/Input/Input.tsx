import React from 'react'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEnvelope, faUser, faPhone, faLock, faKey } from '@fortawesome/free-solid-svg-icons'

import { Form, Row, InputGroup } from 'react-bootstrap'
import classes from './Input.module.css'


const Input = (props: any) => {
    let classArray: string[] = [classes.Input];
    if(props.valid && props.touched){
        classArray.push(classes.Valid)
    }
    if(!props.valid && props.touched){
        classArray.push(classes.Invalid)
    }

    let icon = faUser;
    switch(props.name){
        case 'firstname':
            icon = faUser;
            break;
        case 'lastname':
            icon = faUser;
            break;
        case 'email':
            icon = faEnvelope;
            break;
        case 'telnumber':
            icon = faPhone;
            break;
        case 'password':
            icon = faLock;
            break;
        case 'passwordVerified':
            icon = faKey;
            break;

    }

    return (
        <Form.Group controlId={props.controlId}>
            <InputGroup>
            <InputGroup.Prepend>
                <InputGroup.Text className={classes.InputIcon}>
                    <FontAwesomeIcon icon={icon} className={classes.Icon} />
                </InputGroup.Text>
            </InputGroup.Prepend>
            {/* <Form.Label className={classes.Label}>{props.children}</Form.Label> */}
            <Form.Control
                type={props.type}
                name={props.name}
                value={props.value}
                placeholder={props.placeholder}
                className={classArray.join(' ')}
                onChange={(event: any) => props.onChange(event)}
                onBlur={props.onBlur}
            />
        </InputGroup>
        </Form.Group>
        
    )
}

export default Input
