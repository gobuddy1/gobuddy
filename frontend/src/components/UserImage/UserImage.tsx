import React from 'react'

import { Image } from 'react-bootstrap';
import classes from './UserImage.module.css'

const UserImage = (props: any) => {
    return (
        <Image roundedCircle src={process.env.REACT_APP_HOST + props.src} className={classes.UserImage}/>
    )
}

export default UserImage
