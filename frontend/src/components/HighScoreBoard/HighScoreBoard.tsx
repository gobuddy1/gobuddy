import React from 'react'
import { Container } from 'react-bootstrap';

import classes from './HighScoreBoard.module.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTwitter, faInstagram, faFacebook } from '@fortawesome/free-brands-svg-icons'

const HighScoreBoard = (props: any) => {
    return (
        <Container fluid className={classes.Container}>
            <div className={classes.HighScore}>Highscore</div>
            {props.users.sort((a: any, b: any) => b.score-a.score).map((user: any) => {
                return  <div key={user.id} className={[classes.HighScoreName, user.token.toString() === props.token.toString() ? classes.CurrentUser : ''].join(' ')}>
                            <div className={classes.Name}>
                                {user.name}
                            </div>
                            <div className={classes.Score}>
                                {user.score}
                            </div>
                        </div>
            })}
            <div className={classes.Text}>Share your score:</div>
            <FontAwesomeIcon className={classes.FaIcon} icon={faTwitter}/>
            <FontAwesomeIcon className={classes.FaIcon} icon={faInstagram}/>
            <FontAwesomeIcon className={classes.FaIcon} icon={faFacebook}/>
        </Container>
    )
}

export default HighScoreBoard
