import React, { useEffect, useState, useRef } from 'react'
import { Route, Redirect} from 'react-router-dom'

import { GOBUDDY_LS_TOKEN, GOBUDDY_LS_TOKEN_EXPIRY_DATE} from '../../../constants/localstoragenames'

const AuthRoute = (props: any) => {
    
    // const [isAuthenticated, setIsAuthenticated] = useState<boolean | null>(null);
    const loadingRef = useRef<boolean | null>(null)
    
    // useEffect(() => {
    //     let token = localStorage.getItem(GOBUDDY_LS_TOKEN);
    //     const date = localStorage.getItem(GOBUDDY_LS_TOKEN_EXPIRY_DATE);
    //     if(date){
    //         const tokenExpiration = new Date(date);
    //         if(token){
    //             const dateNow = new Date();
    //             if(tokenExpiration.getTime() < dateNow.getTime()){
    //                 // setIsAuthenticated(false);
    //                 loadingRef.current = false;
    //             }else{
    //                 // setIsAuthenticated(true);
    //                 loadingRef.current = true
    //             }
    //         }
    //     }

    // }, [])

    let token = localStorage.getItem(GOBUDDY_LS_TOKEN);
    const date = localStorage.getItem(GOBUDDY_LS_TOKEN_EXPIRY_DATE);
    if(date){
        const tokenExpiration = new Date(date);
        if(token){
            const dateNow = new Date();
            if(tokenExpiration.getTime() < dateNow.getTime()){
                // setIsAuthenticated(false);
                loadingRef.current = false;
                localStorage.clear();

            }else{
                // setIsAuthenticated(true);
                loadingRef.current = true
                
            }
        }
    }
    const Component = props.component;
    const rest = {exact: props.exact, path: props.path}
    console.log(loadingRef.current)
    return (
        <Route {...rest} render={props =>
            (!(loadingRef.current) || (loadingRef.current === null)) ? (
                <Redirect to='/login'/>
            ) : (
                <Component {...props} />
            )}>

        </Route>
    )
}

export default AuthRoute
