import React, { useState, useEffect } from 'react'
import { useHistory, Link } from 'react-router-dom';
import { Form, Container, Row, InputGroup } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEnvelope } from '@fortawesome/free-regular-svg-icons'
import { faLock } from '@fortawesome/free-solid-svg-icons';

import { postUser } from '../../../shared/backend/backend-service'
import { GOBUDDY_LS_TOKEN, GOBUDDY_LS_UUID, GOBUDDY_LS_TOKEN_EXPIRY_DATE, GOBUDDY_LS_USER}  from '../../../constants/localstoragenames'
import { isAuthenticated } from '../../../util/logouthandler'

import classes from './Login.module.css';
import Logo from '../../Logo/Logo';
import ButtonUI from '../../UI/ButtonUI/ButtonUI'
import SignupMessage from '../SignupMessage/SignupMessage';
import ErrorMessage from '../../UI/ErrorMessage/ErrorMessage';


interface login{
    email: string,
    password: string
}

const LoginForm = () => {
    const history = useHistory();

    const [credentials, setCredentials] = useState<login>({
        email: '',
        password: ''
    })

    const [loginSuccess, setLoginSuccess] = useState({
        message: '',
        success: false
    })

    // useEffect(() => {
    //     isAuthenticated() ? history.push('/') : history.push('/login')
    // }, [])

    const loginHandler = (event: any) => {
        postUser(event, {email: credentials.email, password: credentials.password})  
        .then(resData => {
            console.log(resData);
            localStorage.setItem(GOBUDDY_LS_TOKEN, resData.token);
            localStorage.setItem(GOBUDDY_LS_UUID, resData.userId);
            const remainingMilliseconds = 60 * 60 * 1000;
            const timeNow = new Date();
            console.log(timeNow.toISOString())
            const expiryDate = new Date(
              new Date().getTime() + remainingMilliseconds
            );
            localStorage.setItem(GOBUDDY_LS_TOKEN_EXPIRY_DATE, expiryDate.toISOString());
            setAutoLogout(remainingMilliseconds);
            setLoginSuccess({...loginSuccess, success: true, message: 'Login succeeded'});
            history.push('/digital-gobuddy-main')
        })
        .catch(error => {
            console.log(error);
            setLoginSuccess({...loginSuccess, success: false, message: error.message});
        });
    }

    const logoutHandler = () => {
        localStorage.removeItem(GOBUDDY_LS_TOKEN);
        localStorage.removeItem(GOBUDDY_LS_TOKEN_EXPIRY_DATE);
        localStorage.removeItem(GOBUDDY_LS_UUID);
        localStorage.removeItem(GOBUDDY_LS_USER);
        setLoginSuccess({...loginSuccess, success: false, message: 'Logged out!'});
    };

    const setAutoLogout = (milliseconds: number) => {
        setTimeout(() => {
            logoutHandler();
        }, milliseconds);
    };

    const inputChangeHandler = (event: any) => {
        const value = event.target.value;
        setCredentials({...credentials, [event.target.name]: value})
    }

    const loginMessage = loginSuccess.message
    return (
        <Container className={classes.Login}>
            <Row className="justify-content-center">
                <Logo className={classes.Logo}/>
            </Row>
            <div className={classes.MainText}>Welcome to GoBuddy!</div>
            {/* <SignupMessage>{loginMessage}</SignupMessage> */}
            <ErrorMessage show={loginMessage !== ''}>{loginMessage}</ErrorMessage>
            <Form onSubmit={e => loginHandler(e)}>
                <Form.Group controlId="emailInput">
                <InputGroup>
                    <InputGroup.Prepend>
                        <InputGroup.Text className={classes.InputIcon}>
                            <FontAwesomeIcon icon={faEnvelope} className={classes.Icon} />
                        </InputGroup.Text>
                    </InputGroup.Prepend>
                    <Form.Control 
                        name="email"
                        type="email" 
                        placeholder="Email" 
                        className={classes.InputStyle}
                        value={credentials.email}
                        onChange={event => {inputChangeHandler(event)}}/>
                    </InputGroup>
                </Form.Group>
                <Form.Group controlId="passwordInput">
                <InputGroup>
                    <InputGroup.Prepend>
                        <InputGroup.Text className={classes.InputIcon}>
                            <FontAwesomeIcon icon={faLock} className={classes.Icon} />
                        </InputGroup.Text>
                    </InputGroup.Prepend>
                    <Form.Control 
                        name="password"
                        type="password" 
                        placeholder="Password" 
                        className={classes.InputStyle}
                        value={credentials.password}
                        onChange={event => {inputChangeHandler(event)}}/>
                    </InputGroup>
                </Form.Group>
                <ButtonUI>
                    Sign In
                </ButtonUI>
            </Form>
            <Row className="justify-content-center">
                <Link className={classes.Link} to="/signup">Don't have an account? Sign up here!</Link>
            </Row>
        </Container>
    )
}

export default LoginForm
