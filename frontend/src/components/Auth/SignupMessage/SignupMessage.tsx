import React from 'react'

import classes from './SignupMessage.module.css'

const SignupMessage = (props: any) => {
    return (
        <div className={classes.SignupMessage}>{props.children}</div>
    )
}

export default SignupMessage
