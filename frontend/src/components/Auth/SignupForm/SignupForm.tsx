import React, { useState, useRef, useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { Form, Container } from 'react-bootstrap'

import { requiredValidator, lengthValidator, emailValidator } from '../../../util/validators'
import {putUser} from '../../../shared/backend/backend-service'
import { isAuthenticated } from '../../../util/logouthandler'
import { generateBase64FromImage } from '../../../util/image'

import classes from './SignupForm.module.css';
import ButtonUI from '../../UI/ButtonUI/ButtonUI'
import SignupMessage from '../SignupMessage/SignupMessage'
import Input from '../../Input/Input'
import ImageSelector from '../../ImageSelector/ImageSelector';
import ErrorMessage from '../../UI/ErrorMessage/ErrorMessage'

interface signup{
    value: string,
    valid: boolean,
    touched: boolean,
    validators: any
}

interface status{
    success: boolean,
    message: string
}

const SignupForm = (props: any) => {
    const history = useHistory();

    const [imagePreview, setImagePreview] = useState<any>(null);
    const [image, setImage] = useState<any>(null);

    const [email, setEmail] = useState<signup>({
        value: '',
        valid: false,
        touched: false,
        validators: [requiredValidator, emailValidator]
    })
    const [password, setPassword] = useState<signup>({
        value: '',
        valid: false,
        touched: false,
        validators: [requiredValidator, lengthValidator({ min: 5 })]
    })
    const [passwordVerified, setPasswordVerified] = useState<signup>({
        value: '',
        valid: false,
        touched: false,
        validators: [requiredValidator, lengthValidator({ min: 5 })]
    })
    const [firstname, setFirstname] = useState<signup>({
        value: '',
        valid: false,
        touched: false,
        validators: [requiredValidator]
    })
    const [lastname, setLastname] = useState<signup>({
        value: '',
        valid: false,
        touched: false,
        validators: [requiredValidator]
    })
    const [telnumber, setTelnumber] = useState<signup>({
        value: '',
        valid: false,
        touched: false,
        validators: [requiredValidator]
    })

//    const [formIsValid, setFormIsValid] = useState<boolean>(false)
    const formIsValid = useRef<boolean>(false)

    const [signupSuccess, setSignupSuccess] = useState<status>({
        success: false,
        message: '',
    })

    const [dataVerified, setDataVerified] = useState<boolean>(false)

    // useEffect(() => {
    //     isAuthenticated() ? history.push('/') : history.push('/signup')
    // }, [])

   const verifyInput = (validators: any, value: string) => {
        let isValid = true;
        for (const validator of validators) {
            isValid = isValid && validator(value);
        }
        return isValid
   }
    
    const inputChangeHandler = (event: any) => {
        let value: string = event.target.value;
        switch(event.target.name){
            case 'firstname':
                setFirstname({...firstname, value: value, valid: verifyInput(firstname.validators, value)})
                break;
            case 'lastname':
                setLastname({...lastname, value: value, valid: verifyInput(lastname.validators, value)})
                break;
            case 'password':
                setPassword({...password, value: value, valid: verifyInput(password.validators, value)})
                break;
            case 'passwordVerified':
                const passwordIsVerified = password.value === value;
                setPasswordVerified({...passwordVerified, value: value, valid: (passwordIsVerified && verifyInput(passwordVerified.validators, value))})
                break;
            case 'telnumber':
                setTelnumber({...telnumber, value: value, valid: verifyInput(telnumber.validators, value)})
                break;
            case 'email':
                setEmail({...email, value: value, valid: verifyInput(email.validators, value)})
                break;
        }
        const formIsValidNew: boolean = lastname.valid && firstname.valid && email.valid && password.valid && passwordVerified.valid && telnumber.valid && dataVerified
        formIsValid.current = formIsValidNew
    }

    const inputBlurHandler = (inputType: any) => {
        console.log(inputType)
        switch(inputType){
            case 'firstname':
                setFirstname({...firstname, touched: true})
                break;
            case 'lastname':
                setLastname({...lastname, touched: true})
                break;
            case 'email':
                setEmail({...email, touched: true})
                break;
            case 'password':
                setPassword({...password, touched: true})
                break;
            case 'passwordVerified':
                setPasswordVerified({...passwordVerified, touched: true})
                break;
            case 'telnumber':
                setTelnumber({...telnumber, touched: true})
                break;
        }
    }

    const signupHandler = (event: any) => {
        const data = {
            firstname: firstname, 
            lastname: lastname, 
            email: email, 
            password: password, 
            telnumber: telnumber,
            image: image
        }
        putUser(event, data)  
        .then(resData => {
            console.log(resData);
            return setSignupSuccess({...signupSuccess, success: true, message: 'User created successfully!'});
        })
        .catch(error => {
            console.log(error);
            return setSignupSuccess({...signupSuccess, success: false, message: error.message});
        });
    }

    const onCheckboxChange = () => {
        const newVerified = !dataVerified
        setDataVerified(newVerified)
        const formIsValidNew: boolean = lastname.valid && firstname.valid && email.valid && password.valid && passwordVerified.valid && telnumber.valid && newVerified
        formIsValid.current = formIsValidNew
    }

    const onImageUpload = (event: any) => {
        const files = event.target.files;
        console.log(files)
        setImage(files[0])
        if(files[0]) {
            generateBase64FromImage(files[0])
              .then(b64 => {
                // this.setState({ imagePreview: b64 });
                setImagePreview(b64);
              })
              .catch(e => {
                // this.setState({ imagePreview: null });
                setImagePreview(null);
              });
          }
    }

    const signupMessage = signupSuccess.message
    let form: any = <Container><Form>
        <Input
            controlId="firstnameInput"
            type="text"
            name="firstname"
            placeholder="Enter First Name"
            onChange={(event: any) => inputChangeHandler(event)}
            onBlur={() => inputBlurHandler('firstname')}
            value={firstname.value}
            touched={firstname.touched}
            valid={firstname.valid}
            >First Name
        </Input>
        <Input
            controlId="lastnameInput"
            type="text"
            name="lastname"
            placeholder="Enter Last Name"
            onChange={(event: any) => inputChangeHandler(event)}
            onBlur={() => inputBlurHandler('lastname')}
            value={lastname.value}
            touched={lastname.touched}
            valid={lastname.valid}
            >Last Name
        </Input>
        <Input
            controlId="emailInput"
            type="email"
            name="email"
            placeholder="Enter Email"
            onChange={(event: any) => inputChangeHandler(event)}
            onBlur={() => inputBlurHandler('email')}
            value={email.value}
            touched={email.touched}
            valid={email.valid}
            >Email
        </Input>
        <ImageSelector imagePreview={imagePreview} onChange={onImageUpload}/>
        <Input
            controlId="numberInput"
            type="tel"
            name="telnumber"
            placeholder="Enter Phone Number"
            onChange={(event: any) => inputChangeHandler(event)}
            onBlur={() => inputBlurHandler('telnumber')}
            value={telnumber.value}
            touched={telnumber.touched}
            valid={telnumber.valid}
            >Telephone Number
        </Input>
        <Input
            controlId="passwordInput"
            type="password"
            name="password"
            placeholder="Enter Password"
            onChange={(event: any) => inputChangeHandler(event)}
            onBlur={() => inputBlurHandler('password')}
            value={password.value}
            touched={password.touched}
            valid={password.valid}
            >Password
        </Input>
        <Input
            controlId="passwordVerifiedInput"
            type="password"
            name="passwordVerified"
            placeholder="Enter Password Again"
            onChange={(event: any) => inputChangeHandler(event)}
            onBlur={() => inputBlurHandler('passwordVerified')}
            value={passwordVerified.value}
            touched={passwordVerified.touched}
            valid={passwordVerified.valid}
            >Repeat Password
        </Input>
        <Form.Check
            type='checkbox'
            label='I agree that my information will be saved and handled according to GoBuddys terms and conditions'
            id='checkbox'
            onChange={onCheckboxChange}
            className={classes.FormCheck}
        />
    </Form>
    <ButtonUI clicked={signupHandler} disabled={!formIsValid.current}>Sign Up</ButtonUI>
    </Container>
    if(signupSuccess.success){
        form = <Link to='/login' className={classes.Link}>Return to login page</Link>
    }
    return (
       <Container className={classes.SignupForm} >
           <div className={classes.MainText}>Signup to GoBuddy!</div>
           <SignupMessage>{signupMessage}</SignupMessage>
            {/* <ErrorMessage show={signupMessage!==''}>{signupMessage}</ErrorMessage> */}
           {form}
            <Link className={classes.Link} to="/login">{signupSuccess.success ? "" : "Already have an account? Login here!"}</Link>
       </Container>
    )
}

export default SignupForm
