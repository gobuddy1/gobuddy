import React from 'react';
import { Image } from 'react-bootstrap'

import goBuddyLogo from '../../assets/images/logo192.png'
// import goBuddyLogo from '../../shared/Logo.svg'
import classes from './Logo.module.css'

const Logo = (props: any) => {
    return (
        <Image src={goBuddyLogo} className={[classes.Logo, props.className].join(' ')} fluid />
    )
}

export default Logo
