import React, { useContext, useState, useEffect, useRef } from 'react'
import { Container, Image } from 'react-bootstrap';

import { QuestionContext } from '../../context/QuestionContext'
import { UsersContext } from '../../context/UsersContext/usersContext';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUsers } from '@fortawesome/free-solid-svg-icons';

import classes from './QuizView.module.css'
import AnswerButton from '../UI/ButtonUI/AnswerButton/AnswerButton';
import { IAnswer } from '../../shared/interfaces/question.interface';
import CountDown from '../UI/CountDown/CountDown';
import { TimeCountContext } from '../../context/TimeCountContext';
import { SocketContext } from '../../context/socketContext';
import { GOBUDDY_LS_UUID } from '../../constants/localstoragenames';
import { IQuiz } from '../../shared/interfaces/quiz.interface';

import UserDisplay from '../UserDisplay/UserDisplay';

type QuizViewProps = {
    quiz: IQuiz | undefined
}

const QuizView = (props: QuizViewProps) => {
    const { 
        question, 
        lockedAnswer, putLockedAnswer, 
        showCorrectAnswer, putShowCorrectAnswer, 
        showDescription, putShowDescription } = useContext(QuestionContext)

    const { counter, putCounter } = useContext(TimeCountContext)
    const { users } = useContext(UsersContext)
    const socket = useContext(SocketContext);
    const [uuid, setUuid] = useState(localStorage.getItem(GOBUDDY_LS_UUID));
    const [ isAdminUser, setIsAdminUser ] = useState(false);
    // const [ isAnswerLocked, setIsAnswerLocked ] = useState(false);
    const isAnswerLocked = useRef(false);

    // const [counter, setCounter] = useState(10);

    const onLockAnswer = (event: object, answer: IAnswer) => {
        let points = 0;
        putLockedAnswer(answer);
        points = computeScore(answer);
        socket.emit('setScore', points);
        // setIsAnswerLocked(true);
        isAnswerLocked.current = true;
    }

    const computeScore = (answer: IAnswer) => {
        return answer.correct ? 100*counter : 0;
    }

    const revealAnswer = () => {
        putShowCorrectAnswer(true);
        putCounter(0); // reset counter
    }

    useEffect(() => {
        // If we have an answer locked this variable should be locked
        isAnswerLocked.current = !(lockedAnswer._id === undefined)
    }, [lockedAnswer]);

    useEffect(() => {
        counter > 0 && setTimeout(() => putCounter(counter - 1), 1000);
        if(counter === 0){ revealAnswer() }
    }, [counter]);

    useEffect(() => {
        setIsAdminUser(props.quiz?.createdby.toString() === uuid?.toString());
    }, [])

    let view =  <Container fluid>
                    <FontAwesomeIcon icon={faUsers} className={classes.UsersIcon}/>
                    <p className={classes.UsersText}>{users.length + ` player${users.length > 1 ? "s" : ""} active`}</p>
                    <UserDisplay users={users} />
                </Container>

    if(question._id){
        if(showDescription){
            view =  <Container fluid className={classes.ViewContainer}>
                        <Image className={classes.Image} src={'' + process.env.REACT_APP_HOST + question.image}/>
                        <p className={classes.Description}>{question.description}</p>
                    </Container>
        }else{
            view =  <Container fluid className={classes.ViewContainer}>
                        <Image className={classes.Image} src={'' + process.env.REACT_APP_HOST + question.image}/>
                        <p>{question.question}</p>
                        <CountDown time={counter}/>
                        {question?.answers?.map((answer: IAnswer) => {
                            return <AnswerButton 
                                        lockedAnswer={lockedAnswer} 
                                        answer={answer}
                                        key={answer._id} 
                                        onClick={(e: object) => onLockAnswer(e, answer)}
                                        disabled={(isAnswerLocked.current || showCorrectAnswer)}
                                        showCorrectAnswer={showCorrectAnswer}>
                                {answer.answer}</AnswerButton>
                        })}
                    </Container>
        }
    }

    return (
        <Container fluid className={classes.Container}>
            {view}
        </Container>
    )
}

export default QuizView