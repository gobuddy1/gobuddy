import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { faTimes, faTrashAlt, faEdit } from '@fortawesome/free-solid-svg-icons';

import classes from './MarkerSummary.module.css'
import { ILocation } from '../../shared/interfaces/location.interface'
import { Container } from 'react-bootstrap';

type MarkerSummaryProps = {
    location: ILocation,
    menuOpen: boolean,
    unSelectMarkerSummary: () => void,
    deleteLocation: () => void,
    editLocation: () => void
}

const FIXED_POINT: number = 6;

const MarkerSummary = (props: MarkerSummaryProps) => {
    let MarkerSummaryStateClass = !props.menuOpen ? classes.MarkerSummaryClosed : null;
    
    return (
        <Container fluid className={[classes.MarkerSummary, MarkerSummaryStateClass].join(' ')}>
            <h1 className={classes.Title}>{props.location.name ? props.location.name : 'No name location'} {props.location.range ? '(' + props.location.range + 'm)' : null}</h1>
            <div className={classes.Summary}>
                <p>{props.location.description}</p>
                <p>{props.location.latitude.toFixed(FIXED_POINT)} {props.location.longitude.toFixed(FIXED_POINT)}</p>
            </div>
            <Container className={classes.Icon} onClick={props.unSelectMarkerSummary}>
                <FontAwesomeIcon icon={faTimes} />
            </Container>
            <FontAwesomeIcon onClick={props.deleteLocation} className={classes.DeleteIcon} icon={faTrashAlt}/>
            <FontAwesomeIcon onClick={props.editLocation} className={classes.EditIcon} icon={faEdit}/>
        </Container>
    )
}

export default MarkerSummary
