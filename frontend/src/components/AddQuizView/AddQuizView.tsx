import React, { useState } from 'react'
import { useHistory } from 'react-router-dom';

import classes from './AddQuizView.module.css'
import { Container, Form } from 'react-bootstrap';
import ButtonUI from '../UI/ButtonUI/ButtonUI';
import { GOBUDDY_LS_TOKEN } from '../../constants/localstoragenames';
import { postQuiz } from '../../shared/backend/backend-service';

type AddQuizViewProps = {

}


const AddQuizView = (props: AddQuizViewProps) => {
    const history = useHistory();

    const [name, setName] = useState<string>('');
    const [description, setDescription] = useState<string>('');
    const [error, setErrorMessage] = useState<string>('')
    

    const onChangeInput = (e: any) => {
        const type = e.target.type;
        const value = e.target.value;
        switch(type){
            case 'text':
                setName(value);
                break;
            case 'textarea':
                setDescription(value);
                break;
        }
    }

    const submitForm = (e: any) => {
        e.preventDefault();
        const token = localStorage.getItem(GOBUDDY_LS_TOKEN);
        const data = {
            name: name,
            description: description
        }
        postQuiz(token, data)
        .then(response => {
            if(response.code){
                history.push('/quiz/add-questions/' + response.code);
            }else{
                throw new Error('Could not create challenge!')
            }
        })
        .catch(error => {
            setErrorMessage(error.message);
            console.log(error.message);
        })
    }

    return (
        <Container fluid className={classes.AddQuizViewContainer}>
            <Form className={classes.Form} onSubmit={submitForm}>
                <Form.Group className={classes.FormGroup}>
                    <Form.Label className={classes.InputLabel}>Title</Form.Label>
                    <Form.Control
                        type="text"
                        className={classes.Input}
                        onChange={onChangeInput}
                        autoComplete="off"
                    />
                    <Form.Label className={classes.InputLabel}>Description</Form.Label>
                    <Form.Control
                        as="textarea"
                        className={[classes.Input, classes.TextArea].join(' ')}
                        onChange={onChangeInput}
                        rows={3}
                        autoComplete="off"
                    />
                    <ButtonUI>Create</ButtonUI>
                </Form.Group>
            </Form>
        </Container>
    )
}

export default AddQuizView
