import { faImage } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react'
import { Form, Image, Container } from 'react-bootstrap';

import classes from './ImageSelector.module.css'

type ImageSelectorProps = {
    imagePreview: string,
    onChange?: ((event: React.ChangeEvent<HTMLInputElement>) => void) | undefined
}

const ImageSelector = (props: ImageSelectorProps) => {

    return (
        <Container className={classes.ImageSelectorContainer}>
            <Form.Group className={classes.FileUpload}>
                <Form.Label className={classes.CustomFileUpload}>
                    {props.imagePreview ?   <Image className={classes.Thumbnail} src={props.imagePreview}/> : 
                                            <FontAwesomeIcon className={classes.Icon} icon={faImage}></FontAwesomeIcon>}
                    <Form.Control onChange={props.onChange} id="challengimage" type="file" />
                </Form.Label>
            </Form.Group>
        </Container>
    )
}

export default ImageSelector
