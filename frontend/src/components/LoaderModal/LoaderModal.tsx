import React from 'react'
import { Container } from 'react-bootstrap';

import Loader from '../UI/Loader/Loader';
import classes from './LoaderModal.module.css'

type LoaderModalProps = {
    message: string
}

const LoaderModal = (props: LoaderModalProps) => {
    return (
        <Container fluid className={classes.LoaderModal}>
            <Loader />
            <p className={classes.Text}>{props.message}</p>
        </Container>
    )
}

export default LoaderModal
