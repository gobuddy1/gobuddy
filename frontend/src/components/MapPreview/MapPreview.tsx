import React, { useState, useEffect } from 'react'
import ReactMapGL from 'react-map-gl';
import { Container } from 'react-bootstrap';
import MapMarker from '../MapMarker/MapMarker';
import { IChallenge } from '../../shared/interfaces/challenge.interface';
import { PREVIEW } from '../../constants/locationtypes';

const MAP_STYLE = "mapbox://styles/romori/cki3k629c4la719lrdw3568q9";

type MapPreviewProps = {
    longitude: number,
    latitude: number,
    challenge: IChallenge
}

const MapPreview = (props: MapPreviewProps) => {

    const [viewport, setViewport] = useState({
        width: window.innerWidth * 0.6,
        height: window.innerHeight * 0.6,
        latitude: 0,
        longitude: 0,
        zoom: 15
    })

    // useEffect(() => {
    //     setViewport({...viewport, width: window.innerWidth * 0.6, height: window.innerHeight * 0.6})
    // }, [window.innerWidth, window.innerHeight])

    useEffect(() => {
        const handleResize = () => setViewport({...viewport, width: window.innerWidth * 0.6, height: window.innerHeight * 0.6});
        window.addEventListener("resize", handleResize);
        return () => {
          window.removeEventListener("resize", handleResize);
        };
    });

    useEffect(() => {
        const newViewport = {...viewport, latitude: props.latitude, longitude: props.longitude, zoom: 15}
        setViewport(newViewport)
    }, [props.longitude, props.latitude])


    return (
        <Container>
            <ReactMapGL 
                {...viewport}
                mapboxApiAccessToken={process.env.REACT_APP_MAPBOX_ACCESS_TOKEN}
                mapStyle={MAP_STYLE}
                onViewportChange={viewport => {
                    const newViewport = {...viewport}
                    setViewport(newViewport);
                }}>
                    {props.challenge.locations.map(location => {
                    return <MapMarker
                        key={location._id}
                        id={'question-location-marker'}
                        location={location}
                        long={location.longitude}
                        lat={location.latitude}
                        type={PREVIEW}
                        unlocked={true}
                        offsetLeft={-22}
                        offsetTop={-22}
                    ></MapMarker>})}
                </ReactMapGL>
        </Container>
    )
}

export default MapPreview
