import React from 'react'
import { Marker } from 'react-map-gl';

import { ILocation } from '../../shared/interfaces/location.interface';

import classes from './MapMarker.module.css'
import { USER_LOCATION, GAMES, PREVIEW } from '../../constants/locationtypes'

interface MapMarkerProps {
    key: string,
    id?: string,
    long: number,
    lat: number,
    type: string,
    unlocked: boolean,
    offsetLeft: number,
    offsetTop: number,
    onClick?: ((event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void) | undefined,
    selectedLocation?: ILocation | undefined,
    location: ILocation,
}

const MapMarker = (props: MapMarkerProps) => {

    //Conditional css
    let markerClasses: string[] = [classes.MapMarker];
    switch(props.type){
        case USER_LOCATION: 
            markerClasses.push(classes.UserLocationMarker);
            break;
        case GAMES: 
            markerClasses.push(classes.GameMarker);
            if(props.unlocked){
                markerClasses.push(classes.ActiveMarker)
            }
            break;
        case PREVIEW:
            markerClasses.push(classes.PreviewMarker);
    }

    if(props.selectedLocation === props.location){
        markerClasses.push(classes.SelectedLocation)
    }
    

    return (
            <Marker
                latitude={props.lat}
                longitude={props.long}
                offsetLeft={props.offsetLeft}
                offsetTop={props.offsetTop}>
                <div id={props.id} onClick={props.onClick} className={markerClasses.join(' ')}></div>
                {props.type !== USER_LOCATION ? <div className={classes.LocationName}>{props.location.name}</div> : null}
            </Marker>
    )
}

export default MapMarker
