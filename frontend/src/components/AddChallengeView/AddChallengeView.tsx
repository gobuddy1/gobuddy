import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom';
import { Container, Form } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes, faLock, faLockOpen } from '@fortawesome/free-solid-svg-icons';
import ButtonUI from '../UI/ButtonUI/ButtonUI';
import { postChallenge } from '../../shared/backend/backend-service'
import { GOBUDDY_LS_TOKEN } from '../../constants/localstoragenames';
import { generateBase64FromImage } from '../../util/image'

import classes from './AddChallengeView.module.css'
import ImageSelector from '../ImageSelector/ImageSelector';

type AddChallengeViewProps = {
    show: boolean,
    onClick: ((event: any) => void) | undefined
}

const AddChallengeView = (props: AddChallengeViewProps) => {
    const history = useHistory();

    const [switchState, setSwitchState] = useState<boolean>(true);
    const [challengeName, setChallengeName] = useState<string>('');
    const [challengeDescription, setChallengeDescription] = useState<string>('');
    const [imagePreview, setImagePreview] = useState<any>(null);
    const [image, setImage] = useState<any>(null);
    const [errorMessage, setErrorMessage] = useState('')

    useEffect(() => {
        setImagePreview(null)
        setImage(null)
    }, [props.show])

    const onEnterName = (event: any) => {
        setChallengeName(event.target.value);
    }
    
    const onEnterDescription = (event: any) => {
        setChallengeDescription(event.target.value);
    }
    
    const onTogglePublicState = (event: any) => {
        setSwitchState(!switchState);
    }

    const createChallenge = () => {
        const token = localStorage.getItem(GOBUDDY_LS_TOKEN);
        const data = {
            name: challengeName,
            description: challengeDescription,
            public: switchState,
            image: image
        }
        postChallenge(token, data)
        .then(response => {
            if(response.challengeId){
                history.push('/challenges/' + response.challengeId)
            }else{
                throw new Error('Could not create challenge!')
            }
        })
        .catch(error => {
            setErrorMessage(error.message);
        })
    }

    const onImageUpload = (event: any) => {
        const files = event.target.files;
        console.log(files)
        setImage(files[0])
        if(files[0]) {
            generateBase64FromImage(files[0])
              .then(b64 => {
                // this.setState({ imagePreview: b64 });
                setImagePreview(b64);
              })
              .catch(e => {
                // this.setState({ imagePreview: null });
                setImagePreview(null);
              });
          }
    }
    
    const content = props.show ? 
    <Container fluid className={classes.AddChallengeView}>
        <Container fluid className={classes.FormContainer}>
            <h1 className={classes.CreateChallengeTitle}>Create a Gobuddy!</h1>
            <p>{errorMessage}</p>
            <Form className={classes.Form}>
                <Form.Group controlId="challengeForm.Name">
                    <Form.Control type="text" placeholder="Gobuddy name" onChange={onEnterName} />
                </Form.Group>
                <Form.Group controlId="challengeForm.Description">
                    <Form.Control className={classes.TextArea} as="textarea" rows={3} placeholder="Gobuddy description" onChange={onEnterDescription}/>
                </Form.Group>
                <Container className={classes.PublicSwitch}>
                    <Form.Check 
                        type='switch'
                        id={'switch-challenge-form'}
                        label={'Public Gobuddy'}
                        checked={switchState}
                        onChange={onTogglePublicState}
                        className={classes.Switch}
                    />
                    <FontAwesomeIcon className={classes.SwitchIcon} icon={switchState ? faLockOpen : faLock} />
                </Container>
                <ImageSelector onChange={onImageUpload} imagePreview={imagePreview}/>
            </Form>
            <Container fluid>
                <ButtonUI className={classes.CreateButton} clicked={createChallenge}>Create</ButtonUI>
                <ButtonUI className={classes.CancelButton} clicked={props.onClick}>Cancel</ButtonUI>
            </Container>
        </Container>
        <FontAwesomeIcon className={classes.Icon} onClick={props.onClick} icon={faTimes}/>

    </Container> : null
    return (
        <Container fluid>
            {content}
        </Container>
    )
}

export default AddChallengeView
