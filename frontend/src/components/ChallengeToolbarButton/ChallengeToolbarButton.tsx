import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import classes from './ChallengeToolbarButton.module.css';

const ChallengeToolbarButton = (props: any) => {
    let ButtonActive = props.active ? classes.ButtonActive : ''
    return (
        <FontAwesomeIcon className={[classes.ChallengeToolbarButton, ButtonActive].join(' ')} icon={props.icon} onClick={props.onClick}/>
    )
}

export default ChallengeToolbarButton
