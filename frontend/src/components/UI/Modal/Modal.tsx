import React from 'react'

import { Container } from 'react-bootstrap';
import classes from './Modal.module.css'
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

type ModalProps = {
    children: JSX.Element | string,
    width?: number,
    height?: number,
    show: boolean,
    onClose: ((event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void) | undefined
}

const Modal = (props: ModalProps) => {
    return (
        <Container fluid className={classes.ModalContainer}>
            {props.show ? <Container fluid className={classes.Modal}>
                <Container fluid className={classes.ModalBack} onClick={props.onClose}/>
                <Container fluid className={classes.ModalBody}>
                    <Container fluid className={classes.Icon} onClick={props.onClose}>
                        <FontAwesomeIcon icon={faTimes}/>
                    </Container>
                    {props.children}
                </Container>
            </Container> : null}
        </Container>
    )
}

export default Modal
