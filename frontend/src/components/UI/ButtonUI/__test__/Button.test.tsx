import React from 'react'

import Enzyme, { shallow, render, mount, configure } from 'enzyme';
import toJson from 'enzyme-to-json';
import Adapter from 'enzyme-adapter-react-16';
import sinon from 'sinon'

configure({ adapter: new Adapter() }) //Connect enzyme

import Button from './../ButtonUI'

describe('<ButtonUI />', () => {
    // Snapshot testing - not testing much
    it('renders snapshot correctly', () => {
        const wrapper = shallow(<Button>Button test</Button>)
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    // it('props are working', () => {
    //     const wrapper = shallow(<Button>Button test</Button>);
    //     // console.log(wrapper.setProps({disabled: true}).debug())
    //     console.log(wrapper.setProps({disabled: true}).debug())
    //     expect(1+1).toBe(2)
    // });

})