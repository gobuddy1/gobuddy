import React from 'react'
import { Button } from 'react-bootstrap'

import classes from './ButtonUI.module.css'

type ButtonUiProp = {
    className?: string,
    clicked?: ((event: React.MouseEvent<HTMLElement, MouseEvent>) => void) | undefined,
    disabled?: boolean | undefined,
    children: string
}

const ButtonUI = (props: ButtonUiProp) => {
    return (
        <Button className={[classes.GoBuddyButton, props.className].join(' ')} variant="primary" type="submit" onClick={props.clicked} disabled={props.disabled}>
            {props.children}
        </Button>
    )
}

export default ButtonUI
