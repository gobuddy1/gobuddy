import React from 'react'
import { Button } from 'react-bootstrap'
import { IAnswer } from '../../../../shared/interfaces/question.interface'

import classes from './AnswerButton.module.css'

type AnswerButtonUiProp = {
    className?: string,
    onClick?: ((event: React.MouseEvent<HTMLElement, MouseEvent>) => void) | undefined,
    disabled?: boolean | undefined,
    children: string,
    lockedAnswer?: IAnswer,
    answer?: IAnswer,
    showCorrectAnswer?: boolean
}

const AnswerButton = (props: AnswerButtonUiProp) => {
    // Set class for locked answer, otherwise keep as normal
    let lockedClass = props.lockedAnswer === props.answer ? classes.Locked : '';
    let correctAnswerClass = '';
    let mainClass = classes.AnswerButton;
    if(props.showCorrectAnswer){
        if(props.answer?.correct){
            correctAnswerClass = classes.Correct;
            lockedClass = ''
            mainClass = '' //override main class
        }
        if(!props.answer?.correct && props.lockedAnswer === props.answer){
            correctAnswerClass = classes.Incorrect;
            lockedClass = ''
            mainClass = '' //override main class
        }
        // correctAnswerClass = props.answer?.correct ? classes.Correct : classes.Incorrect;

    }


    return (
        <Button className={[mainClass, props.className, lockedClass, correctAnswerClass].join(' ')} 
                variant="primary" 
                type="submit"
                onClick={props.onClick} 
                disabled={props.disabled}>
            {props.children}
        </Button>
    )
}

export default AnswerButton
