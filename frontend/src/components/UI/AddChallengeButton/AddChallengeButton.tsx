import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';

import { Container } from 'react-bootstrap'
import classes from './AddChallengeButton.module.css'

type AddChallengeButtonProps = {
    onClick: ((event: any) => void) | undefined
}

const AddChallengeButton = (props: AddChallengeButtonProps) => {
    return (
        <Container onClick={props.onClick} fluid className={classes.AddChallengeButtonContainer}>
            <FontAwesomeIcon icon={faPlusCircle} className={classes.Icon}/>
        </Container>
    )
}

export default AddChallengeButton
