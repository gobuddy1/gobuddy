import React from 'react'

import { Container } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconDefinition } from '@fortawesome/fontawesome-common-types'

import classes from './QuestionTypeButton.module.css'

interface QuestionTypeButtonProps{
    id: string,
    icon: IconDefinition,
    children: string,
    active: boolean,
    onClick?: ((event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void) | undefined
}

const QuestionTypeButton = (props: QuestionTypeButtonProps) => {

    const containerClass = props.active ? classes.ContainerActive : classes.ContainerInactive;
    const iconClass = props.active ? classes.IconActive : classes.IconInactive;
    const labelClass = props.active ? classes.LabelActive : classes.LabelInactive;

    return (

        <Container fluid className={[classes.Container, containerClass].join(' ')} onClick={props.onClick} id={props.id}>
            <FontAwesomeIcon className={[classes.Icon, iconClass].join(' ')} icon={props.icon} id={props.id}/>
            {/* <div className={[classes.Label, labelClass].join(' ')}  onClick={props.onClick} id={props.id}>{props.children}</div> */}
        </Container>
        

    )
}

export default QuestionTypeButton
