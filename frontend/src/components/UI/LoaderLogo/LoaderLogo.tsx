import React from 'react'
import { Container } from 'react-bootstrap';

import Logo from '../../Logo/Logo';
import classes from './LoaderLogo.module.css'

const LoaderLogo = () => {
    return (
        <Container fluid className={classes.LoaderLogoContainer}>
            <Logo>
            </Logo>
        </Container>
    )
}

export default LoaderLogo
