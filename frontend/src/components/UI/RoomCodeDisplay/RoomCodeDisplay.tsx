import React, { useContext } from 'react'
import { Container } from 'react-bootstrap';

import classes from './RoomCodeDisplay.module.css'
import Logo from '../../Logo/Logo';
import { MainContext } from '../../../context/MainContext/MainContext';

const RoomCodeDisplay = () => {
    const { room } = useContext(MainContext);
    return (
        <Container fluid className={classes.Container}>
            <Logo className={classes.Logo} />
            <p className={classes.Title}>ROOM CODE</p>
            <p className={classes.RoomCode}>{room}</p>
        </Container>
    )
}

export default RoomCodeDisplay
