import React from 'react'
import { Container } from 'react-bootstrap';

import classes from './CountDown.module.css'

type CountDownProps = {
    time: number
}

const CountDown = (props: CountDownProps) => {
    return (
        <Container fluid className={classes.Container}>
                <p className={classes.Time}>
                    {props.time}
                </p>
        </Container>
    )
}

export default CountDown
