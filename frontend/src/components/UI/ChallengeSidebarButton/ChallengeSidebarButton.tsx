import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Container } from 'react-bootstrap';
import { IconDefinition } from '@fortawesome/fontawesome-common-types'

import classes from './ChallengeSidebarButton.module.css'

interface ChallengeSidebarButtonProps{
    icon: IconDefinition,
    onClick?: ((event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void) | undefined,
    children: any,
    isAddLocation?: boolean,
    selected?: boolean
}

const ChallengeSidebarButton = (props: ChallengeSidebarButtonProps) => {
    const buttonColor = props.isAddLocation ? classes.AddColor : classes.DefaultColor;


    return (
        <Container className={[classes.ChallengeSidebarButton, buttonColor].join(' ')} onClick={props.onClick}>
            <div className={classes.Label}>{props.children}</div>
            <FontAwesomeIcon className={classes.Icon} icon={props.icon} />
        </Container>
    )
}

export default ChallengeSidebarButton
