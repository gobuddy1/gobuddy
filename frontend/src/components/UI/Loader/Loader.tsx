import React from 'react'
import { Container } from 'react-bootstrap';

import classes from './Loader.module.css'

const Loader = () => {
    return (
        <div className={classes.ldsRing}><div></div><div></div><div></div><div></div></div>
    )
}

export default Loader
