import React from 'react'
import { Container } from 'react-bootstrap'
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import classes from './ErrorMessage.module.css'

type ErrorMessageProps = {
    children: any,
    show: boolean
}

const ErrorMessage = (props: ErrorMessageProps) => {
    let view = null
    if(props.show){
        view = <Container fluid className={classes.ErrorMessageContainer}>
                    <FontAwesomeIcon className={classes.Icon} icon={faExclamationTriangle}></FontAwesomeIcon>
                    <p className={classes.Text}>{props.children}</p>
                </Container>
    }

    return (
        <Container fluid>
            {view}
        </Container>
    )
}

export default ErrorMessage
