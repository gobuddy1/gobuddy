import React, { useState } from 'react'
import { Container } from 'react-bootstrap';

import { ILocation } from '../../shared/interfaces/location.interface';
import classes from './LocationInformation.module.css'
import Modal from '../UI/Modal/Modal';
import { isNearGame, distToGame } from '../../util/mapfunctions/mapfunctions';
import ButtonUI from '../UI/ButtonUI/ButtonUI';

type LocationInformationProps = {
    selLoc: ILocation | undefined,
    userLoc: ILocation | undefined,
    show: boolean,
    onClose: ((event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void) | undefined
}

const LocationInformation = (props: LocationInformationProps) => {
    let content = !isNearGame(props.userLoc, props.selLoc) ? <p className={classes.IsNotNearGame}>You are not close enough to play this gobuddy</p> :
        <Container fluid>
            <h1>{props?.selLoc?.name}</h1>
            <img src="https://i.stack.imgur.com/y9DpT.jpg"></img>
            <p>{props?.selLoc?.description}</p>
            <ButtonUI className={classes.Button}>Start</ButtonUI>
        </Container>
    return (
        <Modal show={props.show} onClose={props.onClose}>
            <div className={classes.LocationInformationContainer}>
                {content}
            </div>
        </Modal>
    )
}

export default LocationInformation
