import React from 'react'
import { Container, Image } from 'react-bootstrap';

import classes from './UserDisplay.module.css'
import { IUser } from '../../shared/interfaces/user.interface';

const MAIN_URL = process.env.REACT_APP_HOST

type UserDisplayProps = {
    users: any[]
}

const UserDisplay = (props: UserDisplayProps) => {

    return (
        <Container fluid className={classes.UserDisplayContainer}>
            {props.users.map( (user, idx) => {
            return <Container fluid className={classes.UserTag} key={idx}>
                <Image src={MAIN_URL + user?.avatarImage} className={classes.Image}/>
                <p className={classes.UserName}>{user?.name}</p>
            </Container>})}
        </Container>
    )
}

export default UserDisplay
