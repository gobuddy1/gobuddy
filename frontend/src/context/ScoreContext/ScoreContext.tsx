import { counter } from '@fortawesome/fontawesome-svg-core'
import React, { useState, createContext, FC } from 'react'

// Create Types for MainContext state
type ScoreContextState = {
    score: number,
    putScore: (count: number) => void
}

// Create default values for main context
const contextDefaultValues: ScoreContextState = {
    score: 0,
    putScore: () => {}
}

// Create main context
export const ScoreContext = createContext<ScoreContextState>(contextDefaultValues)

// Create main provider
const ScoreProvider: FC = ({ children }) => {
    const [score, setScore] = useState<number>(contextDefaultValues.score)

    // Using putName and putRoom to manipulate setName and setRoom
    const putScore = (score: number) => setScore(score);

    return (
        <ScoreContext.Provider value={{ score, putScore }}>
            {children}
        </ScoreContext.Provider>
    )
}

export default ScoreProvider;