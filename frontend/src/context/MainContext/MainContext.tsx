import React, { useState, createContext, FC } from 'react'

// Create Types for MainContext state
type MainContextState = {
    name: string,
    room: string,
    token: string,
    avatarImage: string,
    putName: (name: string) => void,
    putRoom: (room: string) => void,
    putToken: (token: string) => void,
    putAvatarImage: (avatarImage: string) => void
}

// Create default values for main context
const contextDefaultValues: MainContextState = {
    name: '',
    room: '',
    token: '',
    avatarImage: '',
    putName: () => {},
    putRoom: () => {},
    putToken: () => {},
    putAvatarImage: () => {}
}

// Create main context
export const MainContext = createContext<MainContextState>(contextDefaultValues)

// Create main provider
const MainProvider: FC = ({ children }) => {
    const [name, setName] = useState<string>(contextDefaultValues.name)
    const [room, setRoom] = useState<string>(contextDefaultValues.room)
    const [token, setToken] = useState<string>(contextDefaultValues.token)
    const [avatarImage, setAvatarImage] = useState<string>(contextDefaultValues.avatarImage)

    // Using putName and putRoom to manipulate setName and setRoom
    const putName = (name: string) => setName(name);
    const putRoom = (room: string) => setRoom(room);
    const putToken = (token: string) => setToken(token);
    const putAvatarImage = (avatarImage: string) => setAvatarImage(avatarImage);

    return (
        <MainContext.Provider value={{ name: name, room: room, token: token, avatarImage: avatarImage, putName: putName, putRoom: putRoom, putToken: putToken, putAvatarImage: putAvatarImage }}>
            {children}
        </MainContext.Provider>
    )
}

// export { MainContext, MainProvider } 
export default MainProvider;