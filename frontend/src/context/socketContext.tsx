import React, { createContext } from 'react'
import { Socket } from 'socket.io-client';
import io from 'socket.io-client';
const SOCKET_URL = process.env.REACT_APP_HOST ? process.env.REACT_APP_HOST : 'http://localhost:8080/';

const default_data = { transports: ['websocket', 'polling'] }

const SocketContext = createContext<typeof Socket>(io(SOCKET_URL, default_data))

const SocketProvider = ({ children }: any) => {
    const socket = io(SOCKET_URL, default_data)
    return (
        <SocketContext.Provider value={socket}>
            {children}
        </SocketContext.Provider>
    )
}

export { SocketContext, SocketProvider }