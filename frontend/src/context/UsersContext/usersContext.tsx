import React, { useState, createContext, FC } from 'react'

// Create Types for MainContext state
type UsersContextState = {
    users: string[],
    putUsers: (users: string[]) => void
}

// Create default values for main context
const contextDefaultValues: UsersContextState = {
    users: [],
    putUsers: () => {},
}

export const UsersContext = createContext<UsersContextState>(contextDefaultValues)

const UsersProvider: FC = ({ children }) => {
    const [users, setUsers] = useState<string[]>(contextDefaultValues.users)

    const putUsers = (users: string[]) => setUsers(users);
    return (
        <UsersContext.Provider value={{ users, putUsers }}>
            {children}
        </UsersContext.Provider>
    )
}

export default UsersProvider