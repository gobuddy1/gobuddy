import { counter } from '@fortawesome/fontawesome-svg-core'
import React, { useState, createContext, FC } from 'react'

// Create Types for MainContext state
type TimeCountContextState = {
    counter: number,
    putCounter: (count: number) => void
}

// Create default values for main context
const contextDefaultValues: TimeCountContextState = {
    counter: 0,
    putCounter: () => {}
}

// Create main context
export const TimeCountContext = createContext<TimeCountContextState>(contextDefaultValues)

// Create main provider
const TimeCountProvider: FC = ({ children }) => {
    const [counter, setCounter] = useState<number>(contextDefaultValues.counter)

    // Using putName and putRoom to manipulate setName and setRoom
    const putCounter = (counter: number) => setCounter(counter);

    return (
        <TimeCountContext.Provider value={{ counter, putCounter }}>
            {children}
        </TimeCountContext.Provider>
    )
}

export default TimeCountProvider;