import React, { useState, createContext, FC } from 'react'
import { IAnswer, IQuestion, Question } from '../../shared/interfaces/question.interface';

// Create Types for QuestionContext state
type QuestionContextState = {
    question: IQuestion,
    putQuestion: (question: IQuestion) => void, //Keep track of current question
    lockedAnswer: IAnswer,
    putLockedAnswer: (lockedAnswer: IAnswer) => void, //Keep track on locked in answer for current question
    showCorrectAnswer: boolean,
    putShowCorrectAnswer: (showCorrectAnswer: boolean) => void, // Keep track on if the correct answer should be showed or not
    showDescription: boolean,
    putShowDescription: (showCorrectAnswer: boolean) => void
}

// Create default values for main context
const contextDefaultValues: QuestionContextState = {
    question: new Question(),
    putQuestion: () => {},
    lockedAnswer: {_id: undefined, answer: '', correct: false},
    putLockedAnswer: () => {},
    showCorrectAnswer: false,
    putShowCorrectAnswer: () => {},
    showDescription: false,
    putShowDescription: () => {}
}

// Create main context
export const QuestionContext = createContext<QuestionContextState>(contextDefaultValues)

// Create main provider
const QuestionProvider: FC = ({ children }) => {
    const [question, setQuestion] = useState<IQuestion>(contextDefaultValues.question)
    const [lockedAnswer, setLockedAnswer] = useState<IAnswer>(contextDefaultValues.lockedAnswer)
    const [showCorrectAnswer, setShowCorrectAnswer] = useState<boolean>(contextDefaultValues.showCorrectAnswer)
    const [showDescription, setShowDescription] = useState<boolean>(contextDefaultValues.showCorrectAnswer)

    const putQuestion = (question: IQuestion) => setQuestion(question);
    const putLockedAnswer = (lockedAnswer: IAnswer) => setLockedAnswer(lockedAnswer);
    const putShowCorrectAnswer = (showCorrect: boolean) => setShowCorrectAnswer(showCorrect);
    const putShowDescription = (showCorrect: boolean) => setShowDescription(showCorrect);

    return (
        <QuestionContext.Provider value={{ 
                question, putQuestion, 
                lockedAnswer, putLockedAnswer, 
                showCorrectAnswer, putShowCorrectAnswer,
                showDescription, putShowDescription
            }}>
            {children}
        </QuestionContext.Provider>
    )
}

// export { MainContext, MainProvider } 
export default QuestionProvider;