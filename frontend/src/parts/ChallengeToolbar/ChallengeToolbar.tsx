import React from 'react';
import { Container } from 'react-bootstrap';
import { faMapMarkedAlt, faShare } from '@fortawesome/free-solid-svg-icons';

import classes from './ChallengeToolbar.module.css';

import ChallengeToolbarButton from '../../components/ChallengeToolbarButton/ChallengeToolbarButton';



const ChallengeToolbar = (props: any) => {
    return (
        <Container fluid>
            <Container fluid className={classes.ChallengeToolbar}>
                <ChallengeToolbarButton icon={faMapMarkedAlt} onClick={props.addLocationControl} active={props.active}/>
                <ChallengeToolbarButton icon={faShare}/>
            </Container>
        </Container>
    )
}

export default ChallengeToolbar
