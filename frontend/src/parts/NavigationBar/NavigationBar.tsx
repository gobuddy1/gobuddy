import React, { useState, useRef, useEffect } from 'react'
import { useHistory } from 'react-router-dom';
import { Container, Navbar, Nav, Dropdown } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faTimes, faGamepad, faSignOutAlt, faCog, faPlus, faNewspaper } from '@fortawesome/free-solid-svg-icons'

import Logo from '../../components/Logo/Logo';
import classes from './NavigationBar.module.css'
import { logoutHandler } from '../../util/logouthandler';
import { GOBUDDY_LS_TOKEN, GOBUDDY_LS_USER } from '../../constants/localstoragenames';
import { getWhoami } from '../../shared/backend/backend-service';
import UserImage from '../../components/UserImage/UserImage';

import { IUser } from '../../shared/interfaces/user.interface'

const NavigationBar = (props: any) => {
    const history = useHistory();

    const node = useRef(document.createElement("div"));
    const closed = useRef(true);
    const [menuOpened, setOpenMenu] = useState(false);
    const [faIcon, setFaIcon] = useState(faBars);
    const [userCredentials, setUser] = useState<IUser>({
        firstname: '',
        lastname: '',
        imageUrl: ''
    });
    const [loggedIn, setLoggedIn] = useState<boolean>(false);

    const onToggleHandler = () => {
        closed.current = !closed.current;
        closed.current ? setFaIcon(faBars) : setFaIcon(faTimes);
    }

    const onMouseHoverHandler = () => {
        setOpenMenu(!menuOpened)
    }

    const closeMenu = (id: any) => {
        setOpenMenu(false);
    }

    const openMenu = () => {
        setOpenMenu(true)
    }

    const mouseDownHandler = (e: any) => {
        if(node.current !== null){
            if (node.current.contains(e.target)) {
                console.log(node.current);
                return;
            }
        }
        closeMenu(e.target.id);
    }

    // Only if you do not have the user in the local storage we ask for the user, otherwise we just use the localstorage ite
    useEffect(() => {
        const token = localStorage.getItem(GOBUDDY_LS_TOKEN);
        const user = localStorage.getItem(GOBUDDY_LS_USER)
        if(!user){
            getWhoami(token)
                .then(user => {
                    setUser({firstname: user.firstname, lastname: user.lastname, imageUrl: user.avatarimage})
                    setLoggedIn(user.firstname && user.lastname && user.avatarimage);
                    localStorage.setItem(GOBUDDY_LS_USER, JSON.stringify(user));
                })
                .catch(error => {
                    console.log(error);
                })
        }else{
            const userObj = JSON.parse(user)
            setUser({firstname: userObj.firstname, lastname: userObj.lastname, imageUrl: userObj.avatarimage})
            setLoggedIn(userObj.firstname && userObj.lastname && userObj.avatarimage);
            localStorage.setItem(GOBUDDY_LS_USER, JSON.stringify(userObj));
        }
    }, [])


    useEffect(() => {
        document.addEventListener("mousedown", mouseDownHandler);
    
        return () => {
          document.removeEventListener("mousedown", mouseDownHandler);
        };
    }, []);

    let NavigationBarContent = 
        <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto"></Nav>
            <Nav></Nav>
        </Navbar.Collapse>

        if(!loggedIn){
            NavigationBarContent = 
            <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="mr-auto">
                    <Dropdown>
                        <Dropdown.ItemText id="dropdownMenu" className={classes.DropDownItemText} onMouseEnter={onMouseHoverHandler} onMouseLeave={onMouseHoverHandler}>About GoBuddy</Dropdown.ItemText>
                        <Dropdown.Menu ref={node} show={menuOpened} className={classes.DropDownMenu} onMouseEnter={openMenu}>
                            <Dropdown.Item href="/#" className={classes.DropDownItem}><FontAwesomeIcon className={classes.DropDownIcon} icon={faPlus}/>About us</Dropdown.Item>
                            <Dropdown.Item href="/#" className={classes.DropDownItem}><FontAwesomeIcon className={classes.DropDownIcon} icon={faGamepad}/>Contact</Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                </Nav>
                <Nav>
                    <Nav.Link className={classes.NavBarLink} href="/login">Log In</Nav.Link>
                    <Nav.Link className={classes.NavBarLink} href="/signup">Sign Up</Nav.Link>
                </Nav>
            </Navbar.Collapse>
        }else{
            NavigationBarContent =
            <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="mr-auto">
                    {/*
                     <Nav.Link href="/dashboard" className={classes.LinkMenu}>Dashboard</Nav.Link>
                    <Nav.Link href="/challenges" className={classes.LinkMenu}>My Gobuddys</Nav.Link>
                    <Nav.Link href="/join-game" className={classes.LinkMenu}>Join Gobuddy</Nav.Link> */}
                    <Nav.Link href="/digital-gobuddy-main" className={classes.LinkMenu}>Digital Gobuddy</Nav.Link>
                    {/* <Dropdown>
                        <Dropdown.ItemText id="dropdownMenu" className={classes.DropDownItemText} onMouseEnter={onMouseHoverHandler} onMouseLeave={onMouseHoverHandler}>My Challenges</Dropdown.ItemText>
                        <Dropdown.Menu ref={node} show={menuOpened} className={classes.DropDownMenu} onMouseEnter={openMenu}>
                            <Dropdown.Item href="/dashboard" className={classes.DropDownItem}><FontAwesomeIcon className={classes.DropDownIcon} icon={faNewspaper}/>Dashboard</Dropdown.Item>
                            <Dropdown.Item href="/create-challenge" className={classes.DropDownItem}><FontAwesomeIcon className={classes.DropDownIcon} icon={faPlus}/>Add Challenge</Dropdown.Item>
                            <Dropdown.Item href="/challenges" className={classes.DropDownItem}><FontAwesomeIcon className={classes.DropDownIcon} icon={faGamepad}/>My Challenges</Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown> */}
                </Nav>
                <Nav>
                    <Dropdown>
                        <Dropdown.Toggle id="dropdownUserMenu" className={classes.DropDownToggle}><UserImage src={userCredentials.imageUrl}/></Dropdown.Toggle>
                        <Dropdown.Menu className={['dropdown-menu-right', classes.DropDownMenu].join(' ')}>
                            <Dropdown.ItemText className={classes.TextItem}>{userCredentials.firstname} {userCredentials.lastname}</Dropdown.ItemText>
                            <Dropdown.Divider className={classes.DropDownDivider}/>
                            <Dropdown.Item href='/user-settings' className={classes.DropDownText}><FontAwesomeIcon className={classes.DropDownIcon} icon={faCog}/>Settings</Dropdown.Item>
                            <Dropdown.Item onClick={() => logoutHandler(history)} className={classes.DropDownText}><FontAwesomeIcon className={classes.DropDownIcon} icon={faSignOutAlt}/>Logout</Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                </Nav>
            </Navbar.Collapse>
        }


    return (
        <Container fluid className={[classes.NavBarContainer, props.className].join(' ')}>
            <Navbar collapseOnSelect expand="lg" className={classes.NavigationBar}>
                <Navbar.Brand href={loggedIn ? "/my-quizzes" : "/"} className={classes.NavBarBrand}><Logo className={classes.Logo}/></Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" className={classes.NavbarToggle} onClick={onToggleHandler}>
                    <FontAwesomeIcon className={classes.Bars} icon={faIcon}/>
                </Navbar.Toggle>
                {NavigationBarContent}
            </Navbar>
        </Container>

    )
}

export default NavigationBar
