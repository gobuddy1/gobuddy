import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

import './App.css';

import MapPage from './pages/MapPage/MapPage';
import MainPage from './pages/MainPage/MainPage';
import LoginPage from './pages/LoginPage/LoginPage';
import SignupPage from './pages/SignupPage/SignupPage';
import NotFound from './pages/NotFoundPage/NotFound';
import AuthRoute from './components/Auth/AuthRoute/AuthRoute';
import ChallengesPage from './pages/ChallengesPage/ChallengesPage'
import CreateChallengePage from './pages/CreateChallengePage/CreateChallengePage';
import SettingsPage from './pages/SettingsPage/SettingsPage';
import SingleChallengePage from './pages/SingleChallengePage/SingleChallengePage';
import InitPage from './pages/InitPage/InitPage';
import SettingsPageProfile from './pages/SettingsPage/SettingsPageProfile/SettingsPageProfile';
import SettingsPagePhoto from './pages/SettingsPage/SettingsPagePhoto/SettingsPagePhoto';
import SettingsPageAccount from './pages/SettingsPage/SettingsPageAccount/SettingsPageAccount';
import SettingsPagePayment from './pages/SettingsPage/SettingsPagePayment/SettingsPagePayment';
import SingleGamePage from './pages/SingleGamePage/SingleGamePage';
import GameChallengePage from './pages/GameChallengePage/GameChallengePage';
import MainDigitalGobuddyPage from './pages/MainDigitalGobuddyPage/MainDigitalGobuddyPage';
import QuizPage from './pages/QuizPage/QuizPage';
import MainProvider from './context/MainContext';
import UsersProvider from './context/UsersContext';
import { SocketProvider } from './context/socketContext';
import QuestionProvider from './context/QuestionContext';
import TimeCountProvider from './context/TimeCountContext';
import AddQuizQuestionPage from './pages/AddQuizQuestionPage/AddQuizQuestionPage';
import MyQuizzesPage from './pages/MyQuizzesPage/MyQuizzesPage';
import EditQuestion from './components/EditQuestion/EditQuestion';

function App() {
  return (
    <div className="App">
      <MainProvider>
        <UsersProvider>
          <QuestionProvider>
            <TimeCountProvider>
              <SocketProvider>
                <BrowserRouter>
                  <Switch>
                    <Route path="/login" component={LoginPage} />
                    <Route path="/signup" component={SignupPage} />
                    <AuthRoute path="/map" component={MapPage} />
                    <AuthRoute path="/challenges" exact component={ChallengesPage} />
                    <AuthRoute path="/challenges/:gameId" exact component={SingleChallengePage} />
                    <AuthRoute path="/create-challenge" component={CreateChallengePage} />
                    <AuthRoute path="/user-settings" exact component={SettingsPage} />
                    <AuthRoute path="/user-settings/profile" exact component={SettingsPageProfile} />
                    <AuthRoute path="/user-settings/photo" exact component={SettingsPagePhoto} />
                    <AuthRoute path="/user-settings/account" exact component={SettingsPageAccount} />
                    <AuthRoute path="/user-settings/payment" exact component={SettingsPagePayment} />
                    <AuthRoute path="/dashboard" component={MainPage} />
                    <AuthRoute path="/game/:gameId" exact component={SingleGamePage} />
                    <AuthRoute path="/join-game" exact component={GameChallengePage} />
                    <AuthRoute path="/digital-gobuddy-main" exact component={MainDigitalGobuddyPage} />
                    <AuthRoute path="/quiz/:quizCode" exact component={QuizPage} />
                    <AuthRoute path="/quiz/add-questions/:quizCode" exact component={AddQuizQuestionPage} />
                    <AuthRoute path="/my-quizzes" exact component={MyQuizzesPage} />
                    {/* <AuthRoute path="/quiz/add-questions/edit-question/:questionId" exact component={EditQuestion} /> */}
                    <AuthRoute path="/quiz/add-questions/edit-question/:quizId/:questionId" exact component={EditQuestion} />
                    <Route exact path="/" component={InitPage} />
                    <Route component={NotFound}></Route>
                  </Switch>
                </BrowserRouter>
              </SocketProvider>
            </TimeCountProvider>
          </QuestionProvider>
        </UsersProvider>
      </MainProvider>
    </div>

  );
}

export default App;
