export const GOBUDDY_LS_TOKEN: string = 'gobuddy.ls.token'
export const GOBUDDY_LS_TOKEN_EXPIRY_DATE: string = 'gobuddy.ls.token.expirydate'
export const GOBUDDY_LS_UUID: string = 'gobuddy.ls.uuid'
export const GOBUDDY_LS_USER: string = 'gobuddy.ls.user'