const express = require('express');
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const multer = require('multer')
const path = require('path')
require('dotenv').config({ path: path.join(__dirname, '.env') });

// var fs = require('fs')
// var https = require('https');


const authRoutes = require('./routes/auth')
const challengeRoutes = require('./routes/challenge')
const quizRoutes = require('./routes/quiz')
const isAuth = require('./middleware/is-auth')
const {addUser, getUser, deleteUser, getUsers, addScore} = require('./util/users')

const app = express();
// const key = fs.readFileSync('../key.pem');
// const cert = fs.readFileSync('../cert.pem');
// const server = https.createServer({key: key, cert: cert }, app);

const PORT = 8080;
const NEW_CHAT_MESSAGE_EVENT = "newChatMessage";
const COUNT_USER_EVENT = "countUser";
const users = {};

const corsOptions = require('./middleware/cors-options');
const cors = require('cors');

const fileStorage = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, path.join(__dirname, 'images'));
    },
    filename: (req, file, callback) => {
        callback(null, new Date().toISOString() + '-' + file.originalname);
    },
});

const fileFilter = (req, file, callback) => {
    if( 
        file.mimetype === 'image/png' ||
        file.mimetype === 'image/jpg' ||
        file.mimetype === 'image/jpeg'
    ){
        callback(null, true);
    }else{
        callback(null, false);
    }
}

app.use(bodyParser.json());

app.use(
    multer({
        storage: fileStorage, fileFilter: fileFilter
    }).single('image')
);

//Serve images directory statically
app.use('/images', express.static(path.join(__dirname, 'images')));

app.use((req, res, next) => {
    const allowedOrigins = [    'http://localhost:3000', 'https://localhost:3000',
                                'http://localhost', 'https://localhost',
                                'http://164.90.200.151', 'https://164.90.200.151',
                                'https://www.themarketplays.net', 'http://www.themarketplays.net',
                                'https://themarketplays.net', 'http://themarketplays.net'];
    // res.setHeader('Access-Control-Allow-Origin', '*');
    const origin = req.headers.origin;
    if (allowedOrigins.includes(origin)) {
        res.setHeader('Access-Control-Allow-Origin', origin);
    }
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
});

app.use('/auth', authRoutes);
app.use('/challenge', challengeRoutes);
app.use('/quiz', quizRoutes);

// app.use('/auth', cors(corsOptions),  authRoutes);
// app.use('/challenge', cors(corsOptions), challengeRoutes);
// app.use('/quiz', cors(corsOptions), quizRoutes);

app.use((error, req, res, next) => {
    console.log(error)
    const status = error.statusCode || 500;
    const message = error.message;
    const data = error.data;
    res.status(status).json({ message: message, data: data });
});

mongoose.connect(`mongodb+srv://${process.env.MONGODB_USER}:${process.env.MONGODB_PASS}@${process.env.MONGODB_CLUSTER}.fff5p.mongodb.net/${process.env.MONGODB_DB}?retryWrites=true&w=majority`, { useUnifiedTopology: true, useNewUrlParser: true })
    .then(result => {
        // const server = https.createServer({key: key, cert: cert }, app).listen(PORT);
        const server = app.listen(PORT);
        const io = require('./socket').init(server);

        io.on("connection", (socket) => {
                socket.on("login", ({name, room, token, avatarImage}, callback) => {
                    const { user, error } = addUser(socket.id, name, room, token, avatarImage)
                    if (error) return callback(error)
                    socket.join(user.room)
                    socket.in(room).emit('notification', { title: 'Someone\'s here', description: `${user.name} just entered the room` })
                    io.in(room).emit('users', getUsers(room))
                    callback()
                })
                socket.on("sendMessage", message => {
                    const user = getUser(socket.id)
                    io.in(user.room).emit('message', { user: user.name, text: message });
                })
                socket.on("changeQuestion", question => {
                    const user = getUser(socket.id);
                    console.log(user)
                    io.in(user.room).emit('question', question);
                })
                socket.on("setShowDescription", showDescription => {
                    const user = getUser(socket.id);
                    io.in(user.room).emit('showDescription', showDescription);
                })
                socket.on("setScore", score => {
                    console.log(score)
                    const user = addScore(socket.id, score)
                    io.in(user.room).emit('score', score);
                    io.in(user.room).emit('users', getUsers(user.room))
                })
                socket.on("disconnect", () => {
                    console.log("User disconnected");
                    const user = deleteUser(socket.id)
                    if (user) {
                        io.in(user.room).emit('notification', { title: 'Someone just left', description: `${user.name} just left the room` })
                        io.in(user.room).emit('users', getUsers(user.room))
                    }
                })
        });
    })
    .catch(error => {
        console.log(error)
    })