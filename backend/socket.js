let io;
const origin = process.env.HOST 

module.exports = {
    init: httpServer => {
        io = require('socket.io')(httpServer, {
            cors: {
                origin: origin,
                methods: ["GET", "POST"]
            }
        })
        return io;
    }
};