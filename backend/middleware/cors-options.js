const  whitelist = [ 'http://localhost:3000', 'https://localhost:3000', 'http://localhost', 'https://localhost', 
                    'https://www.themarketplays.net', 'http://www.themarketplays.net', 'https://themarketplays.net', 
                    'http://themarketplays.net'];

module.exports = {
  origin: (origin, callback) => {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  },
  methods: ['GET','POST','DELETE','UPDATE','PUT','PATCH']
}
