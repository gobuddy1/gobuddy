const { validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
ObjectId = require('mongodb').ObjectId;
const path = require('path')

const User = require('../models/user');
const Challenge = require('../models/challenge');
const Location = require('../models/location');
const Question = require('../models/question')
const pathCreator = require('../util/path-creator')
const codeGenerator = require('../util/code-generator')


exports.createchallenge = (req, res, next) => {
    // const userId = '5fd64ac5091b32f37451d523' //should retreive it from request by authenticating - using dummyvalue instead.

    const userId = req.userId; //Get user id from request (created in is-auth)
    //Create a new challenge
    const name = req.body.name;
    const description = req.body.description;
    const public = req.body.public;

    //Getting image
    if(!req.file){
        const error = new Error('No image provided');
        error.statusCode = 422;
        throw error;
    }
    const imageUrl = pathCreator(req.file.path)
    // Challenge.findOne({
    //     code: codeGenerator()
    // })
    
    const challenge = new Challenge({
        name: name,
        description: description,
        createdby: new ObjectId(userId),
        public: public,
        imageUrl: imageUrl,
        code: codeGenerator() //Fix immediately
    });
    

    User.findOne({
        _id: new ObjectId(userId)
    })
    .then(user => {
        if(!user){
            const error = new Error('A user could not be found.');
            error.statusCode = 401;
            throw error;
        }
        user.challenges.push(challenge)
        return user.save();
    })
    .then(() => {
        return challenge.save();
    })
    .then(result => {
        res.status(201).json({message: 'Challenge created and referenced to user!', challengeId: result._id})
    })
    .catch(error => {
        if (!error.statusCode){
            error.statusCode = 500;
        }
        next(error);
    });
}

exports.createlocation = (req, res, next) => {
    const name = req.body.name;
    const description = req.body.description;
    const longitude = req.body.longitude;
    const latitude = req.body.latitude;
    const range = req.body.range;
    const challengeId = req.body.challengeId; //String value
    const userId = req.userId;

    const location = new Location({
        name: name,
        description: description,
        longitude: longitude,
        latitude: latitude,
        range: range,
        createdby: new ObjectId(userId)
    });
    console.log(location)
    console.log(challengeId)
    Challenge.findOne({
        _id: new ObjectId(challengeId)
    })
    .then(challenge => {
        console.log(challenge)
        if(!challenge){
            const error = new Error('A challenge could not be found!');
            error.statusCode = 401;
            throw error;
        }
        challenge.locations.push(location);
        return challenge.save();
    })
    .then(() => {
        return location.save();
    })
    .then(result => {
        res.status(201).json({message: 'Location created and referenced to challenge!', locationId: result._id})
    })
    .catch(error => {
        if (!error.statusCode){
            error.statusCode = 500;
        }
        next(error);
    });
}

//måste ta bort referens i challenge också!
exports.deleteLocation = (req, res, next) => {
    const locationId = req.body.locationId;
    const userId = req.userId;

    Location.findOne({
        _id: new ObjectId(locationId)
    })
    .then(location => {
        if(!location){
            const error = new Error('A location could not be found!');
            error.statusCode = 401;
            throw error;
        }
        if(location.createdby.toString() !== userId.toString()){
            const error = new Error('Unauthorized deletion');
            error.statusCode = 401;
            throw error;
        }
        return Question.deleteMany({ _id: { $in: location.questions}}) //Delete all questions referencing from this location
    })
    .then(() => {
        return Location.deleteOne({_id: new ObjectId(locationId)})
    })
    .then(()=> {
        res.status(200).json({message: 'Location has been deleted successfully.'})
    })
    .catch(error => {
        if (!error.statusCode){
            error.statusCode = 500;
        }
        next(error);
    })
}

exports.createQuestion = (req, res, next) => {
    const question = req.body.question;
    const answers = req.body.answers;
    const difficulty = req.body.category;
    const category = req.body.category;
    const type = req.body.type;
    const image = req.body.image;
    const timelimit = req.body.timelimit;
    const pricecategory = req.body.pricecategory;
    const locationId = req.body.locationId; //String value
    const userId = req.userId;

    const questionObject = new Question({
        question: question,
        answers: answers,
        difficulty: difficulty,
        category: category,
        type: type,
        image: image,
        timelimit: timelimit,
        pricecategory: pricecategory,
        createdby: new ObjectId(userId)
    });

    Location.findOne({
        _id: new ObjectId(locationId)
    })
    .then(location => {
        if(!location){
            const error = new Error('A location could not be found!');
            error.statusCode = 401;
            throw error;
        }
        location.questions.push(questionObject);
        return location.save();
    })
    .then(() => {
        return questionObject.save();
    })
    .then(result => {
        res.status(201).json({message: 'Question created and referenced to location!', questionId: result._id})
    })
    .catch(error => {
        if (!error.statusCode){
            error.statusCode = 500;
        }
        next(error);
    });

}

// Get all challenges for specific user
exports.getchallenges = (req, res, next) => {
    const userId = req.userId;

    User.findOne({
        _id: new ObjectId(userId)
    })
    .populate({
        path: 'challenges',
        populate: {
            path: 'locations',
            model: Location
        }
    })
    .then(user => {
        if(!user){
            const error = new Error('A user could not be found!');
            error.statusCode = 401;
            throw error;
        }
        res.status(201).json({challenges: user.challenges})
    })
    .catch(error => {
        if (!error.statusCode){
            error.statusCode = 500;
        }
        next(error);
    });
}

// Get all public challenges (to be uodated with pagination)
exports.getpublicchallenges = (req, res, next) => {
    Challenge.find({public: true})
    .then(challenges => {
        if(!challenges){
            const error = new Error('Could not find public challenges!');
            error.statusCode = 401;
            throw error;
        }
        res.status(201).json({challenges: challenges})
    })
    .catch(error => {
        if (!error.statusCode){
            error.statusCode = 500;
        }
        next(error);
    });
}

// Get all locations for specific challenge
exports.getlocations = (req, res, next) => {
    const challengeId = req.params.challengeId;
    const userId = req.userId;

    Challenge.findOne({
        _id: new ObjectId(challengeId)
    })
    .populate('locations')
    .then(challenge => {
        if(!challenge){
            const error = new Error('A challenge could not be found!');
            error.statusCode = 401;
            throw error;
        }
        if(challenge.createdby.toString() !== userId.toString()){
            const error = new Error('Unauthorized access to challenge');
            error.statusCode = 401;
            throw error;
        }
        res.status(201).json({locations: challenge.locations})
    })
    .catch(error => {
        if (!error.statusCode){
            error.statusCode = 500;
        }
        next(error);
    });
}

// Get all questions for specific location
exports.getquestions = (req, res, next) => {
    const locationId = req.params.locationId;

    Location.findOne({
        _id: new ObjectId(locationId)
    })
    .populate('questions')
    .then(location => {
        if(!location){
            const error = new Error('A location could not be found!');
            error.statusCode = 401;
            throw error;
        }
        res.status(201).json({questions: location.questions})
    })
    .catch(error => {
        if (!error.statusCode){
            error.statusCode = 500;
        }
        next(error);
    });
}

exports.deletequestion = (req, res, next) => {
    const questionId = req.body.questionId;
    const locationId = req.body.locationId;

    Location.findOne({
        _id: new ObjectId(locationId)
    })
    .then(location => {
        if(!location){
            const error = new Error('A location could not be found!');
            error.statusCode = 401;
            throw error;
        }
        const newQuestions = location.questions.filter(id => id.toString() !== questionId);
        return location.updateOne({questions: newQuestions});
    })
    .then(() => {
        return Question.deleteOne({_id: new ObjectId(questionId)})
    })
    .then(()=> {
        res.status(200).json({message: 'Question has been deleted successfully.'})
    })
    .catch(error => {
        if (!error.statusCode){
            error.statusCode = 500;
        }
        next(error);
    })
}

exports.deletequestion = (req, res, next) => {
    const questionId = req.body.questionId;
    const locationId = req.body.locationId;

    Location.findOne({
        _id: new ObjectId(locationId)
    })
    .then(location => {
        if(!location){
            const error = new Error('A location could not be found!');
            error.statusCode = 401;
            throw error;
        }
        const newQuestions = location.questions.filter(id => id.toString() !== questionId);
        return location.updateOne({questions: newQuestions});
    })
    .then(() => {
        return Question.deleteOne({_id: new ObjectId(questionId)})
    })
    .then(()=> {
        res.status(200).json({message: 'Question has been deleted successfully.'})
    })
    .catch(error => {
        if (!error.statusCode){
            error.statusCode = 500;
        }
        next(error);
    })
}

exports.getChallenge = (req, res, next) => {
    const challengeCode = req.params.challengeCode;
    const userId = req.userId;
    console.log(challengeCode, userId)

    Challenge.findOne({
        code: challengeCode
    })
    .then(challenge => {
        // Here we should also look for auth users in the "authUsers list"
        if(challenge.createdby.toString() !== userId.toString()){
            const error = new Error('Unauthorized access to challenge');
            error.statusCode = 401;
            throw error;
        }
        res.status(200).json(challenge)
    })
    .catch(error => {
        if (!error.statusCode){
            error.statusCode = 500;
        }
        next(error);
    })
}