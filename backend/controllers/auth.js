const { validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
ObjectId = require('mongodb').ObjectId;
const path = require('path')
const pathCreator = require('../util/path-creator')

const User = require('../models/user')

exports.signup = (req, res, next) => {
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        const error = new Error('Validation failed.');
        error.statusCode = 422;
        error.data = errors.array();
        throw error;
    }
    if(!req.file){
        const error = new Error('No image provided');
        error.statusCode = 422;
        throw error;
    }
    const email = req.body.email;
    const firstname = req.body.firstname;
    const lastname = req.body.lastname;
    const telnumber = req.body.telnumber;
    const password = req.body.password;
    // const imageUrl = req.file.path;
    const imageUrl = pathCreator(req.file.path)

    bcrypt.hash(password, 12)
        .then(hashedPassword => {
            const user = new User({
                email: email,
                firstname: firstname,
                lastname: lastname,
                telnumber: telnumber,
                password: hashedPassword,
                avatarimage: imageUrl
            });
            return user.save();
        })
        .then(result => {
            res.status(201).json({message: 'User created!', userId: result._id})
        })
        .catch(error => {
            if (!error.statusCode){
                error.statusCode = 500;
            }
            next(error);
        });
}


exports.login = (req, res, next) => {
    const email = req.body.email;
    const password = req.body.password;
    let loadedUser;
    User.findOne({
        email: email
    })
    .then(user => {
        if(!user){
            const error = new Error('A user with this email could not be found.');
            error.statusCode = 401;
            throw error;
        }
        loadedUser = user;
        return bcrypt.compare(password, user.password);
    })
    .then(isEqual => {
        if(!isEqual){
            const error = new Error('Password is incorrect.');
            error.statusCode = 401;
            throw error;
        }
        const token = jwt.sign({
            email: loadedUser.email, 
            userId: loadedUser._id.toString()
        }, 'secret', {expiresIn: '1h'});
        res.status(200).json({
            token: token,
            userId: loadedUser._id.toString()
        })
        console.log('User logged in!')

    })
    .catch(error =>{
        if(!error.statusCode){
            error.statusCode = 500;
        }
        next(error);
    })
}

exports.updateUser = (req, res, next) => {
    const reqUserId = req.userId; //User making the request
    const userId = req.body._id
    const email = req.body.email;
    const firstname = req.body.firstname;
    const lastname = req.body.lastname;
    const telnumber = req.body.telnumber;
    const password = req.body.password;
    //Need to update with possibility to update image //robin 
    // const imageUrl = req.file.imageUrl; 

    if(userId !== reqUserId){
        const error = new Error('You are not allowed to change another user');
        error.statusCode = 401; //Not authorized
        throw error;
    }
    bcrypt.hash(password, 12)
    .then(hashedPassword => {
        return User.findById(userId)
        .then(userData => {
            if(!userData){
                const error = new Error('A user with this id could not be found.');
                error.statusCode = 404;
                throw error;
            }
            userData.email = email;
            userData.firstname = firstname;
            userData.lastname = lastname;
            userData.telnumber = telnumber;
            userData.password = hashedPassword;
            return userData.save();
            })
        })
        .then(result => {
            console.log('Updated user!')
            res.status(200).json({
                message: 'Updated user successfully',
                userId: result._id
            })
        })
        .catch(error => {
            if(!error.statusCode){
                error.statusCode = 500;
            }
            next(error);
        })
}

exports.whoami = (req, res, next ) => {
    const userId = req.userId;
    User.findOne({_id: new ObjectId(userId)})
        .then(user => {
            if(!user){
                const error = new Error('A user with this email could not be found.');
                error.statusCode = 404;
                throw error;
            }
            res.status(200).json({
                firstname: user.firstname,
                lastname: user.lastname,
                email: user.email,
                avatarimage: user.avatarimage
            })
        })
        .catch(error => {
            if(!error.statusCode){
                error.statusCode = 500;
            }
            next(error);
        })
}

exports.getUserName = (req, res, next) => {
    const userId = req.params.userId;
    User.findOne({_id: new ObjectId(userId)})
    .then(user => {
        if(!user){
            const error = new Error('A user with this email could not be found.');
            error.statusCode = 404;
            throw error;
        }
        res.status(200).json({
            firstname: user.firstname,
            lastname: user.lastname
        })
    })
    .catch(error => {
        if(!error.statusCode){
            error.statusCode = 500;
        }
        next(error);
    })
}