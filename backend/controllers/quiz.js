const { validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
ObjectId = require('mongodb').ObjectId;
const path = require('path')

const User = require('../models/user');
const Quiz = require('../models/quiz');
const Question = require('../models/question');
const pathCreator = require('../util/path-creator');
const codeGenerator = require('../util/code-generator');

exports.createQuiz = (req, res, next) => {
    // const userId = '5fd64ac5091b32f37451d523' //should retreive it from request by authenticating - using dummyvalue instead.

    const userId = req.userId; //Get user id from request (created in is-auth)
    //Create a new challenge
    const name = req.body.name;
    const description = req.body.description;

    //Getting image
    // if(!req.file){
    //     const error = new Error('No image provided');
    //     error.statusCode = 422;
    //     throw error;
    // }
    // const imageUrl = pathCreator(req.file.path)
    // Challenge.findOne({
    //     code: codeGenerator()
    // })
    
    const quiz = new Quiz({
        name: name,
        description: description,
        createdby: new ObjectId(userId),
        // imageUrl: imageUrl,
        code: codeGenerator() //Fix immediately
    });
    

    User.findOne({
        _id: new ObjectId(userId)
    })
    .then(user => {
        if(!user){
            const error = new Error('A user could not be found.');
            error.statusCode = 401;
            throw error;
        }
        user.quizzes.push(quiz)
        return user.save();
    })
    .then(() => {
        return quiz.save();
    })
    .then(result => {
        res.status(201).json({message: 'Challenge created and referenced to user!', quizId: result._id, code: result.code})
    })
    .catch(error => {
        if (!error.statusCode){
            error.statusCode = 500;
        }
        next(error);
    });
}

exports.getQuiz = (req, res, next) => {
    const quizCode = req.params.quizCode;
    const requestType = req.params.requestType;
    const userId = req.userId;

    Quiz.findOne({
        code: quizCode
    })
    .populate({
        path: 'questions'
        // populate: {
        //     path: 'locations',
        //     model: Location
        // }
    })
    .then(quiz => {
        if(!quiz){
            const error = new Error('Could not find quiz by given ID');
            error.statusCode = 404;
            throw error;
        }
        // Auth-flow for verifying authenticated access to the quiz
        const createdByUser = quiz.createdby.toString() === userId.toString();
        console.log(createdByUser)
        let userAuthenticated = false;
        if(requestType === 'play-quiz'){
            let userInAuthList = false;
            if(quiz.authUsers){
                userInAuthList = quiz.authUsers.map(user => user.toString()).indexOf(userId.toString()) !== -1;
            }
            // const userInAuthList = quiz.authUsers.map(user => user.toString()).indexOf(userId.toString()) !== -1;
            userAuthenticated = !userInAuthList || !createdByUser;
        }else if(requestType === 'edit-quiz'){
            userAuthenticated = createdByUser;
        }else{
            const error = new Error('Unauthorized access to challenge');
            error.statusCode = 401;
            throw error;
        }
        // Here we should also look for auth users in the "authUsers list"
        if(!userAuthenticated){
            const error = new Error('Unauthorized access to challenge');
            error.statusCode = 401;
            throw error;
        }
        res.status(200).json(quiz)
    })
    .catch(error => {
        if (!error.statusCode){
            error.statusCode = 500;
        }
        next(error);
    })
}

exports.getQuizById = (req, res, next) => {
    const quizId = req.params.quizId;
    const requestType = req.params.requestType;
    const userId = req.userId;

    Quiz.findOne({
        _id: new ObjectId(quizId)
    })
    .populate({
        path: 'questions'
    })
    .then(quiz => {
        if(!quiz){
            const error = new Error('Could not find quiz by given ID');
            error.statusCode = 404;
            throw error;
        }
        // Auth-flow for verifying authenticated access to the quiz
        const createdByUser = quiz.createdby.toString() === userId.toString();
        console.log(createdByUser)
        let userAuthenticated = false;
        if(requestType === 'play-quiz'){
            let userInAuthList = false;
            if(quiz.authUsers){
                userInAuthList = quiz.authUsers.map(user => user.toString()).indexOf(userId.toString()) !== -1;
            }
            // const userInAuthList = quiz.authUsers.map(user => user.toString()).indexOf(userId.toString()) !== -1;
            userAuthenticated = !userInAuthList || !createdByUser;
        }else if(requestType === 'edit-quiz'){
            userAuthenticated = createdByUser;
        }else{
            const error = new Error('Unauthorized access to challenge');
            error.statusCode = 401;
            throw error;
        }
        // Here we should also look for auth users in the "authUsers list"
        if(!userAuthenticated){
            const error = new Error('Unauthorized access to challenge');
            error.statusCode = 401;
            throw error;
        }
        res.status(200).json(quiz)
    })
    .catch(error => {
        if (!error.statusCode){
            error.statusCode = 500;
        }
        next(error);
    })
}

exports.getQuizzes = (req, res, next) => {
    const userId = req.userId;

    User.findOne({
        _id: new ObjectId(userId)
    })
    .populate({
        path: 'quizzes',
        populate: {
            path: 'quizzes',
            model: Quiz
        }
    })
    .then(user => {
        if(!user){
            const error = new Error('A user could not be found!');
            error.statusCode = 401;
            throw error;
        }
        res.status(201).json({quizzes: user.quizzes})
    })
    .catch(error => {
        if (!error.statusCode){
            error.statusCode = 500;
        }
        next(error);
    });
}


// GET QUESTION
exports.getQuestion = (req, res, next) => {
    const questionId = req.params.questionId;
    console.log(questionId)
    const userId = req.userId;

    Question.findOne({
        _id: new ObjectId(questionId)
    })
    .then(question => {
        if(!question){
            const error = new Error('Could not find quiz by given ID');
            error.statusCode = 404;
            throw error;
        }
        // Auth-flow for verifying authenticated access to the quiz
        const createdByUser = question.createdby.toString() === userId.toString();

        if(!createdByUser){
            const error = new Error('Unauthorized access for editing question');
            error.statusCode = 401;
            throw error;
        }
        res.status(200).json(question)
    })
    .catch(error => {
        if (!error.statusCode){
            error.statusCode = 500;
        }
        next(error);
    })
}

// DELETE QUESTION
exports.deleteQuestion = (req, res, next) => {
    const questionId = req.body.questionId;
    const quizId = req.body.quizId;
    const userId = req.userId;

    // First delete questions from quiz and then delete question doc itself
    Quiz.findOne({
        _id: new ObjectId(quizId)
    })
    .then(quiz => {
        const createdByUser = quiz?.createdby.toString() === userId.toString();
        if(!createdByUser){
            const error = new Error('Unauthorized access for editing question');
            error.statusCode = 401;
            throw error;
        }

        if(!quiz){
            const error = new Error('A quiz could not be found!');
            error.statusCode = 401;
            throw error;
        }

        const newQuestions = quiz.questions.filter(id => id.toString() !== questionId);
        return quiz.updateOne({questions: newQuestions});
    })
    .then(() => {
        return Question.deleteOne({_id: new ObjectId(questionId)})
    })
    .then(()=> {
        res.status(200).json({message: 'Question has been deleted successfully.'})
    })
    .catch(error => {
        if (!error.statusCode){
            error.statusCode = 500;
        }
        next(error);
    })
}

// POST QUESTION
exports.postQuestion = (req, res, next) => {
    if(!req.file){
        const error = new Error('No image provided');
        error.statusCode = 422;
        throw error;
    }
    const imageUrl = pathCreator(req.file.path)
    const question = req.body.question;
    const description = req.body.description;
    let answers;
    if(typeof req.body.answers === 'string'){
        answers = JSON.parse(req.body.answers);
    }else{
        answers = req.body.answers
    }
    const difficulty = req.body.category;
    const category = req.body.category;
    const type = req.body.type;
    const image = imageUrl;
    const timelimit = req.body.timelimit;
    const pricecategory = req.body.pricecategory;
    const quizId = req.body.quizId; //String value
    const userId = req.userId;

    const questionObject = new Question({
        question: question,
        description: description,
        answers: answers,
        difficulty: difficulty,
        category: category,
        type: type,
        image: image,
        timelimit: timelimit,
        pricecategory: pricecategory,
        createdby: new ObjectId(userId)
    });

    Quiz.findOne({
        _id: new ObjectId(quizId)
    })
    .then(quiz => {
        if(!quiz){
            const error = new Error('A quiz could not be found!');
            error.statusCode = 404;
            throw error;
        }
        quiz.questions.push(questionObject);
        return quiz.save();
    })
    .then(() => {
        return questionObject.save();
    })
    .then(result => {
        res.status(201).json({message: 'Question created and referenced to quiz!', questionId: result._id})
    })
    .catch(error => {
        if (!error.statusCode){
            error.statusCode = 500;
        }
        next(error);
    });

}

// PUT QUESTION- TBD: add image handling
exports.putQuestion = (req, res, next) => {
    let imageUrl = '';
    if(req.file){
        imageUrl = pathCreator(req.file.path);
    }
    const question = req.body.question;
    const description = req.body.description;
    let answers;
    if(typeof req.body.answers === 'string'){
        answers = JSON.parse(req.body.answers);
    }else{
        answers = req.body.answers
    }
    const difficulty = req.body.category;
    const category = req.body.category;
    const type = req.body.type;
    let image = '';
    if(imageUrl){
        image = imageUrl;
    }
    const timelimit = req.body.timelimit;
    const pricecategory = req.body.pricecategory;
    const userId = req.userId;
    const questionId = req.body.questionId;

    Question.findOne({
        _id: new ObjectId(questionId)
    })
    .then(questionObj => {
        console.log(questionObj?.createdby.toString(), userId.toString())
        const createdByUser = questionObj?.createdby.toString() === userId.toString();
        if(!createdByUser){
            const error = new Error('Unauthorized access for editing question');
            error.statusCode = 401;
            throw error;
        }
        if(!questionObj){
            const error = new Error('A quiz could not be found!');
            error.statusCode = 404;
            throw error;
        }
        questionObj.question = question;
        questionObj.description = description;
        questionObj.answers = answers;
        questionObj.difficulty = difficulty;
        questionObj.category = category;
        questionObj.type = type;
        if(image){
            questionObj.image = image;
        }
        questionObj.timelimit = timelimit;
        questionObj.pricecategory = pricecategory;
        return questionObj.save();
    })
    .then(result => {
        res.status(201).json({message: 'Question updated correctly!', questionId: result._id})
    })
    .catch(error => {
        if (!error.statusCode){
            error.statusCode = 500;
        }
        next(error);
    });

}

// DELETE QUIZ (by _id)
// By deleting quiz:
// * Delete quiz ref in User
// * Delete all questions ref. to this quiz
// * Delete Quiz itself
exports.deleteQuiz = (req, res, next) => {
    const quizId = req.body.quizId;
    const userId = req.userId;

    // Delete quiz from user
    User.findOne({
        _id: new ObjectId(userId)
    })
    .then(user => {
        if(!user){
            const error = new Error('Problem finding user');
            error.statusCode = 401;
            throw error;
        }
        const newQuizzes = user.quizzes.filter(id => id.toString() !== quizId);
        return user.updateOne({quizzes: newQuizzes});
    })
    .catch(error => {
        if (!error.statusCode){
            error.statusCode = 500;
        }
        next(error);
    })


    //Delete all questions from quiz
    Quiz.findOne({
        _id: new ObjectId(quizId)
    })
    .then(quiz => {
        const createdByUser = quiz?.createdby.toString() === userId.toString();
        if(!createdByUser){
            const error = new Error('Unauthorized access for editing question');
            error.statusCode = 401;
            throw error;
        }

        if(!quiz){
            const error = new Error('A quiz could not be found!');
            error.statusCode = 401;
            throw error;
        }
        // Delete all questions in array
        const questionIDsString = quiz.questions.map(id => id.toString());
        return Question.deleteMany(
            {
                _id: { $in: questionIDsString }
            }
        );
    })
    .catch(error => {
        if (!error.statusCode){
            error.statusCode = 500;
        }
        next(error);
    })

    // Delete quiz
    Quiz.deleteOne({
        _id: new ObjectId(quizId)
    })
    .then(() => {
        res.status(200).json({message: 'Quiz has been deleted successfully.', quizId: quizId})
    })
    .catch(error => {
        if (!error.statusCode){
            error.statusCode = 500;
        }
        next(error);
    })
}


// PUT QUIZ (UPDATE by _id)
// Behövs nog inte om jag hittar ett sätt uppdatera questions....