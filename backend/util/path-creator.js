const path = require('path');

module.exports = (imagePath) => {
    const pathArray = imagePath.split('/');
    return path.join('images', pathArray[pathArray.length-1]);
} 