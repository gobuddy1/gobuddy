const users = []

const addUser = (id, name, room, token, avatarImage) => {
    const existingUser = users.find(user => user.token.trim().toLowerCase() === token.trim().toLowerCase())

    if (existingUser) return { error: "Username has already been taken" }
    if (!name && !room && !token && !avatarImage) return { error: "Username, token, room and avatar are required" }
    if (!name) return { error: "Username is required" }
    if (!room) return { error: "Room is required" }
    if (!token) return { error: "Room is required" }
    if (!avatarImage) return { error: "Avatar is required" }

    // Add score score to user
    const score = 0;
    const user = { id, name, room, token, score, avatarImage }
    console.log(user)
    users.push(user)
    return { user }
}

const addScore = ( id, score ) => {
    let user = users.find(user => user.id == id)
    user.score += score;
    return user;
}

const getUser = id => {
    const user = users.find(user => user.id == id)
    return user
}

const deleteUser = (id) => {
    const index = users.findIndex((user) => user.id === id);
    if (index !== -1) return users.splice(index, 1)[0];
}

const getUsers = (room) => users.filter(user => user.room === room)

module.exports = { addUser, getUser, deleteUser, getUsers, addScore }