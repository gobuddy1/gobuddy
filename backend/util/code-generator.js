const codeGenerator = require('voucher-code-generator');

const OPTIONS = {
    length: 8,
    count: 1,
    charset: "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
}

module.exports = () => {
    return codeGenerator.generate(OPTIONS)[0]
}