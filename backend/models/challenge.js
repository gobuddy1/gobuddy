const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const challengeSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    locations: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Location'
        }
    ],
    createdby: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    public: {
        type: Boolean,
        required: true
    },
    imageUrl: {
        type: String
    },
    code: {
        type: Number,
        required: false
    },
    mapChallenge: {
        type: Boolean,
        required: false
    }
}, {timestamps: true});

module.exports = mongoose.model('Challenge', challengeSchema)