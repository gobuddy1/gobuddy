const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const locationSchema = new Schema({
    name: {
        type: String,
        // required: true
    },
    description: {
        type: String,
        // required: true
    },
    longitude: {
        type: Number,
        required: true
    },
    latitude: {
        type: Number,
        required: true
    },
    range: {
        type: Number,
        // required: true
    },
    createdby: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    questions: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Question'
        }
    ]
}, {timestamps: true});

module.exports = mongoose.model('Location', locationSchema)