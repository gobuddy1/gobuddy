const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const quizSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    questions: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Question'
        }
    ],
    createdby: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    imageUrl: {
        type: String
    },
    authUsers: [
        {
            type: Schema.Types.ObjectId,
            ref: 'User'
        }
    ],
    code: {
        type: String,
        required: false
    }
}, {timestamps: true});

module.exports = mongoose.model('Quiz', quizSchema)