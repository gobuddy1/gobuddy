const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const userSchema = new Schema({
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    telnumber: {
        type: String,
        required: true
    },
    subscriptiontype: {
        type: String
    },
    avatarimage: {
        type: String //Change upon implementing an avatar for the user
    },
    challenges: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Challenge'
        }
    ],
    quizzes: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Quiz'
        }
    ]
}, {timestamps: true});

module.exports = mongoose.model('User', userSchema)