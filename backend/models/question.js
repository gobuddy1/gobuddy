const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const questionSchema = new Schema({
    question: {
        type: String,
        required: true
    },
    answers: [
        {
            answer: {
                type: String,
                required: true
            },
            correct: {
                type: Boolean,
                required: true
            }
        }
    ],
    description: {
        type: String,
        required: true
    },
    difficulty: {
        type: String,
        required: true
    }, // easy/medium/hard
    category: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    }, // types: T/F, multiple choice, range
    image: {
        type: String //Change upon changing question
    },
    timelimit: {
        type: Number //Should be the timelimit in ms
    },
    createdby: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    pricecategory: {
        type: String
    } // Free/pro

}, {timestamps: true});

module.exports = mongoose.model('Question', questionSchema)