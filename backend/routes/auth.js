const express = require('express')
const { body } = require('express-validator')

const authController = require('../controllers/auth')
const User = require('../models/user');
const isAuth = require('../middleware/is-auth')

const router = express.Router();

//POST /auth/signup
router.put('/signup', [
    body('email')
        .normalizeEmail()
        .isEmail()
        .withMessage('Please enter a valid email')
        .custom((value, { req }) => {
            return User.findOne({email: value}).then(userDoc => {
                if(userDoc){
                    return Promise.reject('Email address already exists!');
                }
            })
        }),
        body('password').trim().isLength({
            min: 5
        }),
        body('firstname').trim().not().isEmpty(),
        body('lastname').trim().not().isEmpty(),
        body('telnumber').trim().not().isEmpty()
    ], authController.signup)

// POST /auth/login
router.post('/login', [
    body('email').normalizeEmail()
], authController.login)

// GET /auth/whoami
router.get('/whoami', isAuth, authController.whoami)

// POST /auth/update-user
router.post('/update-user', 
            isAuth, 
            [body('email').normalizeEmail()], 
            authController.updateUser)

router.get('/username/:userId', isAuth, authController.getUserName);


module.exports = router;