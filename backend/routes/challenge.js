const express = require('express')
const { body } = require('express-validator')

const User = require('../models/user');
const isAuth = require('../middleware/is-auth')
const challengeController = require('../controllers/challenge')

const router = express.Router();

//POST Challenge
router.post('/create-challenge', isAuth, challengeController.createchallenge);

//POST Location
router.post('/create-location', isAuth, challengeController.createlocation);

//POST Question
router.post('/create-question', isAuth, challengeController.createQuestion);

//GET Challenges
router.get('/get-challenges', isAuth, challengeController.getchallenges)

//GET Challenges
router.get('/get-public-challenges', isAuth, challengeController.getpublicchallenges)

//GET Locations
router.get('/get-locations/:challengeId', isAuth, challengeController.getlocations)

//GET Questions
router.get('/get-questions/:locationId', isAuth, challengeController.getquestions)

//DELETE Question
router.delete('/delete-question', isAuth, challengeController.deletequestion)

//DELETE Location
router.delete('/delete-location', isAuth, challengeController.deleteLocation)

//GET Challenge
router.get('/get-challenge/:challengeCode', isAuth, challengeController.getChallenge)

module.exports = router;