const express = require('express')
const { body } = require('express-validator')

const User = require('../models/user');
const isAuth = require('../middleware/is-auth')
const quizController = require('../controllers/quiz')

const router = express.Router();

//POST Quiz
router.post('/create-quiz', isAuth, quizController.createQuiz);

//GET Quiz
router.get('/get-quiz/:requestType/:quizCode', isAuth, quizController.getQuiz)

//GET Quiz by ID (doc id)
router.get('/get-quiz-byId/:requestType/:quizId', isAuth, quizController.getQuizById)

//GET Quizzes
router.get('/get-quizzes', isAuth, quizController.getQuizzes)

//GET Question
router.get('/get-question/:questionId', isAuth, quizController.getQuestion)

// DELETE QUESTION
router.delete('/delete-question', isAuth, quizController.deleteQuestion)

// POST QUESTION
router.post('/create-question', isAuth, quizController.postQuestion)

// PUT QUESTION
router.put('/update-question', isAuth, quizController.putQuestion)

// DELETE QUIZ (by _id)
router.delete('/delete-quiz', isAuth, quizController.deleteQuiz)

// PUT QUIZ (UPDATE by _id)
// Needed to update quiz name and description...

module.exports = router;